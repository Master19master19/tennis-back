<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/' , function () {
    return redirect( 'login' );
});

Auth::routes();

Route::group([ 'middleware' => [ 'auth' , 'web' , 'Admin' , 'App\Http\Middleware\Active' ] ] , function () {
	Route::get( '/log/outs' , 'LogController@outs' );
	Route::get( '/log/ins' , 'LogController@ins' );
	Route::get( '/log/courts' , 'LogController@courts' );
	Route::get( '/log/users' , 'LogController@users' );
	Route::get( 'backup' , 'BackupController@index' );
	Route::get( 'stats' , 'StatisticController@index' );
	// RESOURCES
	Route::resources([
		// 'orders' => 'OrderController',
		'tickets' => 'TicketController',
		'services' => 'ServiceController',
		'types' => 'TypeController',
		'categories' => 'CategoryController',
		'qualities' => 'QualityController',
		'users' => 'UserController',
		'arenas' => 'ArenaController',
		'courts' => 'CourtController',
		'seats' => 'SeatController',
		'subscriptions' => 'SubscriptionController',
		'announcements' => 'TournamentAnnoucementController',
		'tourcategories' => 'TournamentCategoryController',
		'trainers' => 'TrainerController',
		'news' => 'NewsController',
	]);
	// RESOURCES
	// DESTROYS
	Route::get( 'cards' , 'OrderController@cards' ) -> name( 'cards' );
	Route::get( 'orders/destroy/{id}' , 'OrderController@destroy' ) -> name( 'orders.destroy' );
	Route::get( 'services/destroy/{id}' , 'ServiceController@destroy' ) -> name( 'services.destroy' );
	Route::get( 'types/destroy/{id}' , 'TypeController@destroy' ) -> name( 'types.destroy' );
	Route::get( 'categories/destroy/{id}' , 'CategoryController@destroy' ) -> name( 'categories.destroy' );
	Route::get( 'qualities/destroy/{id}' , 'QualityController@destroy' ) -> name( 'qualities.destroy' );
	Route::get( 'users/destroy/{id}' , 'UserController@destroy' ) -> name( 'users.destroy' );
	Route::get( 'arenas/destroy/{id}' , 'ArenaController@destroy' ) -> name( 'arenas.destroy' );
	Route::get( 'courts/destroy/{id}' , 'CourtController@destroy' ) -> name( 'courts.destroy' );
	Route::get( 'seats/destroy/{id}' , 'SeatController@destroy' ) -> name( 'seats.destroy' );
	Route::get( 'subscriptions/destroy/{id}' , 'SubscriptionController@destroy' ) -> name( 'subscriptions.destroy' );
	Route::get( 'announcements/destroy/{id}' , 'TournamentAnnoucementController@destroy' ) -> name( 'announcements.destroy' );
	Route::get( 'tourcategories/destroy/{id}' , 'TournamentCategoryController@destroy' ) -> name( 'tourcategories.destroy' );
	Route::get( 'trainers/destroy/{id}' , 'TrainerController@destroy' ) -> name( 'trainers.destroy' );
	Route::get( 'news/destroy/{id}' , 'NewsController@destroy' ) -> name( 'news.destroy' );
	// DESTROYS
	// OTHER
	Route::post( 'tickets.answer/{id}' , 'TicketController@answer' ) -> name( 'tickets.answer' );
	// Route::get( 'editor' , 'CourtController@editor' ) -> name( 'editor' );
	Route::get( 'reserver' , 'CourtController@reserver' ) -> name( 'reserver' );
	Route::get( 'reserver/{date}' , 'CourtController@reserver' ) -> name( 'reserver' );
	// Route::get( 'boughtSeats' , 'BoughtSeatController@index' ) -> name( 'boughtSeats.index' );
	// Route::get( 'boughtSeats/{date}' , 'BoughtSeatController@index' ) -> name( 'boughtSeats' );
	Route::get( 'app_info' , 'AppInfoController@index' ) -> name( 'app.info' );
	Route::post( 'app_info_post' , 'AppInfoController@store' ) -> name( 'app.info.post' );
	Route::get( 'home' , function () {
		return redirect() -> route( 'arenas.index' );
	});
	// OTHER
});
// Route::get( 'mutate' , 'TestController@mutate' );
// Route::get( 'bug/users' , 'BugController@users' );
// Route::get( 'bug/reserves' , 'BugController@reserves' );
// Route::get( 'bug/canceled' , 'BugController@canceled' );
// Route::get( 'bug/reserved' , 'BugController@reserved' );
// Route::get( 'bug/paid' , 'BugController@paid' );
// Route::get( 'bug/compareHistory' , 'BugController@compareHistory' );
// Route::get( 'bug/test' , 'BugController@test' );
// Route::get( 'bug/checkWrongCourts' , 'BugController@checkWrongCourts' );
// Route::get( 'bug/cancelReserveByOrder/{orderId}' , 'BugController@cancelReserveByOrder' );
// Route::get( 'bug/getPaymentInfo/{orderId}' , 'BugController@getPaymentInfo' );
// Route::get( 'bug/cancelCheck' , 'BugController@cancelCheck' );
// Route::get( 'bug/fixtwentysix' , 'BugController@fixtwentysix' );
// Route::get( 'bug/fixhu' , 'BugController@fixhu' );
// Route::get( 'bug/sendSomeMail/{id}' , 'BugController@sendSomeMail' );
Route::get( 'bug/getMyUserTests' , 'BugController@getMyUserTests' );
// Route::get( 'bug/temp/testNewSubscriptions' , 'BugController@testNewSubscriptions' );
// Route::get( 'bug/fixforosix' , 'BugController@fixforosix' );
// Route::get( 'bug/clearHistory' , 'BugController@clearHistory' );
// Route::get( 'bug/mail/fix/temp' , 'BugController@fixMailTemp' );

Route::get( 'payStats' , 'BugController@payStats' );
Route::get( 'test/yandex/apple_pay' , 'Api\PaymentController@showPaymentTest' );
// Route::get( 'fixGuidErrorsOneTime' , 'BugController@fixGuidErrorsOneTime' );

// Route::get( 'maintenance' , 'TestController@disable_maintenance' );
Route::get( 'test/sendOFPCourtReserves' , 'TestController@sendOFPCourtReserves' );
Route::get( 'test/cronRemoveExpiredReserves' , 'TestController@cronRemoveExpiredReserves' );
Route::get( 'test/sms' , 'TestController@testSMS' );