<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([ 'middleware' => [ 'auth:api' , 'ban' ] ] , function () {
	Route::post( 'resetPasswordAuth' , 'Api\UserController@resetPasswordAuth' );
	Route::get( 'balance' , 'Api\UserController@balance' );
	Route::post( 'help' , 'Api\UserController@help' );
	Route::post( 'balance' , 'Api\UserController@requestBalance' );
	Route::get( 'refCode' , 'Api\UserController@refCode' );
	Route::get( 'services' , 'Api\OtherController@services' );
	Route::get( 'service/{categoryId}/{typeId}' , 'Api\OtherController@getService' );
	Route::get( 'typesAndCats' , 'Api\OtherController@typesAndCats' );
	Route::get( 'categories' , 'Api\OtherController@categories' );
	Route::get( 'orders' , 'Api\OtherController@orders' );
	Route::post( 'registerOrder' , 'Api\OtherController@registerOrder' );
	Route::get( 'referral' , 'Api\ReferralController@referral' );
	Route::get( 'catServices/{categoryId}' , 'Api\OtherController@getCatServices' );
	Route::post( 'payCart' , 'Api\PaymentController@payCart' );
	Route::post( 'payCart/new' , 'Api\PaymentController@payCartNew' );
	Route::post( 'pay' , 'Api\PaymentController@payCartAllNew' );
	Route::get( 'seatsHistory' , 'Api\OtherController@seatsHistory' );
	Route::get( 'subscriptionsHistory' , 'Api\OtherController@subscriptionsHistory' );
	Route::get( 'user' , 'Api\OtherController@getUser' );
	Route::post( 'settings/name' , 'Api\SettingController@changeName' );

	Route::get( 'profile/cards' , 'Api\SettingController@cards' );
	Route::get( 'profile/cards/remove/{cardId}' , 'Api\SettingController@removeCard' );
	Route::get( 'profile/addCard' , 'Api\PaymentController@addCard' );
	
	Route::post( 'settings/phone' , 'Api\SettingController@changePhone' );
	Route::post( 'settings/requestEmailChangeCode' , 'Api\SettingController@requestEmailChangeCode' );
	Route::post( 'setExpoPushToken' , 'Api\OtherController@setExpoPushToken' );
	Route::post( 'settings/checkEmailChangeCode' , 'Api\SettingController@checkEmailChangeCode' );
	Route::get( 'needCardDemo' , 'Api\OtherController@needCardDemo' );
});

Route::any( 'kassaNotify' , 'Api\PaymentController@kassaNotify' );

// Route::post('login', 'API\UserController@login');
Route::group([ 'middleware' => [ 'ban' ] ] , function () {
	Route::post( 'register' , 'Api\UserController@register' );
	Route::post( 'login' , 'Api\UserController@login' );
	Route::post( 'resetPassword' , 'Api\UserController@resetPassword' );
	Route::post( 'checkForgotPasswordCode' , 'Api\UserController@checkForgotPasswordCode' );
	Route::post( 'storeResetPassword' , 'Api\UserController@storeResetPassword' );
	Route::post( 'requestCode' , 'Api\UserController@requestCode' );
	Route::post( 'checkCode' , 'Api\UserController@checkCode' );
});
Route::get( 'subscriptions' , 'Api\OtherController@getSubscriptions' );
Route::get( 'news/{limit}' , 'Api\NewsController@index' );
Route::get( 'news/{id}' , 'Api\NewsController@show' );
Route::get( 'arena/{date}' , 'Api\CourtController@index' );
Route::get( 'arena/{date}/new' , 'Api\CourtController@getCourtsNew' );
Route::post( 'personalTrainingOrder' , 'Api\SchoolController@personalTrainingOrder' );
Route::post( 'personalChildTrainingOrder' , 'Api\SchoolController@personalChildTrainingOrder' );
Route::post( 'feedback' , 'Api\OtherController@feedback' );
Route::get( 'trainers' , 'Api\OtherController@trainers' );
Route::get( 'appInfo' , 'AppInfoController@show' );
Route::get( 'announcements' , 'TournamentAnnoucementController@showAll' );
Route::get( 'announcements/{id}' , 'TournamentAnnoucementController@show' );


// Route::group(['middleware' => 'auth:api'], function(){
// Route::post('details', 'API\UserController@details');

// ADMIN
// Route::post( 'editor' , 'CourtController@editorPost' ) -> name( 'editor.post' );
// Route::get( 'editor' , 'CourtController@editor' ) -> name( 'editor' );
Route::post( 'reserver' , 'CourtController@reserverPost' ) -> name( 'reserver.post' );
Route::post( 'boughtSeats' , 'CourtController@boughtSeatsPost' ) -> name( 'boughtSeats.post' );


Route::get( 'tmail/{id}' , 'Api\PaymentController@testMail' );
Route::get( 'paymentConfirm' , 'Api\PaymentController@loading' );
Route::get( 'rules' , 'Api\OtherController@rules' );


Route::post( '1c/mobileTennis' , 'Api\BitrixController@inhale' );
Route::post( 'mobileTennis' , 'Api\BitrixController@inhale' );
Route::post( '1c' , 'Api\BitrixController@inhale' );
// Route::prefix( '1c' ) -> group( function () {
//     // Route::post( '' , 'Api\BitrixController@inhale' ) -> middleware( 'auth.basic.once' );
//     Route::post( '' , 'Api\BitrixController@inhale' );
//     // Route::post( 'contragent/update' , 'Api\BitrixController@updateContragent' );
//     // Route::post( 'rent' , 'Api\BitrixController@rent' );
//     // Route::post( 'subscription' , 'Api\BitrixController@subscription' );
// });




Route::get( '1c.courts' , 'Api\BitrixController@courts' );
Route::get( 'pay/{confirmation_token}/isCard' , 'Api\PaymentController@showPaymentCard' );
Route::get( 'pay/{confirmation_token}' , 'Api\PaymentController@showPayment' );
Route::get( 'showPaymentTest' , 'Api\PaymentController@showPaymentTest' );



Route::get( 'pushIt' , 'BugController@pushIt' );