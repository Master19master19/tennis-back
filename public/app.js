$( function() {
  handleImages();
  handleTeams();
});


function handleImages() {
  if ( ! $( '#image-preview' ).length ) return;
  $( ".custom-file-input" ).on( "change" , function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
  document.getElementById("img").onchange = function () {
    var reader = new FileReader();
    reader.onload = function (e) {
      document.getElementById( "image-preview" ).src = e.target.result;
    };
    reader.readAsDataURL(this.files[0]);
  };
}


function handleTeams() {
  $( '#add-achievement' ).click( function() {
    let tpl = "<textarea name=\"achievements[]\" minlength=\"5\" maxlength=\"150\" class=\"form-control mt-3\" placeholder=\"Введите достижение\"></textarea>";
    $( '#achievements-wrapper' ).append( tpl );
  });
  $( '#add-experience' ).click( function() {
    let tpl = "<textarea name=\"experience[]\" minlength=\"5\" maxlength=\"150\" class=\"form-control mt-3\" placeholder=\"Введите стаж\"></textarea>";
    $( '#experience-wrapper' ).append( tpl );
  });
}