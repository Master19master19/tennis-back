
	$(document).ready( function () {
		// $('#table').DataTable();


		$('#table tfoot th').each( function () {
	        var title = $(this).text();
	        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	    } );
 
    // DataTable
    var sorting = $( '#table' ).data( 'sort' ) || 0;
    var table = $('#table').DataTable({
        "order": [[ sorting, "desc" ]],
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        }
    });


	} );