<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubAdditionalPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_additional_prices', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
            $table -> string( 'title' ) -> nullable();
            $table -> string( 'price' ) -> nullable();
            $table -> string( 'deposit' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_additional_prices');
    }
}
