<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalTrainingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_training_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string( 'name' ) -> nullable();
            $table -> string( 'phone' ) -> nullable();
            $table -> string( 'email' ) -> nullable();
            $table -> unsignedBigInteger( 'user_id' ) -> nullable();
            $table -> string( 'additional' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_training_requests');
    }
}
