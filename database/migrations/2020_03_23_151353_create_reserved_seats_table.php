<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservedSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        Schema::create('reserved_seats', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
            $table -> unsignedBigInteger( 'seat_id' ) -> nullable();
            $table -> unsignedBigInteger( 'court_id' ) -> nullable();
            $table -> unsignedBigInteger( 'arena_id' ) -> nullable();
            $table -> unsignedBigInteger( 'user_id' ) -> nullable();
            $table -> string( 'time' );
            $table -> date( 'date' ) -> nullable(); 
            $table -> string( 'reserved_until' );
            $table -> string( 'ip' ) -> nullable();
            $table -> tinyInteger( 'checked' ) -> default( 0 );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserved_seats');
    }
}
