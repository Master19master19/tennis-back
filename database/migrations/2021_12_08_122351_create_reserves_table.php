<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('second_connection') -> create('reserves', function (Blueprint $table) {
            $table -> bigIncrements('id');
            $table -> string( 'hash' );
            $table -> string('date');
            $table -> string('time');
            $table -> string('court_id');
            $table -> string('user_id')->nullable();
            $table -> unsignedInteger('price');
            $table -> unsignedInteger('tries') -> default(0);
            $table -> unsignedInteger('sent') -> default(0);
            $table -> string('error') -> nullable();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserves');
    }
}
