<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubPersonalPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_personal_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string( 'level' ) -> nullable();
            $table -> string( 'oneHourTraining' ) -> nullable();
            $table -> string( 'tenTraining' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_personal_prices');
    }
}
