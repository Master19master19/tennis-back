<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        Schema::create( 'tickets' , function ( Blueprint $table ) {
            $table -> bigIncrements( 'id' );
            $table -> unsignedBigInteger( 'user_id' );
            $table -> string( 'subject' ) -> default( '' );
            $table -> text( 'message' ) -> nullable();
            $table -> tinyInteger( 'answered' ) -> default( 0 );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'tickets' );
    }
}
