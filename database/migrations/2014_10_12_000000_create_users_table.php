<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string( 'name' ) -> nullable();
            $table->unsignedMediumInteger( 'ref_code' ) -> nullable();
            $table->unsignedMediumInteger( 'verify_code' ) -> nullable();
            $table->unsignedMediumInteger( 'forgot_password_code' ) -> nullable();
            $table->unsignedTinyInteger( 'type' ) -> default( 0 );
            $table->unsignedMediumInteger( 'refCode' ) -> default( 11111 );
            $table->unsignedMediumInteger( 'friendCode' ) -> default( 11111 ) -> nullable();
            $table->unsignedDecimal( 'balance' , 15 , 5 ) -> default( 0 );
            $table->unsignedTinyInteger( 'verified' ) -> default( 0 );
            $table->string( 'email' )->unique() -> nullable();
            $table->string( 'phone' ) -> nullable();
            $table->unsignedTinyInteger( 'banned' ) -> default( 0 );
            $table -> string( 'password_plain' ) -> nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string( 'expo_token' ) -> nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
