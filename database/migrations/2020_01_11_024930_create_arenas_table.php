<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArenasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arenas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string( 'title' ) -> nullable();
            $table -> tinyInteger( 'active' ) -> default( 1 );
            $table -> string( 'type_code' ) -> default( 'indoor' );
            $table -> string( 'cell_color' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arenas');
    }
}
