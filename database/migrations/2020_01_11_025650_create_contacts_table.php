<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string( 'phone' ) -> nullable();
            $table -> string( 'address' ) -> nullable();
            $table -> string( 'email' ) -> nullable();
            $table -> string( 'url' ) -> nullable();
            $table -> string( 'facebook' ) -> nullable();
            $table -> string( 'instagram' ) -> nullable();
            $table -> string( 'twitter' ) -> nullable();
            $table -> string( 'vkontakte' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
