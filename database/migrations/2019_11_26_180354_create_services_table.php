<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'services' , function (Blueprint $table) {
            $table -> bigIncrements( 'id' );
            $table -> string( 'title' ) -> nullable();
            $table -> unsignedBigInteger( 'category_id' );
            $table -> unsignedBigInteger( 'type_id' );
            $table -> unsignedBigInteger( 'quality_id' ) -> nullable();
            $table -> unsignedBigInteger( 'min_order' );
            $table -> unsignedBigInteger( 'max_order' );
            $table -> unsignedDecimal( 'thousand_price' , 15 , 5 );
            $table -> unsignedDecimal( 'price' , 15 , 5 );
            $table -> text( 'description' ) -> nullable();
            $table -> tinyInteger( 'active' ) -> default( 1 );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
