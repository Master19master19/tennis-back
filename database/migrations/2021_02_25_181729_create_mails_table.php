<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
            $table -> bigIncrements('id');
            $table -> unsignedBigInteger( 'order_id' );
            $table -> unsignedTinyInteger( 'done' ) -> default( 0 );
            $table -> unsignedTinyInteger( 'tries' ) -> default( 0 );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
    }
}
