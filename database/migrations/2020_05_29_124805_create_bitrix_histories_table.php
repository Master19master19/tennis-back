<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitrixHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'bitrix_histories' , function ( Blueprint $table ) {
            $table -> bigIncrements( 'id' );
            $table -> text( 'data' );
            $table -> integer( 'tries' ) -> default( 0 );
            $table -> string( 'error' ) -> nullable();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitrix_histories');
    }
}
