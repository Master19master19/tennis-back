<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalChildTrainingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_child_training_requests', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
            $table -> string( 'child_name' ) -> nullable();
            $table -> string( 'child_birthday' ) -> nullable();
            $table -> string( 'parent_name' ) -> nullable();
            $table -> unsignedBigInteger( 'user_id' ) -> nullable();
            $table -> string( 'additional' ) -> nullable();
            $table -> string( 'phone_number' ) -> nullable();
            $table -> string( 'level' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_child_training_requests');
    }
}
