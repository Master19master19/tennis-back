<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentAnnoucementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'tournament_annoucements' , function (Blueprint $table) {
            $table -> bigIncrements( 'id' );
            $table -> string( 'title' ) -> nullable();
            $table -> date( 'date_start' );
            $table -> date( 'date_end' );
            $table -> string( 'city' ) -> nullable();
            $table -> unsignedInteger( 'category_id' );
            $table -> string( 'type' ) -> nullable();
            $table -> string( 'court_coverage' ) -> nullable();
            $table -> string( 'official_ball' ) -> nullable();
            $table -> text( 'discharge' ) -> nullable();
            $table -> text( 'entrance_fee' ) -> nullable();
            $table -> string( 'director_name' ) -> nullable();
            $table -> string( 'director_email' ) -> nullable();
            $table -> string( 'director_phone' ) -> nullable();
            $table -> string( 'place' ) -> nullable();
            $table -> string( 'start_time' ) -> nullable();
            $table -> unsignedInteger( 'maximum_participants' ) -> nullable();
            $table -> text( 'first_part' ) -> nullable();
            $table -> text( 'second_part' ) -> nullable();
            $table -> text( 'prize' ) -> nullable();
            $table -> string( 'img_url' ) -> nullable();
            $table -> text( 'img_path' ) -> nullable();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_annoucements');
    }
}
