<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'trainers' , function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string( 'name' );
            $table -> string( 'description' );
            $table -> string( 'img_url' );
            $table -> string( 'img_path' );
            $table -> text( 'education' ) -> nullable();
            $table -> text( 'speciality' ) -> nullable();
            $table -> text( 'achievements' ) -> nullable();
            $table -> string( 'experience' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers');
    }
}
