<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_schemes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string( 'address' ) -> nullable();
            $table -> string( 'metro' ) -> nullable();
            $table -> string( 'bus' ) -> nullable();
            $table -> string( 'mapCode' ) -> nullable();
            $table -> string( 'mapCodePlanB' ) -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_schemes');
    }
}
