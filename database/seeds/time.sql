
CREATE TABLE `times` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `times` ( `title` ) VALUES
('07:00' ),
('08:00' ),
('09:00' ),
('10:00' ),
('11:00' ),
('12:00' ),
('13:00' ),
('14:00' ),
('15:00' ),
('16:00' ),
('17:00' ),
('18:00' ),
('19:00' ),
('20:00' ),
('21:00' ),
('22:00' ),
('23:00' );

ALTER TABLE `times`
  ADD PRIMARY KEY (`id`);