<!DOCTYPE html>
<html>
<head>
	<title>
		Payment Stats , tennis.ru
	</title>
</head>
<body>

<center>
	

<h1>Заказы с очень долгим ответом</h1>
<table style="width: 100%;" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th>ID</th>
			<th>Дата</th>
			<th>Сумма</th>
			<th>Задержка (Минут)</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $fuckOrders as $ek => $order )
			<tr>
				<td>{{ $order[ 'key' ] }}</td>
				<td>{{ $order[ 'created_at' ] }}</td>
				<td>{{ $order[ 'price' ] }}</td>
				<td>{{ $order[ 'timediff' ] }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
<h1>
	Статистика
</h1>
<table style="width: 50%;" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<td>Минут ( Ответ от Яндекса )</td>
			<td>Количество заказов</td>
		</tr>
	</thead>
	<tbody>
		@foreach( $differences as $min => $count )
			<tr style="text-align: center; font-size: 20px; padding: 10px;">
				<td style="padding: 6px;"><b>{{ $min }} мин.</b></td>
				<td style="padding: 6px;"><b>{{ $count }}</b></td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>Общее количество заказов</th>
			<th>{{ $totalCount }}</th>
		</tr>
	</tfoot>
</table>


</center>
</body>
</html>