@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Пользователи</h1>
        <a href="{{ route( 'users.create' ) }}" class="btn btn-info mb-4">
            <i class="fa fa-plus"></i>
            Добавить нового
        </a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Тел. номер
                    </th>
                    <th>
                        Роль
                    </th>
                    <th>
                        Верифицирован
                    </th>
                    <th>
                        Баланс
                    </th>
                    <th>
                        E-mail
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $users as $user )
                    <tr>
                        <td>{{ $user -> id }}</td>
                        <td>{{ $user -> name }}</td>
                        <td>{{ $user -> phone }}</td>
                        <td>{{ $user -> role }}</td>
                        <td>{{ $user -> isVerified }}</td>
                        <td>{{ $user -> balance + 0 }}</td>
                        <td>{{ $user -> email }}</td>
                        <td>
                            <a href="{{ route( 'users.edit' , $user -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'users.destroy' , $user -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection