@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Тикет #{{ $ticket -> id }} от {{ $ticket -> user -> email }}</h1>
        <div class="card w-50">
            <div class="card-body">
                <h5 class="card-title">{{ $ticket -> subject }}</h5>
                <small class="my-3 d-block">Дата: {{ $ticket -> created_at }}</small>
                <p class="card-text">{{ $ticket -> message }}</p>
                <!-- <a class="btn btn-primary" data-toggle="modal" href="#" data-target="#ticket-modal">Ответить</a> -->
            </div>
        </div>
    </div>
    @include( 'partials.ticket-modal' , [ 'ticket' => $ticket ] )
@endsection