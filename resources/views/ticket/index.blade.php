@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Тикеты</h1>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Пользователь
                    </th>
                    <th>
                        Тема
                    </th>
                    <th>
                        Сообщение
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $tickets as $ticket )
                    <tr>
                        <td>{{ $ticket -> id }}</td>
                        <td>{{ $ticket -> user -> email }}</td>
                        <td>{{ $ticket -> subject }}</td>
                        <td>{{ $ticket -> message }}</td>
                        <td>
                            <a href="{{ route( 'tickets.show' , $ticket -> id ) }}" class="btn btn-info btn-sm">Подробнее</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection