@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Абонементы</h1>
        <!-- <a href="{{ route( 'subscriptions.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новый</a> -->
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Количество тренировок в неделю
                    </th>
                    <th>
                        Продолжительность тренировки
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Дата
                    </th>
                    <!-- <th>
                        <i class="fa fa-wrench"></i>
                    </th> -->
                </tr>
            </thead>
            <tbody>
                @forelse ( $subscriptions as $subscription )
                    <tr>
                        <td>{{ $subscription -> id }}</td>
                        <td>{{ $subscription -> title }}</td>
                        <td>{{ $subscription -> weekCount }}</td>
                        <td>{{ $subscription -> duration }}</td>
                        <td>{{ $subscription -> price }}</td>
                        <td>{{ $subscription -> created_at }}</td>
                        <!-- <td>
                            <a href="{{ route( 'subscriptions.edit' , $subscription -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'subscriptions.destroy' , $subscription -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td> -->
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection