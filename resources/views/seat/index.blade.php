@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Места</h1>
        <a href="{{ route( 'seats.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новое</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Время
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Цена ( weekend )
                    </th>
                    <th>
                        Арена
                    </th>
                    <th>
                        Дата
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $seats as $seat )
                    <tr>
                        <td>{{ $seat -> id }}</td>
                        <td>{{ $seat -> time }}</td>
                        <td>{{ $seat -> price }}</td>
                        <td>{{ $seat -> price_weekend }}</td>
                        <td>{{ $seat -> arena -> title }}</td>
                        <td>{{ $seat -> created_at }}</td>
                        <td>
                            <a href="{{ route( 'seats.edit' , $seat -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'seats.destroy' , $seat -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection