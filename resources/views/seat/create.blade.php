@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Добавить место</h1>
        <form class="form" method="POST" action="{{ route( 'seats.store' ) }}">
            @csrf
            <div class="form-group">
                <label>Время <span class="text-danger">*</span></label>
                <input type="text" minlength="2" maxlength="100" required="" name="time" autofocus="" class="form-control" value="{{ old( 'time' ) }}" />
            </div>
            <div class="form-group">
                <label>Цена <span class="text-danger">*</span></label>
                <input type="number" min="200" required="" name="price" autofocus="" class="form-control" value="{{ old( 'price' ) }}" />
            </div>
            <div class="form-group">
                <label>Цена ( weekend ) <span class="text-danger">*</span></label>
                <input type="number" min="200" required="" name="price_weekend" autofocus="" class="form-control" value="{{ old( 'price_weekend' ) }}" />
            </div>
            <div class="form-group">
                <label>Корт <span class="text-danger">*</span></label>
                <select required="" class="form-control" name="arena_id">
                    @foreach ( $arenas as $arena )
                        <option value="{{ $arena -> id }}">{{ $arena -> title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection