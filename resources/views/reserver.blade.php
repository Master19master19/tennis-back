@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <h1 class="text-center my-4"><a class="mx-5 px-5"  href="/reserver/{{ date( 'Y-m-d' , strtotime( $date . ' -1 day' ) ) }}"><<</a> Брони {{ $date }} <a  class="mx-5 px-5" href="/reserver/{{ date( 'Y-m-d' , strtotime( $date . ' +1 day' ) ) }}">>></a></h1>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>
                    Время:
                </th>
                @foreach ( $seats as $arena )
                <th colspan="{{ count( $arena[ 'courts' ] ) * 2 }}">
                    {{ $arena[ 'title' ] }}
                </th>
                @endforeach
            </tr>
            <tr>
                <th>
                </th>
                @foreach ( $seats as $arena )
                    @foreach ( $arena[ 'courts' ] as $court )
                        <th colspan="2">
                            {{ $court[ 'title' ] }}
                        </th>
                    @endforeach
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ( $times as $time )
                <tr>
                    <th>
                        {{ $time -> title }}
                    </th>
                    @foreach ( $seats as $arena )
                    @foreach ( $arena[ 'courts' ] as $court )
                        <td colspan="2">
                            <button class="form-control btn btn-sm btn-xs btn-success @if( in_array(  (int) $time -> title , $arena[ 'unAvailableSeats' ][ $court[ 'id' ] ] ) ) btn-danger @endif" data-court="{{ $court[ 'id' ] }}" data-time="{{ $time -> title }}" data-date="{{ $date }}" >{{ $time -> title }}</button>
                        </td>
                    @endforeach
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection


@section ( 'scripts' )
    <script type="text/javascript">
        $( function() {
            $( '[data-date]' ).on( 'click' , function() {
                var courtId = $( this ).data( 'court' );
                var time = $( this ).data( 'time' );
                var date = $( this ).data( 'date' );
                var data = {
                    courtId: courtId,
                    date: date,
                    time: time,
                }
                $.post ( '/api/reserver' , data , function( res ) {
                    console.log( res , data );
                    toastr.success( 'Сохранено!' );
                    window.location.reload();
                })
            });
        });
        //     $( 'input' ).on( 'change' , function ( e ) {
        //         // if ( e.which == 13 ) {
        //             let val = $( this ).val();
        //             let name = $( this ).attr( 'name' );
        //             let argz = name.split( '-' );
        //             let arena_id = argz[ 0 ];
        //             let week_type = argz[ 1 ];
        //             let time = argz[ 2 ];
        //             let court_id = argz[ 3 ];
        //             let data = { time , arena_id , court_id };
        //             val = val.length > 1 ? val : 0;
        //             if ( week_type == 'week' ) {
        //                 data[ 'price' ] = val;
        //             } else{
        //                 data[ 'price_weekend' ] = val;
        //             }
        //             $.post ( '/api/editor' , data , function( res ) {
        //                 console.log( res );
        //                 toastr.success( 'Сохранено!' , val );

        //             })
        //             // alert( time + weekType + arenaId );
        //         // }
        //     });
        //     $( 'input' ).on( 'focus' , function ( e ) {
        //         $( this ).val( '' );
        //     });
        // });
    </script>
@endsection