@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Категории турниров</h1>
        <a href="{{ route( 'tourcategories.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Тип
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $tourCats as $tourCat )
                    <tr>
                        <td>{{ $tourCat -> id }}</td>
                        <td>{{ $tourCat -> title }}</td>
                        <td>{{ $tourCat -> getLevel }}</td>
                        <td>
                            <a href="{{ route( 'tourcategories.edit' , $tourCat -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'tourcategories.destroy' , $tourCat -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection