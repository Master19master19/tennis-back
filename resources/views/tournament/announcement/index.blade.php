@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Анонсы турниров</h1>
        <a href="{{ route( 'announcements.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Начало
                    </th>
                    <th>
                        Конец
                    </th>
                    <th>
                        Город
                    </th>
                    <th>
                        Категория
                    </th>
                    <th>
                        Тип
                    </th>
                    <th>
                        Покрытие
                    </th>
                    <th>
                        Офф. мяч
                    </th>
                    <th>
                        Разряд
                    </th>
                    <th>
                        Вступ.взнос
                    </th>
                    <th>
                        Директор
                    </th>
                    <th>
                        Место
                    </th>
                    <th>
                        Начало (час)
                    </th>
                    <th>
                        Макс. количество участников
                    </th>
                    <th>
                        Первая часть
                    </th>
                    <th>
                        Вторая часть
                    </th>
                    <th>
                        Призовой фонд
                    </th>
                    <th>
                        Картинка
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $announcements as $announ )
                    <tr>
                        <td>{{ $announ -> id }}</td>
                        <td>{{ $announ -> title }}</td>
                        <td>{{ $announ -> date_start }}</td>
                        <td>{{ $announ -> date_end }}</td>
                        <td>{{ $announ -> city }}</td>
                        <td>{{ $announ -> category -> title }}</td>
                        <td>{{ $announ -> type }}</td>
                        <td>{{ $announ -> court_coverage }}</td>
                        <td>{{ $announ -> official_ball }}</td>
                        <td>{{ $announ -> discharge }}</td>
                        <td>{{ $announ -> entrance_fee }}</td>
                        <td>{{ $announ -> director_name }} , {{ $announ -> director_email }} , {{ $announ -> director_phone }}</td>
                        <td>{{ $announ -> place }}</td>
                        <td>{{ $announ -> start_time }}</td>
                        <td>{{ $announ -> maximum_participants }}</td>
                        <td>{{ $announ -> first_part }}</td>
                        <td>{{ $announ -> second_part }}</td>
                        <td>{{ $announ -> prize }}</td>
                        <td><img height="100" src="{{ $announ -> img_url }}" /></td>
                        <td>
                            <a href="{{ route( 'announcements.edit' , $announ -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'announcements.destroy' , $announ -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="20">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection