@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Арены</h1>
        <a href="{{ route( 'arenas.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Дата
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $arenas as $arena )
                    @php $unique = uniqid(); @endphp
                    <tr>
                        <td>{{ $arena -> id }}</td>
                        <td>{{ $arena -> title }}</td>
                        <td>{{ $arena -> created_at }}</td>
                        <td>
                            <a href="{{ route( 'arenas.edit' , $arena -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'arenas.destroy' , $arena -> id ) }}" onclick="return '{{ $unique }}' == prompt( 'Чтобы удалить арену, введите {{ $unique }}' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection