@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Редактировать арену</h1>
        <form class="form" method="POST" action="{{ route( 'arenas.update' , $arena -> id ) }}">
            @csrf
            @method( 'PUT' )
            <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ $arena -> title }}" />
            </div>
            <div class="form-group">
                <label>Цвет <span class="text-danger">*</span></label>
                <input type="color" required="" name="cell_color" class="form-control" value="{{ $arena -> cell_color }}" />
            </div>
            <div class="custom-control custom-checkbox mb-4">
                <input type="checkbox" class="custom-control-input" id="active" name="active" {{ ( $arena -> active == 0 ) ? "" : "checked" }}>
                <label class="custom-control-label" for="active">Активно</label>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection