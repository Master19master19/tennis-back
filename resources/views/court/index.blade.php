@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Корты</h1>
        <a href="{{ route( 'courts.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Арена
                    </th>
                    <th>
                        Дата
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $courts as $court )
                    <tr>
                        <td>{{ $court -> id }}</td>
                        <td>{{ $court -> title }}</td>
                        <td>{{ $court -> arena -> title }}</td>
                        <td>{{ date( 'd/m/Y' , strtotime( $court -> created_at ) ) }}</td>
                        <td>
                            <a href="{{ route( 'courts.edit' , $court -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'courts.destroy' , $court -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection