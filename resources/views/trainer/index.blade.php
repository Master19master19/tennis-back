@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Тренеры</h1>
        <a href="{{ route( 'trainers.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Описание
                    </th>
                    <th>
                        Образование
                    </th>
                    <th>
                        Специальность
                    </th>
                    <th>
                        Картинка
                    </th>
                    <th colspan="2">
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $trainers as $trainer )
                    <tr>
                        <td>{{ $trainer -> id }}</td>
                        <td>{{ $trainer -> name }}</td>
                        <td>{{ $trainer -> description }}</td>
                        <td>{{ $trainer -> education }}</td>
                        <td>{{ $trainer -> speciality }}</td>
                        <td><img src="{{ $trainer -> image_url }}" height="100" /></td>
                        <td colspan="2">
                            <a href="{{ route( 'trainers.edit' , $trainer -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'trainers.destroy' , $trainer -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection