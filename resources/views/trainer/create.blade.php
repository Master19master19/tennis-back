@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Добавить тренера</h1>
        <form enctype="multipart/form-data" class="form" method="POST" action="{{ route( 'trainers.store' ) }}">
            @csrf
            <div class="form-group">
                <label>Имя <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="name" autofocus="" class="form-control" value="{{ old( 'name' ) }}" />
            </div>
            <div class="form-group">
                <label>Описание <span class="text-danger">*</span></label>
                <textarea name="description" minlength="5" maxlength="100" required="" class="form-control" >{{ old( 'description' ) }}</textarea>
            </div>
            <div class="form-group">
                <label>Образование <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="education" class="form-control" value="{{ old( 'education' ) }}" />
            </div>
            <div class="form-group">
                <label>Специальность <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="speciality" class="form-control" value="{{ old( 'speciality' ) }}" />
            </div>
            <div class="form-group">
                <label>Достижения <span class="text-danger">*</span></label>
                <div id="achievements-wrapper">
                    <textarea name="achievements[]" minlength="5" maxlength="150" required="" class="form-control" placeholder="Введите достижение"></textarea>
                </div>
                <button type="button" id="add-achievement" class="btn btn-sm btn-primary mt-2"><i class="fa fa-plus"></i> Добавить еще</button>
            </div>
            <div class="form-group">
                <label>Тренерский стаж <span class="text-danger">*</span></label>
                <div id="experience-wrapper">
                    <textarea name="experience[]" minlength="5" maxlength="150" required="" class="form-control" placeholder="Введите стаж"></textarea>
                </div>
                <button type="button" id="add-experience" class="btn btn-sm btn-primary mt-2"><i class="fa fa-plus"></i> Добавить еще</button>
            </div>
            <img src="" class="mb-4 w-50" id="image-preview" />
            <div class="custom-file mb-4">
                <input id="img" accept="image/png,image/jpg,image/jpeg" type="file" class="form-control custom-file-input" required="" name="img" id="customFile">
                <label class="custom-file-label" for="customFile">Картинка <span class="text-danger">*</span></label>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection