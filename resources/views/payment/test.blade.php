<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body style="margin-top: 20px;">
        <h4 style="text-align: center;margin: auto; width: 70%;position: relative;z-index: 5;">Тестовый платеж для ЮКасса</h4>



<script src="https://kassa.yandex.ru/checkout-ui/v2.js"></script>

<div id="payment-form" style="z-index: 2; position: relative;background: white;margin-top: 5px;"></div>

<script>
const checkout = new window.YandexCheckout({
    confirmation_token: "{!! $confirmation_token !!}", //Токен, который перед проведением оплаты нужно получить от Яндекс.Кассы
    return_url: "{!! env( 'KASSA_RETURN_URL' ) !!}",
    error_callback(error) { console.log({error})},
        customization: {
        //Настройка цветовой схемы, минимум один параметр, значения цветов в HEX
        colors: {
            //Цвет акцентных элементов: кнопка Заплатить, выбранные переключатели, опции и текстовые поля
            controlPrimary: '#7DC82F',
            controlPrimaryContent: '#ffffff'
        }
    },

});
checkout.render( "payment-form" );
</script>


</body>
</html>