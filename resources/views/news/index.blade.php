@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Новости</h1>
        <a href="{{ route( 'news.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новость</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Описание
                    </th>
                    <th>
                        Картинка
                    </th>
                    <th>
                        Дата
                    </th>
                    <th colspan="2">
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $news as $new )
                    <tr>
                        <td>{{ $new -> id }}</td>
                        <td>{{ $new -> title }}</td>
                        <td>{{ $new -> content }}</td>
                        <td><img src="{{ $new -> image_url }}" height="150" /></td>
                        <td colspan="2">
                            <a href="{{ route( 'news.edit' , $new -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'news.destroy' , $new -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection