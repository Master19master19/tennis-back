@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Редактировать новость</h1>
        <form enctype="multipart/form-data" class="form" method="POST" action="{{ route( 'news.update' , $news -> id ) }}">
            @csrf
            @method( 'PUT' )
            <div class="form-group">
                <label>Имя <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="50" required="" name="title" autofocus="" class="form-control" value="{{ $news -> title }}" />
            </div>
            <div class="form-group">
                <label>Описание <span class="text-danger">*</span></label>
                <textarea name="content" minlength="5" maxlength="500" required="" class="form-control" >{{ $news -> content }}</textarea>
            </div>
            <img src="{{ $news -> image_url }}" class="mb-4 w-50" id="image-preview" />
            <div class="custom-file mb-4">
                <input id="img" accept="image/png,image/jpg,image/jpeg" type="file" class="form-control custom-file-input" name="img" id="customFile">
                <label class="custom-file-label" for="customFile">Картинка <span class="text-danger">*</span></label>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection