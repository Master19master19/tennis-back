@extends('layouts.app')
@section( 'title' )
	Users Tennis
@endsection

@section('content')
	<link rel="stylesheet" type="text/css" href="/css/datatable.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
	<h1>
		Users Tennis
	</h1>
	<div>
		<table id="table" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Phone</th>
					<th>Phone 1C</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>
				@foreach( $data as $key => $row )
					<tr>
						<td>{{ $row -> id }}</td>
						<td>{{ $row -> phone }}</td>
						<td>{{ preg_replace('/[^0-9.]+/', '', $row -> phone ) }}</td>
						<td>{{ $row -> name }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
		</table>
	</div>
@endsection



@section ( 'scripts' )
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/datatables.js"></script>
@endsection