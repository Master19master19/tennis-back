@extends('layouts.app')
@section( 'title' )
	Courts Tennis
@endsection

@section('content')
	<link rel="stylesheet" type="text/css" href="/css/datatable.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
	<h1>
		Courts Tennis
	</h1>
	<div>
		<table id="table" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Title</th>
					<th>Arena</th>
				</tr>
			</thead>
			<tbody>
				@foreach( $data as $key => $row )
					<tr>
						<td>{{ $row -> id }}</td>
						<td>{{ $row -> title }}</td>
						<td>{{ $row -> arena -> title }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
		</table>
	</div>
@endsection



@section ( 'scripts' )
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/datatables.js"></script>
@endsection