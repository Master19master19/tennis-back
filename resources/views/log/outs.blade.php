@extends('layouts.app')
@section( 'title' )
	Outs Tennis
@endsection

@section('content')
	<link rel="stylesheet" type="text/css" href="/css/datatable.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
	<h1>
		Outs Tennis
	</h1>
	<div>
		<table data-sort="1" id="table" class="w-100 table table-striped table-bordered">
			<thead>
				<tr>
					<th>Message</th>
					<th>Id</th>
					<th>Created At  &nbsp &nbsp &nbsp Error</th>
					<th>Error Message</th>
					<th>Class</th>
				</tr>
			</thead>
			<tbody>
				@foreach( $data as $key => $row )
					<tr>
						<td><pre>{!! $row -> message_admin !!}</pre></td>
						<td><h3>{{ $row -> id }}</h3></td>
						<td>
							<h3> 
								{{ date( 'm-d H:i:s' , strtotime( $row -> created_at ) ) }}
								&nbsp &nbsp &nbsp
								{{ $row -> error }}
							</h3>
						</td>
						<td>
							{{ $row -> error_message }}
						</td>
						<td>{{ $row -> classs }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
		</table>
	</div>
@endsection



@section ( 'scripts' )
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/datatables.js"></script>
@endsection