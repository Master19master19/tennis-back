@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Заказы</h1>
        <!-- <a href="{{ route( 'orders.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a> -->
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Пользователь
                    </th>
                    <th>
                        Услуга
                    </th>
                    <th>
                        Количество
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Оплачено
                    </th>
                    <th>
                        Состояние
                    </th>
                    <th>
                        Ссылка
                    </th>
                    <th>
                        Дата
                    </th>
                    <!-- <th>
                        <i class="fa fa-wrench"></i>
                    </th> -->
                </tr>
            </thead>
            <tbody>
                @forelse ( $orders as $order )
                    <tr>
                        <td>{{ $order -> id }}</td>
                        <td>{{ $order -> user -> email }}</td>
                        <td>
                            <a target="_blank" href="{{ route( 'services.show' , $order -> service -> id ) }}">
                                {{ $order -> service -> type -> title }}
                            </a>
                        </td>
                        <td>{{ $order -> quantity }}</td>
                        <td>{{ $order -> total_pricee }}</td>
                        <td>{{ $order -> is_paid }}</td>
                        <td>{{ __( $order -> state ) }}</td>
                        <td>{{ $order -> link }}</td>
                        <td>{{ $order -> created_at }}</td>
                        <!-- <td>
                            <a href="{{ route( 'orders.edit' , $order -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'orders.destroy' , $order -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td> -->
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4">
                        Общая стоимость: 
                    </td>
                    <td colspan="10">
                        <b>
                            {{ $total }}
                        </b>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection