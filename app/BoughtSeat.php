<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoughtSeat extends Model
{
    protected $guarded = [];
	public function court () {
		return $this -> hasOne( 'App\Court', 'id' , 'court_id' );
	}
	public function arena () {
		return $this -> hasOne( 'App\Arena', 'id' , 'arena_id' );
	}
	public function user () {
		return $this -> hasOne( 'App\User', 'id' , 'user_id' );
	}
}
