<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {
    protected $guarded = [];
	public function type () {
		return $this -> hasOne( 'App\Type', 'id' , 'type_id' );
	}
	public function category () {
		return $this -> hasOne( 'App\Category', 'id' , 'category_id' );
	}
	protected $appends = [ 'activee' ];
    public function getActiveeAttribute () {
        return $this -> active == 1 ? "Да" : "Нет";
    }
    public function getPriceAttribute ( $value ) {
        return $value + 0;
    }
    public function getThousandPriceAttribute ( $value ) {
        return $value + 0;
    }
}
