<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalChildTrainingRequest extends Model
{
    //
    protected $guarded = [];
}
