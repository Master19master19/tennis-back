<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalTrainingRequest extends Model
{
	public $timestamps = true;
    protected $guarded = [];
    //
}
