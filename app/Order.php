<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
	public function user () {
		return $this -> hasOne( 'App\User', 'id' , 'user_id' );
	}
	protected $appends = [ 'pricee' , 'is_paid' ];
    // public function getStateeAttribute () {
    //     return $this -> active == 1 ? "Да" : "Нет";
    // }
    public function getPriceeAttribute () {
        return $this -> price + 0;
    }
    public function getIsPaidAttribute () {
        return $this -> paid == 1 ? "Да" : "Нет";
    }
}
