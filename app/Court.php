<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Court extends Model
{
    //
    protected $guarded = [];
	public function arena () {
		return $this -> hasOne( 'App\Arena', 'id' , 'arena_id' );
	}
	public function seats () {
		return $this -> hasMany( 'App\Seat', 'court_id' , 'id' );
	}
}
