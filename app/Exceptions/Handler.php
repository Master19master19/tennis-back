<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Helpers\LogHelper;

class Handler extends ExceptionHandler {
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        try {
            parent::report( $exception );
            if ( strlen( $exception -> getMessage() ) > 1 ) {
                $message = $exception -> getMessage();
                if ( strpos( $message , 'authorization server denied the request' ) === false && strpos( $message , 'Unauthenticated' ) === false && strpos( $message , 'The GET method is not' ) === false && strpos( $message , 'The given data was invalid' ) === false && strpos( $message , 'The POST method is not supported for this route' ) === false ) {
                    $res = LogHelper::telegram( 'Tennis.ru ' . $message );
                    LogHelper::send( $message );
                    file_put_contents( __DIR__ . '/fullmessage' , json_encode( $exception ) . PHP_EOL , FILE_APPEND );
                }
            }
        } catch ( Exception $e ) {
            file_put_contents( __DIR__ . '/fatal.log' , $e -> getMessage() .PHP_EOL , FILE_APPEND );
        }
        if ( $exception instanceof \Illuminate\Database\QueryException ) {
            file_put_contents( __DIR__ . '/mysql.error' , $exception -> getMessage() . PHP_EOL , FILE_APPEND );
            header('HTTP/1.1 400 Bad Request');
            exit( json_encode([
                'result' => false,
                'errorCode' => 400,
                'error' => "Проблема с подключением Mysql"
            ]));
        } else {

        }

        parent::report($exception);
        // try {
        //     LogHelper::telegram( $exception -> getMessage() );
        // } catch ( Exception $e ) {
        //     file_put_contents( __DIR__ . '/cant_send_telegram' , $e -> getMessage() . PHP_EOL , FILE_APPEND );
        // }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $ip = $request -> ip();
        if ( $ip == env( 'BITRIX_IP' ) ) {
            $code = $exception -> getCode();
            $message = $exception -> getMessage();
            parent::report( $exception );
            $data = [
                'result' => true,
                'errorCode' => 500,
                'errorMessage' => $message
            ];
            exit ( json_encode( $data ) );
        } else {
            return parent::render($request, $exception);
        }
    }
}
