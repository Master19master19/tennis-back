<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoughtSubscription extends Model {
    //
	public function user () {
		return $this -> hasOne( 'App\User', 'id' , 'user_id' );
	}
	public function subscription () {
		return $this -> hasOne( 'App\Subscription', 'id' , 'subscription_id' );
	}
}
