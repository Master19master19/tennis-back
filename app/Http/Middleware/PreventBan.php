<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Ban;
use App\Card;
use App\User;

class PreventBan
{
    /**
     * Handle an incoming req.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \Closure  $next
     * @return mixed
     */

    protected function getPhone ( $number ) {
        $response = str_replace( '-' , '' , $number );
        $response = str_replace( ')' , '' , $response );
        $response = str_replace( '(' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        return $response;
    }
    public function handle($req, Closure $next)
    {
        // file_put_contents(__DIR__ . '/asd' , json_encode( $req->all()));
        if ( Auth::check() ) {
            $check = Ban::wherePhone( Auth::user() -> phonee ) -> count();
            if ( $check > 0 ) {
                $checkCard = Card::whereUserId( Auth::user() -> id ) -> count();
                if ( $checkCard ) {
                    if ( in_array( $req -> path() , [ 'api/user' , 'api/profile/cards' ] ) || strpos( $req -> path() , 'api/profile/cards/remove' ) !== false ) {
        // file_put_contents(__DIR__ . '/c' , json_encode( $req->all()));
                        return $next( $req );
                    }
        // file_put_contents(__DIR__ . '/cs' , json_encode( $req->all()));
                        

                    return response() -> json([ 'error' => 'Ваш профиль заблокирован
Вы можете удалить привязанные карты' ] , 426 );
                } else {
        // file_put_contents(__DIR__ . '/cf' , json_encode( $req->all()));
                    return response() -> json([ 'error' => 'Ваш профиль заблокирован' ] , 425 );
                }
            }
        } else {
            if ( $req -> has( 'phone' ) ) {
                $numberCheck = $this -> getPhone( $req -> phone );
                $check = Ban::wherePhone( $numberCheck ) -> count();
                if ( $check ) {
                    $user = User::wherePhone( $req -> phone ) -> first();
                    if ( null !== $user ) {
                        $checkCard = Card::whereUserId( $user -> id ) -> count();
                        if ( $checkCard ) {
                            return $next( $req );
                        }
                    }
                    return response() -> json([ 'error' => 
                        'Авторизация невозможна
Профиль заблокирован' ] , 422 );
                }
            }
        }
        return $next($req);
    }
}
