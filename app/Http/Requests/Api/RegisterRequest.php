<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'nullable|email|unique:users', 
            'password' => 'required|min:6', 
            'passwordRepeat' => 'required|same:password',
            'phone' => 'required|string|min:18|max:18|unique:users',
        ];
    }
    public function messages()
    {
        return [
            'phone.min' => 'Проверьте правильность номера телефона',
            'phone.max' => 'Проверьте правильность номера телефона',
        ];
    }
}
