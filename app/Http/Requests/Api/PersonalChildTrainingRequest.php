<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PersonalChildTrainingRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'child_name' => 'required|string|min:4', 
            'child_birthday' => 'nullable|string', 
            'parent_name' => 'required|string|min:4', 
            'phone_number' => 'required|string|min:11|max:12',
            'level' => 'required|string|min:2', 
            'additional' => 'string|nullable', 
        ];
    }
}
