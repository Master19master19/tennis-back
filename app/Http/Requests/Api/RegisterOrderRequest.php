<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegisterOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|integer|exists:categories,id', 
            'type_id' => 'required|integer|exists:types,id', 
            'link' => 'required|string|min:5|max:50', 
            'quantity' => 'required|integer|min:1|max:1000000',
        ];
    }
}
