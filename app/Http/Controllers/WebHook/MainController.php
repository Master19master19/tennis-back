<?php

namespace App\Http\Controllers\WebHook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function tennisbook_ru ( Request $req ) {
        // header('Access-Control-Allow-Origin: *');
        file_put_contents(__DIR__.'/as', json_encode( $req -> all()) .PHP_EOL, FILE_APPEND );
        try {
            $phone = $req -> input( 'Phone' , 'Phone unknown' );
            $phone = preg_replace( '/[^0-9+]+/' , '' , $phone);
            $data = [
                'class' => 'payment',
                'name' => $req -> input( 'Name' , 'Name unknown' ),
                'phone' => $phone,
                'email' => $req -> input( 'Email' , 'noemail@mail.com' ),
                'changeDate' => date( 'Y-m-d\TH:i:s' ),
                'items' => [
                    [
                        'itemType' => 'product',
                        'productId' => 'd0dc6636-c5ba-11e9-9ab5-382c4a64b65a',
                        'count' => 1,
                        'sum' => 2000
                    ]
                ]
            ];
            try {
                $payment = $req -> input( 'payment' );
                $amount = $payment[ 'amount' ];
                $data[ 'items' ][0]['sum'] = $amount;
                $data[ 'items' ][0]['count'] = $amount / 2000;
            } catch ( \Exception $e ) {
                file_put_contents( __DIR__ . '/exception.js', $e -> getMessage() .PHP_EOL, FILE_APPEND );
            }
            file_put_contents(__DIR__.'/data.js', json_encode( $data ) .PHP_EOL, FILE_APPEND );
            $sendToPartnerStatus = $this -> sendToPartner( $data );
            return response() -> json([ 'success' ]);
        } catch ( \Exception $e ) {
            file_put_contents( __DIR__ . '/exception_global.js', $e -> getMessage() .PHP_EOL, FILE_APPEND );
        }
// {
//   "Name": "Лагкуев Маирбек Владимирович",
//   "Phone": "+7 (918) 822-06-08",
//   "Email": "l-m-v@yandex.ru",
//   "Book": "Book-1",
//   "Checkbox": "yes",
//   "Checkbox_2": "yes",
//   "payment": {
//     "sys": "yakassa",
//     "systranid": "2925987b-000f-5000-9000-1f44cd1c0aff",
//     "orderid": "1471792238",
//     "products": [
//       {
//         "name": "Настольная книга тренера по теннису!",
//         "quantity": "1",
//         "amount": "2000",
//         "price": "2000",
//         "sku": "Методическое пособие разработано на основе системы 10s и опыте ведущих европейских школФормат: PDFСтраниц: 176Печать: Полноцветная"
//       }
//     ],
//     "amount": "2000"
//   },
//   "formid": "form345503053"
// }
    }

    protected function sendToPartner ( $data ) {
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client -> request( 'POST' , env( 'WEBHOOK_BITRIX_URL' ) , [
                'timeout' => 15,
                'json' => $data,
                'auth' => [
                    env( 'BITRIX_USERNAME' ),
                    env( 'BITRIX_PASSWORD' )
                ]
            ]);
            return true;
        } catch ( RequestException $e ) {
            $message = iconv( "windows-1251" , "utf-8" , $e -> getMessage() );
            file_put_contents( __DIR__ . '/bitrix_error' , json_encode( $message ) . PHP_EOL , FILE_APPEND );
        }
    }
}
