<?php

namespace App\Http\Controllers;

use App\Helpers\CourtHelper;
use Illuminate\Http\Request;
use App\Seat;

class BoughtSeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function isWeekend( $date ) {
        return ( date( 'N' , strtotime( $date ) ) >= 6 );
    }

    public function index ( $date = null ) {
        $date = $date ?? date( 'Y-m-d' );

        $arenas = CourtHelper::getCourts( $date );
        // dd($arenas);
        foreach ($arenas as $keye => $arena) {
            foreach ($arena['courts'] as $key => $court) {
                $arenas[$keye]['courts'][$key]['seats'] = Seat::where('court_id',$court['id'])->get()->toArray();
            }
        }
        // dd($this -> isWeekend( $date ));
        return view( 'boughtSeats' , [ 'arenas' => $arenas , 'isWeekend' => $this -> isWeekend( $date ) , 'date' => $date ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoughtSeat  $boughtSeat
     * @return \Illuminate\Http\Response
     */
    public function show(BoughtSeat $boughtSeat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoughtSeat  $boughtSeat
     * @return \Illuminate\Http\Response
     */
    public function edit(BoughtSeat $boughtSeat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoughtSeat  $boughtSeat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoughtSeat $boughtSeat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoughtSeat  $boughtSeat
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoughtSeat $boughtSeat)
    {
        //
    }
}
