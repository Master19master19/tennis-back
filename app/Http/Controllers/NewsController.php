<?php

namespace App\Http\Controllers;

use App\News;
use App\Helpers\NotificationHelper;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index () {
        $news = News::all();
        return view( 'news.index' , compact( 'news' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view( 'news.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendNotification( $data ) {
        NotificationHelper::send( 'news' , $data );
    }
    public function store ( Request $req ) {
        $img = $req -> img;
        $url = "/club/news/" . $img -> getClientOriginalName();
        $res = $img -> move( public_path() . "/club/news/" , $img -> getClientOriginalName() );
        $path = $res -> getRealPath();
        $data = $req -> except( [ '_token' , 'img' ] );
        $data[ 'image_path' ] = $path;
        $data[ 'image_url' ] = $url;
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        News::insert( $data );
        $data = News::orderBy( 'id' , 'DESC' ) -> first();
        $this -> sendNotification( $data );
        return redirect() -> route( 'news.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        // dd($news);
        // $news = News::findOrFail( $id );
        return view( 'news.edit' , compact( 'news' ) );
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, News $news)
    {
        $data = $req -> except( [ '_token' , 'img' , '_method' ] );
        if ( isset( $req -> img ) ) {
            $img = $req -> img;
            $url = "/club/news/" . $img -> getClientOriginalName();
            $res = $img -> move( public_path() . "/club/news/" , $img -> getClientOriginalName() );
            $path = $res -> getRealPath();
            $data[ 'image_path' ] = $path;
            $data[ 'image_url' ] = $url;
        }
        News::whereId( $news -> id ) -> update( $data );
        return redirect() -> route( 'news.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news,$id)
    {
        News::findOrFail( $id ) -> delete();
        return redirect() -> route( 'news.index' );
    }
}
