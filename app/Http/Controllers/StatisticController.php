<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $totalOrders = 0;
        $users = User::where( 'name' , 'NOT LIKE' , '%test%' ) -> select( 'id' ) -> get();
        $countUsers = count( $users );
        foreach ( $users as $key => $user ) {
            $order = Order::where( 'user_id' , $user -> id ) -> count();
            if ( $order !== 0 ) {
                $totalOrders++;
            }
            // dd($order);
        }
        echo "<h1>Всего пользователей: <b>";
        echo $countUsers;
        echo "<b></h1><br>";
        echo "<h1>Всего полкупок для каждого юзера: <b>";
        echo $totalOrders;
        echo "<b></h1><br>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
