<?php

namespace App\Http\Controllers;

use App\Court;
use App\Seat;
use App\Arena;
use App\BoughtSeat;
use Illuminate\Http\Request;
use App\Helpers\BitrixHelper;
use App\Helpers\CourtHelper;

class CourtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reserver( $date = null ) {
        if ( $date == null ) {
            $date = date( 'Y-m-d' );
        }
        $seats = CourtHelper::getCourts( $date , true );
        // dd($seats);
        return view( 'reserver' , compact( 'seats' , 'date' ) );
        $seats = [];
        foreach ( $seatz as $key => $value ) {
            $seats[ $value -> arena_id ][ $value -> court_id ][ $value -> time ] = [ $value[ 'price' ] , $value[ 'price_weekend' ] ];
        }
        return view( 'editor' , compact( 'seats' ) );
    }
    public function reserverPost ( Request $req ) {
        $data = $req -> only([ 'courtId' , 'time' , 'date' ]);
        $availability = BoughtSeat::where( [ 'date' =>  $data[ 'date' ] , 'paid' => 1 , 'court_id' => $data[ 'courtId' ] , 'time' => $data[ 'time' ] ] ) -> first();
        $seat = Seat::where([ 'court_id' => $data[ 'courtId' ] , 'time' => $data[ 'time' ] ]) -> firstOrFail();
        $arena = Court::find( $data[ 'courtId' ] ) -> arena;
        $dateWeekName = strtolower( date( 'l' , strtotime( $data[ 'date' ] ) ) );
        if ( null === $availability ) {
            $newBought = new BoughtSeat;
            $newBought -> time = $data[ 'time' ];
            $newBought -> date = $data[ 'date' ];
            $newBought -> court_id = $data[ 'courtId' ];
            $newBought -> arena_id = $arena -> id;
            $newBought -> user_id = 88888;
            $newBought -> paid = '1';
            $newBought -> price = $seat -> $dateWeekName;
            $newBought -> seat_id = $seat -> id;
            $newBought -> save();
        } else {
            // $availability = BoughtSeat::where( [ 'date' =>  $data[ 'date' ] , 'paid' => 1 , 'court_id' => $data[ 'courtId' ] , 'time' => $data[ 'time' ] ] ) -> delete();
        }
        return response() -> json(['true']);
        dd($availability);
        dd($req->all());
    }
    public function editor() {
        return false;
        $seatz = Seat::all();
        $seats = [];
        foreach ( $seatz as $key => $value ) {
            $seats[ $value -> arena_id ][ $value -> court_id ][ $value -> time ] = [ $value[ 'price' ] , $value[ 'price_weekend' ] ];
        }
        return view( 'editor' , compact( 'seats' ) );
    }
    public function boughtSeatsPost ( Request $req ) {
        $data = $req -> all();
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        $data [ 'paid' ] = 1;
        $res = BoughtSeat::insert( $data );
        // dd($res);
        return response() -> json( $res );
        $oldSeat = Seat::where([
            'time' => $req -> time,
            'arena_id' => $req -> arena_id,
        ]) -> first();
        if ( null === $oldSeat ) {
            $seat = Seat::create( $req -> all() );
        } else {
            $oldSeat -> update( $req -> all() );
        }
        return response() -> json([ 'status' => 1 ]);
    }
    public function editorPost ( Request $req ) {
        return false;
        $oldSeat = Seat::where([
            'time' => $req -> time,
            'arena_id' => $req -> arena_id,
            'court_id' => $req -> court_id
        ]) -> first();
        if ( null === $oldSeat ) {
            $seat = Seat::create( $req -> all() );
        } else {
            $oldSeat -> update( $req -> all() );
            $seat = $oldSeat;
        }
        // $arena = Arena::findOrFail( $req -> arena_id );
        // BitrixHelper::updateSeat( $seat , $req -> has( 'price' ) ? 'week' : 'weekend' , $arena );
        return response() -> json([ 'status' => 1 ]);
    }
    public function index () {
        $courts = Court::all();
        return view( 'court.index' , compact( 'courts' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arenas = Arena::all();
        return view( 'court.create' , compact( 'arenas' ) );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $data = $req -> only( 'title' , 'arena_id' );
        $res = Court::create( $data );
        return redirect() -> route( 'courts.index' );
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function show(Court $court)
    {
        //
        return view( 'court.show' , compact( 'court' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function edit(Court $court)
    {
        //
        $arenas = Arena::all();
        return view( 'court.edit' , compact( 'arenas' , 'court' ) );
        // return view( 'court.edit' , compact( 'court' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Court $court)
    {
        $data = $req -> only( 'title' , 'arena_id' );
        $res = $court -> update( $data );
        return redirect() -> route( 'courts.index' );
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Court::findOrFail( $id ) -> delete();
        return redirect() -> route( 'courts.index' );
        //
    }
}
