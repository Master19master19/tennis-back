<?php

namespace App\Http\Controllers;

use App\Trainer;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $trainers = Trainer::all();
        return view( 'trainer.index' , compact( 'trainers' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view( 'trainer.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sanitizeData ( $data ) {
        foreach ( $data[ 'achievements' ] as $key => $value) {
            if ( strlen( $value ) < 4 || null == $value ) {
                unset( $data[ 'achievements' ][ $key ] );
            }
        }
        foreach ( $data[ 'experience' ] as $key => $value) {
            if ( strlen( $value ) < 4 || null == $value ) {
                unset( $data[ 'experience' ][ $key ] );
            }
        }
        $data[ 'experience' ] = json_encode( array_values( $data[ 'experience' ] ) );
        $data[ 'achievements' ] = json_encode( array_values( $data[ 'achievements' ] ) );
        return $data;
    }
    public function store(Request $req) {
        $img = $req -> img;
        $url = "/club/team/" . $img -> getClientOriginalName();
        $res = $img -> move( public_path() . "/club/team/" , $img -> getClientOriginalName() );
        $path = $res -> getRealPath();
        $data = $req -> except( [ '_token' , 'img' ] );
        $data[ 'img_path' ] = $path;
        $data[ 'img_url' ] = $url;
        $data = $this -> sanitizeData( $data );
        Trainer::insert( $data );
        return redirect() -> route( 'trainers.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer $trainer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer)
    {
        // dd($trainer);
        // $trainer = Trainer::findOrFail( $id );
        return view( 'trainer.edit' , compact( 'trainer' ) );
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Trainer $trainer)
    {
        $data = $req -> except( [ '_token' , 'img' , '_method' ] );
        if ( isset( $req -> img ) ) {
            $img = $req -> img;
            $url = "/club/team/" . $img -> getClientOriginalName();
            $res = $img -> move( public_path() . "/club/team/" , $img -> getClientOriginalName() );
            $path = $res -> getRealPath();
            $data[ 'img_path' ] = $path;
            $data[ 'img_url' ] = $url;
        }
        $data = $this -> sanitizeData( $data );
        Trainer::whereId( $trainer->id ) -> update( $data );
        return redirect() -> route( 'trainers.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer $trainer,$id)
    {
        Trainer::findOrFail( $id ) -> delete();
        return redirect() -> route( 'trainers.index' );
    }
}
