<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BoughtSeat;
use App\ReservedSeat;
use App\BitrixHistory;
use App\Helpers\BitrixHelper;
use App\Helpers\BitrixSendPaymentHelper;
use App\Helpers\BitrixCancelReserveHelper;
use App\Helpers\BitrixReserveHelper;
use App\Order;
use App\User;
use App\Helpers\Reserve\CronHelper;
use App\Helpers\SMS;

class TestController extends Controller
{
	public function testSMS() {
		$title = "Some title";
		$desc = 5555;
		$to = "37477797590";
		$res = SMS::sendSMS( $title , $desc , $to );
	}
	public function cronRemoveExpiredReserves() {
		// CronHelper::removeExpiredReserves();
		// CronHelper::copyBoughtSeats();
		CronHelper::sendToBitrix();
		
	}


	private function setEnv($key, $value) {
		$oposite = $value === 'false' ? 'true' : 'false';
		file_put_contents(app()->environmentFilePath(), str_replace(
			"BITRIX_DOWN=\"{$oposite}\"",
			"BITRIX_DOWN=\"{$value}\"",
			file_get_contents(app()->environmentFilePath())
		));
		return true;
	}

	public function disable_maintenance() {
		$oldVal = env( 'BITRIX_DOWN' );
		if ( $oldVal == 'true' || $oldVal == true ) {
			$res = $this -> setEnv( "BITRIX_DOWN" , "false" );
			exit( '<h1 style="text-align:center;margin-top:20px;">Maintenance mode ОТКЛЮЧЕН, оплаты и брони теперь будут приниматься</h1><h2>Время : ' . date( 'H:m' ) . ' </h2>' );
			dd( $res , 'env is' , env( 'BITRIX_DOWN' ) );
		} else if ( $oldVal == 'false' || $oldVal == false ) {
			$res = $this -> setEnv( "BITRIX_DOWN" , "true" );
			exit( '<h1 style="text-align:center;margin-top:20px;">Maintenance mode ВКЛЮЧЕН, оплаты и брони НЕ БУДУТ ПРИНИМАТСЯ</h1><h2>Время : ' . date( 'H:m' ) . ' </h2>' );
		}
	}


	public function sendOFPCourtReserves() {
		$reserves = BoughtSeat::where([
			'court_id' => 31,
		])
		// ->where('temp_sent_ofp',0)
			-> where( 'user_id','!=',0)
		 -> get();
		foreach($reserves as $reserve ) {
			// $result = BitrixReserveHelper::tempSendWithBoughtSeatId( $reserve -> id );
			$result_payment = BitrixSendPaymentHelper::tempSendPaymentByBoughtSeatId( $reserve -> id );
			// if($result) {
			// 	BoughtSeat::where([
			// 		'id' => $reserve -> id,
			// 	]) -> update(['temp_sent_ofp' => 1 ]);
			// } else {
			// 	BoughtSeat::where([
			// 		'id' => $reserve -> id,
			// 	]) -> update(['temp_sent_ofp' => 2 ]);
			// }
			// dd($reserve,$result_payment);
		}
		dd($reserves);
	}
	

    public function mutate () {
        $boughtSeats = BoughtSeat::where([
            'paid' => 1,
            'checked' => 0
        ]) -> orderBy( 'id', 'DESC' ) -> take( 10 ) -> get();
        foreach ( $boughtSeats as $boughtSeat ) {
	        $res = BitrixSendPaymentHelper::send($boughtSeat->order_id);
	        if( $res == true ) {
	        	BoughtSeat::where( 'order_id' , $boughtSeat -> order_id ) -> update( [ 'checked' => 1 ]);
	        	// $boughtSeat -> checked = 1;
	        	// $boughtSeat -> save();
	        	// dd('cool',$res);
	        } else {
	        	dd('error',$res);
	        }
        }
        exit( 'done' );
    }
    //
    public function mutateeee () {
    	dd(date('Y-m-d H:i:s'));
    	$json = '{"class":"rent","newReserve":[{"courtId":"13","startTime":"2020-06-01T12:00:00","finishTime":"2020-06-01T12:59:59"},{"courtId":"13","startTime":"2020-06-01T13:00:00","finishTime":"2020-06-01T13:59:59"}],"phone":"+7916128","changeDate":"2020-05-30T13:03:53"}';
    	$res = json_decode( $json , 1 );
    	$res = BitrixHelper::send( $res );
    	dd('ok', $res );
    	// exit('pok');
        // BitrixCancelReserveHelper::check();
        // exit;
    	// $rr=\DB::getQueryLog();
    	// dd($rr);
       // exit();
    	$dat = '{"class":"rent","newReserve":[{"courtId":"9","startTime":"2020-06-01T14:00:00","finishTime":"2020-06-01T14:59:59"},{"courtId":"9","startTime":"2020-06-02T09:00:00","finishTime":"2020-06-02T09:59:59"},{"courtId":"9","startTime":"2020-06-03T14:00:00","finishTime":"2020-06-03T14:59:59"},{"courtId":"9","startTime":"2020-06-04T09:00:00","finishTime":"2020-06-04T09:59:59"},{"courtId":"9","startTime":"2020-06-05T14:00:00","finishTime":"2020-06-05T14:59:59"},{"courtId":"9","startTime":"2020-06-06T09:00:00","finishTime":"2020-06-06T09:59:59"}],"phone":"+79688676704","changeDate":"2020-05-29T16:10:47"}';
    	$res = json_decode( $dat , 1 );
    	// dd($res);
    	$ress = BitrixHelper::send( $res );
    	dd($ress);
    }
    public function mutateB () {
    	$orders = Order::wherePaid( '1' ) -> where( 'paidd' , 0 ) -> where( 'created_at' , '>' , '2020-05-29 15:00:44' ) -> get();
    	foreach ( $orders as $key => $order ) {
        	BitrixSendPaymentHelper::send( $order -> id );
        	Order::whereId( $order -> id ) -> update( [ 'paidd' => 1 ] );
        	dd( $order );
        	sleep( 1 );
    	}
    }
    public function mutateC() {
		$res = BitrixHistory::orderBy( 'id' , 'DESC' ) -> take( 10 ) -> get();
		foreach ( $res as $record ) {
			$data = $record -> data;
			$dat = json_decode( $data , 1 );
			if ( isset ( $dat[ 'items' ] ) ) {
				foreach ($dat['items'] as $key => $value) {
					$startTime = $value[ 'startTime' ];
					if ( strpos( $startTime , 'T' ) === false ) {
						$startTime = "0000:00:00T" . $startTime;
					}
					$finishTime = $value[ 'finishTime' ];
					if ( strpos( $finishTime , 'T' ) === false ) {
						$finishTime = "0000:00:00T" . $finishTime;
					}
					$value['startTime']=$startTime;
					$value['finishTime']=$finishTime;
					$dat[ 'items' ][ $key ] = $value;
				}	
			}
			// $record 
			$res = BitrixHelper::send( $dat , 'checking' );
			// dd($dat,$record);
			if ( $res === true ) {
				BitrixHistory::whereId( $record -> id ) -> delete();
			} else {
				dd($res);
				BitrixHistory::whereId( $record -> id ) -> update([ 'tries' => $record -> tries + 1 ]);
			}
			exit;
			sleep( 1 );
		}
    }
    public function tmail() {
    	$data = [
    		'title' => 'rest',
    		'desc' => 'restdesc',
    	];
		$rres = \Mail::send( 'mail.send' , $data , function( $message ) {
			$message -> to( 'vmxitaryan@gmail.com' );
			$message -> subject( 'subjecting@gmail.com' );
		});
		dd($rres);

			// $mailSMTP = new SendMailSmtpClass('app@tennis.ru', 'Gny39We6', 'ssl://smtp.yandex.ru', 465);
			// // $mailSMTP = new SendMailSmtpClass('логин', 'пароль', 'хост', 'имя отправителя');

			// // заголовок письма
			// $headers= "MIME-Version: 1.0\r\n";
			// $headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
			// // $headers .= "From: Tennis.ru <app@tennis.ru>\r\n"; // от кого письмо
			// $result =  $mailSMTP->send('vmxitaryan@gmail.com', 'Тема письма', 'Текст письма', $headers); // отправляем письмо
			// // $result =  $mailSMTP->send('Кому письмо', 'Тема письма', 'Текст письма', 'Заголовки письма');
			// if($result === true){
			// echo "Письмо успешно отправлено";
			// }else{
			// echo "Письмо не отправлено. Ошибка: " . $result;
			// }
    // 	  $to = "vmxitaryan@gmail.com";
		  // $subject = "Robot - Робот";
		  // $message = "Message,\n сообщение!";
		  // $headers = "From: Tennis.ru <app@tennis.ru>\r\nContent-type: text/plain; charset=utf-8 \r\n";
		  // mail ($to, $subject, $message, $headers);

    }
}
