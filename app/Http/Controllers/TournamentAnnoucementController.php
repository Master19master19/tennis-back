<?php

namespace App\Http\Controllers;

use App\TournamentAnnoucement;
use App\TournamentCategory as TourCat;
use App\Helpers\NotificationHelper;
use Illuminate\Http\Request;

class TournamentAnnoucementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $announcements = TournamentAnnoucement::has( 'category' ) -> with( 'category' ) -> get();
        return view( 'tournament.announcement.index' , compact( 'announcements' ) );
    }
    public function showAll () {
        $announcements = TournamentAnnoucement::has( 'category' ) -> with( 'category' ) -> orderBy( 'id' , 'DESC' ) -> get();
        return response() -> json( $announcements );
    }
    public function show ( $id ) {
        $announcement = TournamentAnnoucement::with( 'category' ) -> whereId( $id ) -> get();
        return response() -> json( $announcement );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        $tourCats = TourCat::all();
        return view( 'tournament.announcement.create' , compact( 'tourCats' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        $img = $req -> img;
        $url = "/announcementsImages/" . $img -> getClientOriginalName();
        $res = $img -> move( public_path() . "/announcementsImages/" , $img -> getClientOriginalName() );
        $path = $res -> getRealPath();
        $data = $req -> except( [ '_token' , 'img' ] );
        $data[ 'img_path' ] = $path;
        $data[ 'img_url' ] = $url;
        // $data[ 'date_end' ] = $data[ 'date_start' ];
        TournamentAnnoucement::insert( $data );
        NotificationHelper::sendAnnouncement( 'tournament_annoucements' , $data );
        // dd($data);
        return redirect() -> route( 'announcements.index' );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TournamentAnnoucement  $tournamentAnnoucement
     * @return \Illuminate\Http\Response
     */
    public function edit(TournamentAnnoucement $tournamentAnnoucement , $id ) {
        $announcement = TournamentAnnoucement::findOrFail( $id );
        $tourCats = TourCat::all();
        return view( 'tournament.announcement.edit' , compact( 'tourCats' , 'announcement' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TournamentAnnoucement  $tournamentAnnoucement
     * @return \Illuminate\Http\Response
     */
    public function update( Request $req, TournamentAnnoucement $tournamentAnnoucement , $id ) {
        $data = $req -> except( [ '_token' , 'img' , '_method' ] );
        if ( isset( $req -> img ) ) {
            $img = $req -> img;
            $url = "/announcementsImages/" . $img -> getClientOriginalName();
            $res = $img -> move( public_path() . "/announcementsImages/" , $img -> getClientOriginalName() );
            $path = $res -> getRealPath();
            $data[ 'img_path' ] = $path;
            $data[ 'img_url' ] = $url;
        }
        // $data[ 'date_end' ] = $data[ 'date_start' ];
        TournamentAnnoucement::whereId( $id ) -> update( $data );
        // dd($data);
        return redirect() -> route( 'announcements.index' );
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TournamentAnnoucement  $tournamentAnnoucement
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        TournamentAnnoucement::findOrFail( $id ) -> delete();
        return redirect() -> route( 'announcements.index' );
    }
}
