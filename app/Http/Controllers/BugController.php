<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BoughtSubscription;
use App\BoughtSeat;
use App\ReservedSeat;
use App\In;
use App\BitrixIn;
use App\Order;
use App\Out;
use App\Seat;
use App\BitrixHistory;
use DB;
use App\Helpers\MysqlHelper;
use App\Helpers\PaymentHelper;
use App\Helpers\BitrixReserveHelper;
use App\Helpers\FixHelper;
use App\Helpers\PushHelper;
use App\Helpers\BitrixSendPaymentHelper;
use App\Helpers\BackupHelper;
use App\Helpers\BitrixCancelReserveHelper;
use App\Helpers\BitrixHistoryHelper;
use App\Helpers\BitrixHelper;
use App\Helpers\Helper;

class BugController extends Controller {
    public function pushIt() {
        $text = "Открыта аренда кортов с 23:00 до 24:00. Ждём Вас!";
        exit($text);
        $users = User::whereNotNull( 'expo_token' )
        -> where( 'temp_push_sent' , 0 )
        // -> whereId( 55 )
         -> take(500) -> get();
        foreach ($users as $key => $user ) {
            $data = [];
            $data[ 'title' ] = "Акция";
            $data[ 'body' ] = $text;
            $data[ 'data' ] = '';
            $userId = $user -> id;
            $res = PushHelper::push( $userId , $data );
            if ( $res === true ) {
                $user -> temp_push_sent = 1;
                $user -> save();
            } else if ( $res === false ) {
                $user -> temp_push_sent = 2;
                $user -> save();
            } else {
                var_dump($res,$user);
                exit;
            }
            // exit( 'done' );
            usleep(500);
        }
        exit( 'done' );
    }
    public function testNewSubscriptions ( ) {
        $order_id = 59954;
        BitrixSendPaymentHelper::send( $order_id );
    }
    // public function fixMailTemp() {
    //     $startDate = "2021-02-26 10:17:29";
    //     $endDate = "2021-02-26 10:33:31";
    //     $ordersNotSent = Order::where( 'paid' , '1' ) -> where( 'price' , '>' , 10 ) -> whereBetween( 'updated_at' , [ $startDate , $endDate ] ) -> get();
    //     foreach ( $ordersNotSent as $key => $order ) {
    //         $orderId = $order -> id;
    //         $mailForFuture = new \App\Mail;
    //         $mailForFuture -> order_id = $orderId;
    //         $mailForFuture -> save();
    //         // dd($orderId);
    //     }
    //     dd($ordersNotSent);
    // }
    // public function fixGuidErrorsOneTime() {
    //     $bubu = 0;
    //     $reee = [];
    //     $query1 = Out::where( 'message' , 'LIKE' , '%newReserve%' ) -> where( 'message' , 'LIKE' , '%guid%' ) -> get();
    //     foreach ( $query1 as $key => $value ) {
    //         if ( strpos( $value -> message , 'guid' ) !== false ) {
    //             $mes = json_decode( $value[ 'message' ] );
    //             $guid = $mes -> guid;
    //             $query2 = BitrixHistory::where( 'data' , 'LIKE' , '%cancelReserve%' ) -> where( 'data' , 'LIKE' , '%' . $guid . '%' ) -> first();
    //             if ( null !== $query2 ) {
    //                 $bubu++;
    //                 if ( $bubu > 1 ) {
    //                     $baba = json_decode( $query2 -> data , 1 );
    //                     Helper::splitToBeCanceledReserve( $baba , 'fixingfeb12' , true );
    //                 }
    //                 $reee[] = $query2 -> data;
    //             }
    //         }
    //     }
    //     dd($reee);
    //     exit( 'end' );
    //     dd($query1);
    // }
    public function token() {
            $token = $user -> createToken( 'MyApp' ) -> accessToken;
            return response() -> json( [ 'token' => $token ] , 200 ); 
    }
    public function clearHistory() {
        BitrixHistoryHelper::clearHistoryu();
    }
    public function fixforosix() {
        echo "<h1>";
        echo date( 'H:i' ) . "<br>";
        $res = Out::whereNotNull( 'fix_forosix' ) -> where( 'fix_forosix_done' , 0 ) -> take( 10 ) -> get();
        $count = Out::whereNotNull( 'fix_forosix' ) -> where( 'fix_forosix_done' , 0 ) -> count();
        foreach ( $res as $key => $value ) {
            usleep( 500000 );
            $val = json_decode( $value -> fix_forosix , 1 );
            // var_dump($key);
            $canCancel = BitrixCancelReserveHelper::canCancelByDataProvided( $val );
            if ( $canCancel ) {
                $resp = BitrixHelper::send( $val , 'cancel_reserve' );
                if ( true == $resp ) {
                    $value -> fix_forosix_done = 1;
                    echo( $key . "<br>" );
                } else {
                    $value -> fix_forosix_done = 4;
                }
                $value -> save();
                // dd($resp,'yep');
            } else {
                $value -> fix_forosix_done = 3;
                $value -> save();
                // dd($value,'nope');
            }
        }
        echo "<br>" . $count;
        echo "<h1>";
        exit();
    }
    // public function cancelReserveByOrder( $order_id ) {
    //     BitrixCancelReserveHelper::cancelReserveByOrderId( $order_id );
    // }
    // public function cancelCheck() {
    //     BitrixCancelReserveHelper::check();
    // }
    // public function getPaymentInfo( $order_id ) {
    //     $res = PaymentHelper::getPaymentInfoStatus( $order_id );
    //     dd($res);
    // }

     protected function sendMail ( $order ) {
        $user = User::findOrFail( $order -> user_id ) -> toArray();
        $finalData = [
            'boughtSubscriptions' => [],
            'boughtSeats' => [],
            'user' => $user,
            'order' => $order
        ];
        $boughtSubscriptions = BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> with( 'subscription' ) -> get() -> toArray();
        if ( null !== $boughtSubscriptions ) {
            foreach ( $boughtSubscriptions as $key => $value ) {
                $finalData[ 'boughtSubscriptions' ][] = $value;
            }
        }
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> with( [ 'arena' , 'court' ] ) -> get() -> toArray();
        if ( null !== $boughtSeats ) {
            foreach ( $boughtSeats as $key => $value ) {
                $finalData[ 'boughtSeats' ][] = $value;
            }
        }
        // dd($finalData);
        \App\Helpers\Mail::sendToAdmin( $finalData );
    }



    public function sendSomeMail( Request $req , $id ) {
        $order = Order::find( $id );
        $this -> sendMail($order);
    }
    public function payStats () {
        $fuckOrders = [];
        $differences = [];
        $totalCount = Order::wherePaid( '1' ) -> count();
        $paidOrders = Order::wherePaid( '1' ) -> where( 'price' , '>' , 500 ) -> whereDate( 'created_at' , '>' , '2020-06-01 22:00:00' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        foreach ( $paidOrders as $key => $order ) {
            $difference = ceil ( ( strtotime( $order[ 'updated_at' ] ) - strtotime( $order[ 'created_at' ] ) ) / 60 );
            $paidOrders[ $key ][ 'timediff' ] = $difference;
            // if ( $key === 1500 ) {
                // dd($difference,$order);
            // }
            // if ( $difference > 10 ) {
                // dd($order,$difference);
            // }
            // if ( $difference > 56 &&  ) dd($order);
            if ( $difference < 1 ) dd($order);
            // if ( $difference > 56 && strpos( $order[ 'updated_at' ] , '22:' ) === false ) dd($order);
            if ( isset( $differences[ $difference ] ) ) {
                $differences[ $difference ]++;
            } else {
                $differences[ $difference ] = 1;
            }
            if ( $difference > 20 ) {
                $fuckOrders[] = $paidOrders[ $key ];
                // dd($order);
            }
        }
        foreach ( $differences as $key => $value ) {
            if ( $value < 1 ) {
                unset( $differences[ $key ] );
            }
        }
        ksort( $differences );
        // dd($fuckOrders);
        return view( 'errors.payStats' , compact( 'differences' , 'totalCount' , 'fuckOrders' ) );
    }
    public function checkWrongCourts() {
        $shit = [];
        $boughtSeats = BoughtSeat::orderBy( 'id' , 'DESC' ) -> offset( 2000 ) -> take( 2000 ) -> get();
        foreach ($boughtSeats as $bb ) {
            $seatRecord = Seat::where([
                'time' => $bb[ 'time' ],
                'arena_id' => $bb[ 'arena_id' ],
                'court_id' => $bb[ 'court_id' ],
            ]) -> firstOrFail();
            if ( $bb->seat_id != $seatRecord->id ) {
                $shit[] = [
                    'seat' => $seatRecord -> toArray(),
                    'bought_seat' => $bb -> toArray(),
                ];
            }
        }
        dd($shit);
    }
    public function test () {
        BackupHelper::backup( true );
        exit;
        $order = PaymentHelper::createPayment( 150 );
        dd($order);
        exit;
        $id = 2618;
            BitrixSendPaymentHelper::send( $id );
            exit;
        $orders = Order::where( 'paid' , 1 ) -> count();
        dd($orders);
        $orders = Order::where( 'paid' , '1' ) -> orderBy( 'id' , 'DESC' ) -> take( 2000 ) -> get();
        foreach ($orders as $key => $value) {
            usleep( 150000 );
            BitrixSendPaymentHelper::send( $value -> id );
        }
    }
    public function compareHistory() {
        $success = 0;
        $successs = 0;
        $bitrixIns = BitrixIn::all();
        // $ins = In::all();
        foreach ( $bitrixIns as $key => $value) {
            $check = In::where( 'message' , '=' , $value -> message ) -> first();
            if ( $check == null ) {
                    dd($value);
            } else {
                $success++;
            }
        }
        dd($success,$successs);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function users () {
    //     exit('done');
    //     $users = User::orderBy( 'id' , 'ASC' ) -> get();
    //     foreach ( $users as $key => $user ) {
    //         if ( $user -> id > 55 ) {
    //             // dd( ( $user -> id ) % 2 == 0 );
    //             BitrixHelper::createUser([
    //                 'name' => $user -> name,
    //                 'phone' => $user -> phone,
    //                 'email' => $user -> email
    //             ]);
    //             if ( ( $user -> id ) % 2 == 0 ) {
    //                 sleep( 1 );
    //             }
    //         }
    //     }
    // }
    public function canceled () {
        FixHelper::fixCancel();
    }
    public function reserved () {
        FixHelper::fixReserve();
    }
    // public function reservesBak () {
    //     $reservedSeats = [];
    //     $bought = ReservedSeat::where( 'paid' , 1 ) -> where( 'created_at' , 'LIKE' , '%2020-05-29%' ) -> where( 'user_id' , '!=' , 0 ) -> take( 10 ) -> get();
    //     foreach ( $bought as $key => $bou ) {
    //         $reservedSeats = [
    //             'created_at' => $bou[ 'created_at' ],
    //             'court_id' => $bou[ 'court_id' ],
    //             'time' => $bou[ 'time' ],
    //             'date' => $bou[ 'date' ],
    //         ];
    //         $user = User::whereId( $bou[ 'user_id' ] ) -> first();
    //         if ( null == $user ) {
    //             // dd($bou);
    //             file_put_contents( __DIR__ . '/reserve_no_user' , json_encode( $bou ) . PHP_EOL , FILE_APPEND );
    //             continue;
    //         }
    //         $phone = $user -> phone;
    //         // dd('asdsd');
    //         $res = BitrixReserveHelper::sendSingle( $reservedSeats , $phone );
    //     }
    //     dd($reservedSeats);
    //     $users = User::orderBy( 'id' , 'ASC' ) -> get();
    //     foreach ( $users as $key => $user ) {
    //         if ( $user -> id > 55 ) {
    //             // dd( ( $user -> id ) % 2 == 0 );
    //             BitrixHelper::createUser([
    //                 'name' => $user -> name,
    //                 'phone' => $user -> phone,
    //                 'email' => $user -> email
    //             ]);
    //             if ( ( $user -> id ) % 2 == 0 ) {
    //                 sleep( 1 );
    //             }
    //         }
    //     }
    // }
    public function getMyUserTests() {
        $userId = '55';
        $data = [
            'subscriptions' => [],
            'seats' => [],
            'user' => [],
        ];
        $user = User::findOrFail( $userId );
        $boughtSubscriptions = BoughtSubscription::whereUserId( $userId ) -> where( 'paid' , '1' ) -> with( 'subscription' ) -> where( 'created_at' , '>' , '2021-01-01 00:00:00' ) -> get();
        $boughtSeats = BoughtSeat::whereUserId( $userId ) -> where( 'paid' , '1' ) -> with( [ 'arena' , 'court' ] ) -> where( 'created_at' , '>' , '2021-01-01 00:00:00' ) -> get();

        foreach ( $boughtSubscriptions as $key => $sub ) {
            $data[ 'subscriptions' ][] = [
                'subTitle' => $sub -> subscription -> title,
                'profile_name' => $sub -> subscription -> profile_name,
                'date_created' => date( 'Y-m-d H:i:s' , strtotime( $sub -> created_at ) ),
            ];
        }
        foreach ( $boughtSeats as $key => $seat ) {
            $data[ 'seats' ][] = [
                'date_created' => date( 'Y-m-d H:i:s' , strtotime( $seat -> created_at ) ),
                'date' => date( 'Y-m-d' , strtotime( $seat -> date ) ) . ' ' . $seat -> time,
                'court' => $seat -> court -> title,
                'arena' => $seat -> arena -> title,
            ];
        }
        $data[ 'user' ] = $user;
        return view( 'getMyUserTests' , $data );
        // dd($data);
        // dd($boughtSubscriptions);
    }
}
