<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Card;
use App\Helpers\BitrixHelper;

class SettingController extends Controller
{

    public function removeCard ( $cardId ) {
        Card::whereId( $cardId ) -> where( 'user_id' , Auth::user() -> id ) -> delete();
        $cards = Card::where([
            'user_id' => Auth::user() -> id
        ]) -> whereNotNull( 'token' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $cards );
    }

    public function cards () {
        $cards = Card::where([
            'user_id' => Auth::user() -> id
        ]) -> whereNotNull( 'token' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $cards );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function maintenance () {
        return response() -> json([ 'error' => 'Функция временно недоступна. Пожалуйста, попробуйте позже.' ]);
    }
    public function checkEmailChangeCode ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'code' => 'required|numeric|digits:4',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        if ( $req -> code == $user -> verify_code ) {
            $user -> email = strtolower( $req -> email );
            $res = BitrixHelper::updateUser( $user );
            if ( ! $res ) {
                return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
            }
            $user -> save();
            return response() -> json( [ 'status' => 'ok' ] );
        } else {
            return response() -> json( [ 'error' => __( 'Wrong code' ) ] , 422 );
        }
    }

    public function requestEmailChangeCode ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'email' => 'required|email|unique:users',
        ]);
        $email = $req -> email;
        $code = rand( 1000 , 9999 );
        \App\Helpers\Mail::sendMail( 'Код для смены почты:' , $code , $email );
        User::whereId( Auth::user() -> id ) -> update([ 'verify_code' => $code ]);
        return response() -> json( [ 'status' => 'ok' ] );
    }
    public function changeName( Request $req ) {

        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'name' => 'required|max:15|min:4',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        $user -> name = $req -> name;
        $res = BitrixHelper::updateUser( $user );
        if ( ! $res ) {
            return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
        }
        $user -> save();
        return response() -> json( [ 'status' => 'ok' ] );
    }
    public function changePhone( Request $req ) {
        
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'phone' => 'required|string|min:17|max:18',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        $check = User::wherePhone( $req -> phone ) -> first();
        if ( null !== $check ) {
            return response() -> json( [ 'error' => 'Пользователь с таким номером телефона уже существует' ] , 422 );
        }
        $oldPhone = $user -> phone;
        $user -> phone = $req -> phone;
        $res = BitrixHelper::updateUser( $user , $oldPhone );
        if ( ! $res ) {
            return response() -> json( [ 'error' => 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
        }
        $user -> save();
        return response() -> json( [ 'status' => 'ok' ] );
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
