<?php
    namespace App\Http\Controllers\Api;
    use App\User; 
    use Auth; 
    // use Validator;
    use Cookie;
    use Hash;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use App\Http\Requests\Api\RegisterRequest;
    use App\Http\Requests\Api\RequestCodeRequest;
    use App\Http\Requests\Api\LoginRequest;
    use App\Http\Requests\Api\VerifyCodeRequest;
    use App\Http\Requests\Api\ResetPasswordRequest;
    use App\Http\Requests\Api\ResetPasswordAuthRequest;
    use App\Http\Requests\Api\StoreResetPasswordRequest;
    use App\Http\Requests\Api\BalanceRequest;
    use App\Http\Requests\Api\HelpRequest;
    use App\Ticket;
    use App\Helpers\BitrixHelper;
    use App\Helpers\LogHelper;

    class UserController extends Controller {
        public function register ( RegisterRequest $req ) {
            // file_put_contents( __DIR__ . '/register.json' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
            $input = $req -> only( [ 'name' , 'email' , 'password' , 'phone' ] );
            $check = User::whereEmail( $req -> email ) -> first();
            if ( $check !== null ) {
                return response() -> json( [ 'error' => 'Пользователь с такой эл. почтой зарегистрирован' ] , 422 );
            }
            $check = User::wherePhone( $req -> phone ) -> first();
            if ( $check !== null ) {
                return response() -> json( [ 'error' => 'Пользователь с таким номером телефона зарегистрирован' ] , 422 );
            }
            $input[ 'email' ] = strtolower( $input[ 'email' ] );
            $res = BitrixHelper::createUser( $input );
            if ( ! $res ) {
                return response() -> json( [ 'error' => 'Произошла ошибка синхронизации с базой 1С, пожалуйста попробуйте позже' ] , 422 );
            }
            $input[ 'password' ] = bcrypt ( $input[ 'password' ] );
            $input[ 'refCode' ] = rand( 10000 , 99999 );
            // $check = User::wherePhone( $input[ 'phone' ] ) -> count();
            $user = User::create( $input );
            $token = $user -> createToken( 'MyApp' ) -> accessToken;
            return response() -> json( [ 'token' => $token ] , 200 ); 
        }
        public function requestCode ( RequestCodeRequest $req ) {
            $phone = $req -> phone;
            $code = rand( 1000 , 9999 );
            $res = \App\Helpers\SMS::sendSMS( 'Ваш код регистрации:' , $code , $phone );
            if ( $res !== false ) {
                User::where( 'phone' , $phone ) -> update([ 'verify_code' => $code ]);
                return response() -> json( [ 'status' => 'ok' ] );
            } else {
                return response() -> json( [ 'error' => 'Произошла ошибка, пожалуйста проверьте номер телефона' ] , 422 );
            }
        }
        public function checkCode ( VerifyCodeRequest $req ) {
            $phone = $req -> phone;
            $user = User::wherePhone( $phone ) -> firstOrFail();
            if ( $user -> verify_code != $req -> code && $user -> forgot_password_code != $req -> code ) {
                return response() -> json( [ 'error' => __( 'Wrong code' ) ] , 422 );
            } else {
                $user -> verified = 1;
                $user -> save();
                $token = $user -> createToken( 'MyApp' ) -> accessToken;
                return response() -> json( [ 'token' => $token ] , 200 ); 
            }
        }

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function login ( LoginRequest $req ) {
            // file_put_contents(__DIR__.'/login', json_encode($req->all()));
            if ( Auth::attempt([ 'phone' => $req -> phone , 'password' => $req -> password ]) ) { 
                $user = Auth::user();
                if ( $user -> verified != 1 ) {
                    return response() -> json( [ 'error' => 'Not verified' ] , 302 );
                }
                $token = $user -> createToken( 'MyApp' ) -> accessToken;
                return response() -> json( [ 'token' => $token ] , 200 ); 
            } else {
                return response() -> json( [ 'error' => __( 'Wrong credentials' ) ] , 422 );
            } 
        }
        public function resetPassword ( ResetPasswordRequest $req ) {
            $code = rand( 1000 , 9999 );
            $phone = $req -> phone;
            $res = \App\Helpers\SMS::sendSMS( 'Ваш код:' , $code , $phone );
            if ( $res !== false ) {
                User::where( 'phone' , $phone ) -> update([ 'forgot_password_code' => $code ]);
                return response() -> json( [ 'status' => 'ok' ] );
            } else {
                return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
            }
                // return response() -> json( [ 'error' => $req -> email ] , 422 );
            // User::where( 'email' , $req -> email ) -> update( [ 'forgot_password_code' => $code ] );
            // \App\Helpers\Mail::sendMail( 'Ваш код:' , $code , $req -> email );
            // Cookie::queue( 'email' , $req -> email , 43200 );
            // return response() -> json( [ 'status' => 'ok' ] );
        }

        public function resetPasswordAuth ( ResetPasswordAuthRequest $req ) {
            // $code = rand( 1000 , 9999 );
            // exit(Auth::user() -> id .'asdsd');
            $password = Hash::make( $req -> old_password );
            if ( 1 || Hash::check( $password , Auth::user() -> password ) ) {
                User::whereId( Auth::user() -> id ) -> update( [ 'password' => Hash::make( $req -> new_password ) ] );
                return response() -> json( [ 'status' => 'ok' ] );
            } else {
                return response() -> json( [ 'error' => [ __( 'Wrong old password' ) ] ] , 401 );
                // return back() -> withErrors( [ 'some_error' ], 401 );
            }
        }


        public function checkForgotPasswordCode ( VerifyCodeRequest $req ) {
            // $email = Cookie::get( 'email' );
                // return response() -> json( [ 'error' => $email ] , 422 );
            $email = $req -> email;
            $user = User::where( 'email' , $email ) -> firstOrFail();
            if ( $user -> forgot_password_code != $req -> code ) {
                return response() -> json( [ 'error' => __( 'Wrong code' ) ] , 422 );
            } else {
                // User::where( 'id' , $user -> id ) -> update([ 'verified' => 1 ]);
                return response() -> json( [ 'status' => 'ok' ] );
            }
        }
        public function storeResetPassword ( StoreResetPasswordRequest $req ) {
            $password = bcrypt( $req -> password );
            $phone = $req -> phone;
            User::where( 'phone' , $phone ) -> update([ 'password' => $password ]);
            return response() -> json( [ 'status' => 'ok' ] );
        }
        public function balance() {
            $user = Auth::user();
            if ( null !== $user ) {
                return response() -> json( [ 'balance' => $user -> balance + 0 ] );
            } else {
                return response() -> json( [ 'error' => 'Unauthorized' ] , 422 );
            }
        }
        public function refCode() {
            $user = Auth::user();
            if ( null !== $user ) {
                return response() -> json( [ 'refCode' => $user -> refCode ] );
            } else {
                return response() -> json( [ 'error' => 'Unauthorized' ] , 422 );
            }
        }
        public function requestBalance ( BalanceRequest $req ) {
            $user = Auth::user();
            if ( null !== $user ) {
                User::where( 'id' , $user -> id ) -> update([ 'balance' => $user -> balance + $req -> balance ]);
                return response() -> json( [ 'status' => 'ok' ] );
            } else {
                return response() -> json( [ 'error' => 'Not authorized' ] , 422 );
            }

        }
        public function help ( HelpRequest $req ) {
            $user = Auth::user();
            if ( null !== $user ) {
                $ticket = $req -> only( [ 'subject' , 'message' ] );
                $ticket[ 'user_id' ] = $user -> id;
                $ticket = Ticket::create( $ticket );
                return response() -> json( [ 'status' => 'ok' ] );
            } else {
                return response() -> json( [ 'error' => 'Unauthorized' ] , 422 );
            }

        }
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
