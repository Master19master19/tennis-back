<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\PersonalTrainingRequest;
use App\Http\Requests\Api\PersonalChildTrainingRequest;

use App\PersonalTrainingRequest as PRequest;
use App\PersonalChildTrainingRequest as PCRequest;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function personalTrainingOrder ( PersonalTrainingRequest $req ) {
        $data = $req -> only( [ 'name' , 'phone' , 'email' , 'additional' ] );
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        PRequest::insert( $data );
        \App\Helpers\Mail::sendpersonalTrainingOrder( $data );
        return response() -> json([]);
    }
    public function personalChildTrainingOrder ( PersonalChildTrainingRequest $req ) {
        $data = $req -> only( [ 'child_name' , 'phone_number' , 'parent_name' , 'child_birthday' , 'additional' , 'level' ] );
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        \App\Helpers\Mail::sendpersonalChildTrainingOrderToAdmin( $data );
        PCRequest::insert( $data );
        return response() -> json([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
