<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\BoughtSeat;
use App\ReservedSeat;
use App\Seat;
use App\BoughtSubscription;
use App\Subscription;
use App\Order;
use App\User;
use App\Card;
use Auth;
use App\Helpers\PaymentHelper;
use App\Helpers\BitrixSendPaymentHelper;
use App\Helpers\BitrixCancelReserveHelper;
use App\Helpers\BitrixReserveHelper;
use App\AppPayment;
use App\Helpers\Reserve\ReserveHelper;
use App\Helpers\Subscription\SubscriptionHelper;

class PaymentController extends Controller {
    protected $payment_description = 'Оплата корта(-ов) ';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function addCard ( Request $req ) {
        $totalPrice = env( 'ADD_CARD_PRICE' );
        $orderUniqueId = uniqid();
        $order = Order::create([
            'unique' => $orderUniqueId,
            'user_id' => Auth::user() -> id,
        ]);
        $response = PaymentHelper::createPaymentNew( $totalPrice , Auth::user() -> phone , true );
        // $card = Card::create([
        //     'user_id' => Auth::user() -> id,
        //     'order_id' => $order -> id,
        // ]);
        Order::find( $order -> id ) -> update([
            'price' => $totalPrice,
            'key' => $response[ 'payment_id' ]
        ]);
        return response() -> json( $response );
    }



    public function payCartAllNew ( Request $req ) {
        // if ( Auth::user() -> id !== 55 ) {
    	
		if ( env( 'BITRIX_DOWN' ) == true ) {
            return response() -> json( [ 'error' => 'Ведутся технические работы, приложение временно не принимает оплату. Пожалуйста, попробуйте позже.' ]);
        }
        // file_put_contents( __DIR__ . '/aaa' , json_encode( $req->all()));
        $this -> log( $req -> all() );
        $seats = $req -> cartTotal[ 'seats' ] ?? [];
        $subscriptions = $req -> cartTotal[ 'subscriptions' ];
        $totalPrice = 0;
        $orderUniqueId = uniqid();
        $order = Order::create([
            'unique' => $orderUniqueId,
            'user_id' => Auth::user() -> id,
            'card_id' => $req -> cardId
        ]);

        // exit;
        // dd($seats);
        if ( count( $seats ) ) {
            // ReserveHelper::checkReservedBatch( $seats , Auth::user() -> id );
            $errors = $this -> checkIfErrorSeats( $seats );
            if ( $errors === 'wait_20_minutes' ) {
                return response() -> json( [ 'error' => $this -> getError( 'wait_20_minutes' ) ] );
            }
            if ( $errors === 'date_not_permitted' ) {
                return response() -> json( [ 'error' => $this -> getError( 'date_not_permitted' ) ] );
            }

            if ( $errors === 'date_expired' ) {
                return response() -> json( [ 'error' => $this -> getError( 'date_expired' ) ] );
            }
            if ( $errors === 555554444 ) {
                return response() -> json( [ 'error' => 'Неправильные данные, пожалуйста обратитесь в техническую поддержку' ] );
            }
            if ( $errors === 789789765465 ) {
                return response() -> json( [ 'error' => 'Убедитесь, что Вы только что не оплачивали данный заказ.
Если Вы уже оплатили заказ, то в течение 20 минут забронированные корты и купленные абонементы отобразятся в приложении у Вас в профиле.' ] );
            }
            if ( is_array( $errors ) && count( $errors ) ) {
                foreach ( $errors as $key => $error ) {
                    if ( isset( $error[ 'old_reserve' ] ) ) {
                        BitrixCancelReserveHelper::cancelReserve( $error[ 'old_reserve' ] , Auth::user() );
                        unset( $errors[ $key ] );
                    }
                }
                if ( count( $errors ) ) {
                    return response() -> json( [ 'error' => $this -> getError( 'bought' , $errors ) ] );
                }
            }
            $handleResp = $this -> handleSeats( $seats , $order -> id , true );
            if ( $handleResp == 'reserve_try_again' ) {
                file_put_contents(env('LOG_PATH') . '/reserve_try_again1' , 'data' . PHP_EOL , FILE_APPEND );
                return response() -> json( [ 'error' => $this -> getError( 'reserve_try_again' , $errors ) ] );
            }
            if ( $handleResp == 190909090 ) {
                return response() -> json( [ 'error' => 'К сожалению в заданном промежутке времени уже есть забронированные занятия' ] );
            }
            if ( $handleResp == 290909090 ) {
                return response() -> json( [ 'error' => 'Неправильные данные' ] );
            }
            
            if ( $handleResp == 'reserved_already' ) {
                return response() -> json( [ 'error' => $this -> getError( 'reserved_already' ) ] );
            }
            $totalPrice += $handleResp;
        }

        // DO NOT MOVE
        if ( count( $subscriptions ) ) {
            $totalPrice += $this -> handleSubscriptions( $subscriptions , $order -> id );
        }
        // DO NOT MOVE

        if ( $req -> cardId != 0 ) {
            Order::find( $order -> id ) -> update([
                'price' => $totalPrice,
            ]);
            $response = PaymentHelper::payWithCard( $totalPrice , Auth::user() -> phone , $req -> cardId , $order -> id , Auth::user() -> email );
            if ( $response ) {
                // $this -> orderSuccess( $order -> id );
                return response() -> json( [ 'done' => 1 ] );
            } else {
                return response() -> json( [ 'error' => $this -> getError( 'card_invalid' ) ] );
            }
        } else {
            $response = PaymentHelper::createPayment( $totalPrice , Auth::user() -> phone , 'Оплата корта(-ов)' , Auth::user() -> email );
            Order::find( $order -> id ) -> update([
                'price' => $totalPrice,
                'key' => $response[ 'payment_id' ]
            ]);
        }
        return response() -> json( $response );
    }




    public function index ( Request $req ) {
        $response = PaymentHelper::createPayment( 10 );
        return $response;
    }
    public function loading( Request $req ) {
        file_put_contents( env( 'LOG_PATH' ) . '/loading.json' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        return view( 'loading' );
    }
    
    public function testMail ( Request $req ) {
        $order = Order::where( [ 'id' => $req -> id ] ) -> firstOrFail();
        $this -> sendMail( $order );
    }
    
    protected function sendMail ( $order ) {
        $user = User::findOrFail( $order -> user_id ) -> toArray();
        $finalData = [
            'boughtSubscriptions' => [],
            'boughtSeats' => [],
            'user' => $user,
            'order' => $order
        ];
        $boughtSubscriptions = BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> with( 'subscription' ) -> get() -> toArray();
        if ( null !== $boughtSubscriptions ) {
            foreach ( $boughtSubscriptions as $key => $value ) {
                $finalData[ 'boughtSubscriptions' ][] = $value;
            }
        }
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> with( [ 'arena' , 'court' ] ) -> get() -> toArray();
        if ( null !== $boughtSeats ) {
            foreach ( $boughtSeats as $key => $value ) {
                $finalData[ 'boughtSeats' ][] = $value;
            }
        }
        // dd($finalData);
        \App\Helpers\Mail::sendToAdmin( $finalData );
    }



    public function kassaNotify ( Request $req ) {
        $response = $req -> object;
        file_put_contents( env( 'LOG_PATH' ) . '/kassa_logs/payments.' . date( 'm_d' ) , json_encode( [ $req -> all() , date( 'Y-m-d H:i:s' ) ] ) .PHP_EOL , FILE_APPEND );
        // file_put_contents( __DIR__.'/kassa.jkz' , json_encode( [$response,date('Y-m-d H:i:s')] ) .PHP_EOL , FILE_APPEND );
        // return;
        if ( ! isset( $response[ 'paid' ] ) ) {
            file_put_contents( env( 'LOG_PATH' ) . '/kassa_logs/refunds_maybe.' . date( 'm_d' ) , json_encode( [ $req -> all() , date( 'Y-m-d H:i:s' ) ] ) .PHP_EOL , FILE_APPEND );
            $log = $response[ 'refunda' ];
            return;
        }
        if ( $response[ 'paid' ] == true ) {
            $order = Order::where( [ 'key' => $response[ 'id' ] ] ) -> first();
            if ( $order == null ) {
                file_put_contents( env( 'LOG_PATH' ) . '/order_not_found.log' , json_encode( [ $response[ 'id' ] , $req -> all() , date( 'Y-m-d H:i:s' ) ] ) . PHP_EOL , FILE_APPEND );
                return response() -> json([ 'status' => false ] , 404 );
            }
            if ( $order -> paid == '1' ) {
                file_put_contents( env( 'LOG_PATH' ) .'/already.paid' , json_encode( [ $response , date( 'Y-m-d H:i:s' ) ] ) . PHP_EOL , FILE_APPEND );
                return;
            } 
            $order -> paid = '1';
            $order -> save();
            if ( $response[ 'amount' ][ 'value' ] == env( 'ADD_CARD_PRICE' )  ) {
                $this -> saveCardIfNeeded( $response , $order );
            } else {
                $this -> orderSuccess( $order -> id );
                $this -> saveCardIfNeeded( $response , $order );
            }
        } else if ( $response[ 'paid' ] == false ) { // hahahaaaaa , its a boolean , but wait , its programming, everything is possible )))))))
            if ( $response[ 'amount' ][ 'value' ] > env( 'ADD_CARD_PRICE' )  ) {
                $order = Order::where( [ 'key' => $response[ 'id' ] ] ) -> first();
                if ( $order == null ) {
                    file_put_contents( env( 'LOG_PATH' ) . '/order_not_found_cancel.log' , json_encode( [ $response[ 'id' ] , $req -> all() , date( 'Y-m-d H:i:s' ) ] ) . PHP_EOL , FILE_APPEND );
                    return response() -> json([ 'status' => false ] , 404 );
                }
                BitrixCancelReserveHelper::cancelReserveByOrderId( $order -> id );
            }
        }
        return response()->json( [ 'status' => 'success' ] , 200 );
    }


    public function kassaNotifyBak ( Request $req ) {
        $response = $req -> object;
        file_put_contents( env( 'LOG_PATH' ) .'/payments.json' , json_encode( [ date( 'Y-m-d H:i:s' ) , $response ]) .PHP_EOL , FILE_APPEND );
        // file_put_contents( env( 'LOG_PATH' ) .'/kassa.k' , json_encode( [$response[ 'id' ],
            // $response[ 'paid' ],date('Y-m-d H:i:s')] ) .PHP_EOL , FILE_APPEND );
        if ( $response[ 'paid' ] == true ) {
            $order = Order::where( [ 'key' => $response[ 'id' ] ] ) -> firstOrFail();
            $order -> paid = '1';
            $order -> save();
            BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
            BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
            $this -> sendMail( $order );
            BitrixSendPaymentHelper::send( $order -> id );
        } else if ( $response[ 'paid' ] == false ) { // hahahaaaaa , its a boolean , but wait , its programming, everything is possible )))))))
            $order = Order::where( [ 'key' => $response[ 'id' ] ] ) -> firstOrFail();
            BitrixCancelReserveHelper::cancelReserveByOrderId( $order -> id );
        }
        return response()->json( [ 'status' => 'success' ] , 200 );
    }


    protected function orderSuccess ( $orderId ) {
        $order = Order::where( [ 'id' => $orderId ] ) -> firstOrFail();
        // if ( $order -> paid == '1' ) {
            // file_put_contents( __DIR__.'/already.paid' , json_encode( [$response,date('Y-m-d H:i:s')] ) .PHP_EOL , FILE_APPEND );
            // return;
        // } 
        BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
        BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
        BitrixSendPaymentHelper::send( $order -> id , $order -> key );
        $this -> sendMail( $order );
    }



    protected function saveCardIfNeeded ( $response , $order ) {
        if ( isset( $response[ 'payment_method' ] ) && $response[ 'payment_method' ][ 'saved' ] == true ) {
            $paymentMethod = $response[ 'payment_method' ];
            $title = $paymentMethod[ 'card' ][ 'first6' ] . "******" . $paymentMethod[ 'card' ][ 'last4' ];
            try {
                $card = Card::create([
                    'user_id' => $order -> user_id,
                    'order_id' => $order -> id,
                    'token' => $paymentMethod[ 'id' ],
                    'title' => $title,
                ]);
            } catch ( \Exception $e ) {
                file_put_contents( env( 'LOG_PATH' ) . '/card_exists.json' , json_encode( $e -> getMessage() ) . PHP_EOL , FILE_APPEND );
                return true;
            }
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    protected function handleSeats ( $seats , $orderUniqueId , $isNew = false ) {
        $totalPrice = 0;
        $couldNotBuy = false;
        $allSeats = [];
        $reservedSeats = [];
        foreach ( $seats as $weirdKey => $seat ) {
            // if( in_array( $seat[ 'courtId' ] , [ 31,32,33,34])) {
            //     exit( json_encode( ['error'=>'Этот корт временно недоступен для бронирования']) );
            // }
            $seatRecord = Seat::where([
                'time' => $seat[ 'time' ],
                'id' => $seat[ 'id' ],
                'arena_id' => $seat[ 'arenaId' ],
                'court_id' => $seat[ 'courtId' ],
            ]) -> firstOrFail();
            $isBought = BoughtSeat::where([
                'court_id' => $seat[ 'courtId' ],
                'time' => $seat[ 'time' ],
                'arena_id' => $seat[ 'arenaId' ],
                'seat_id' => $seat[ 'id' ],
                'date' => $seat[ 'date' ],
                'paid' => 1
            ]) -> count();
            if ( $isBought ) {
                $couldNotBuy = true;
                continue;
            }
            if ( ! $isNew ) {
                $price = $seatRecord -> price;
                if ( date( 'N' , strtotime( $seat[ 'date' ] ) ) >= 6 ) {
                    $price = $seatRecord -> price_weekend;
                }
            } else {
                $dateTitle = strtolower( date( 'l' , strtotime( $seat[ 'date' ] ) ) );
                $price = $seatRecord[ $dateTitle ];
            }
            $allSeats[] = [
                'order_id' => $orderUniqueId,
                'court_id' => $seat[ 'courtId' ],
                'time' => $seat[ 'time' ],
                'price' => $price,
                'arena_id' => $seat[ 'arenaId' ],
                'seat_id' => $seat[ 'id' ],
                'user_id' => Auth::user() -> id,
                'date' => $seat[ 'date' ],
                'ip' => $_SERVER[ 'REMOTE_ADDR' ],
                'created_at' => date( 'Y-m-d H:i:s' ),
                'updated_at' => date( 'Y-m-d H:i:s' ),
                // 'paid' => 1 // toDelete
            ];
            $reservedSeats[] = [
                'court_id' => $seat[ 'courtId' ],
                'time' => $seat[ 'time' ],
                'arena_id' => $seat[ 'arenaId' ],
                'seat_id' => $seat[ 'id' ],
                'user_id' => Auth::user() -> id,
                'date' => $seat[ 'date' ],
                'ip' => $_SERVER[ 'REMOTE_ADDR' ],
                'created_at' => date( 'Y-m-d H:i:s' ),
                'updated_at' => date( 'Y-m-d H:i:s' ),
                'reserved_until' => strtotime( env( 'RESERVE_TIME' ) )
                // 'paid' => 1 // toDelete
            ];
            $totalPrice += ( float ) $price;
            if ( strlen( $seat[ 'courtTitle' ] ) > 5 && strlen( $seat[ 'date' ] ) > 3 && strlen( $seat[ 'time' ] ) > 3 ) {
                $this -> payment_description = $seat[ 'courtTitle' ] . ' ' . $seat[ 'date' ] . ' ' . $seat[ 'time' ];
            }
        }
        if ( $totalPrice <= 100 ) {
            return 290909090;
        }
        // file_put_contents( __DIR__ . '/bbbb' , json_encode($reservedSeats));
        $res = BitrixReserveHelper::send( $reservedSeats );
        // $res = true;
        if ( null === $res ) {
            return 'reserve_try_again';
        }
        if ( ! $res ) {
            return 190909090;
        }
        ReservedSeat::insert( $reservedSeats );
        // file_put_contents( __DIR__ . '/bbbb' , json_encode($reservedSeats));
        BoughtSeat::insert( $allSeats );
        return $totalPrice;
    }
    protected function handleSubscriptions ( $subscriptions , $orderUniqueId ) {
        $totalPrice = 0;
        $allSubscriptions = [];
        foreach ( $subscriptions as $weirdKey => $subscription ) {
            if ( isset( $subscription[ 0 ] ) ) {
                $subscription_id = $subscription[ 0 ][ 'id' ]; // single
            } else {
                $subscription_id = $subscription[ 'id' ]; // backward compatibility
            }
            $subscriptionRecord = Subscription::whereId( $subscription_id ) -> firstOrFail(); // single
            $price = $subscriptionRecord -> price;
            $current_data = [
                'order_id' => $orderUniqueId,
                'price' => $price,
                'subscription_id' => $subscription_id,
                'user_id' => Auth::user() -> id,
                'created_at' => date( 'Y-m-d H:i:s' ),
                'updated_at' => date( 'Y-m-d H:i:s' ),
                // 'paid' => 1 // toDelete
            ];
            $current_data[ 'profile_name' ] = Auth::user() -> name;
            $current_data[ 'profile_birthday' ] = "01-01-2010";
            if ( isset( $subscription[ 0 ] ) && isset( $subscription[ 0 ][ 'profile' ] ) && isset( $subscription[ 0 ][ 'profile' ][ 'name' ] ) ) {
                $current_data[ 'profile_name' ] = $subscription[ 0 ][ 'profile' ][ 'name' ];
            } 
            if ( isset( $subscription[ 0 ] ) && isset( $subscription[ 0 ][ 'profile' ] ) && isset( $subscription[ 0 ][ 'profile' ][ 'birthday' ] ) ) {
                $current_data[ 'profile_birthday' ] = substr( $subscription[ 0 ][ 'profile' ][ 'birthday' ] , 0 , 10 );
            } 
            if ( isset( $subscription[ 0 ] ) && isset( $subscription[ 0 ][ 'type' ] ) ) {
                $current_data[ 'type' ] = $subscription[ 0 ][ 'type' ];
            } 
            $allSubscriptions[] = $current_data;
            $totalPrice += ( float ) $price;
        }
        BoughtSubscription::insert( $allSubscriptions );
        return $totalPrice;
    }
    private function checkDatePermitted ( $dateToCheck ) {
        $endNextMonth = date( 'Y-m-t' , strtotime( "+1 month" ) );
        $endCurrMonth = date( 'Y-m-t' , strtotime( "+1 minute" ) );
        $endingDatePermitted = $endNextMonth;
        $todayDate = date( 'd' );
        $todayHour = date( 'H' );
        if ( $todayDate < env( 'RESERVE_OPENING_MONTH_DAY' ) ) {
            $endingDatePermitted = $endCurrMonth;
        } else if ( $todayDate == env( 'RESERVE_OPENING_MONTH_DAY' ) && $todayHour < 10 ) {
            $endingDatePermitted = $endCurrMonth;
        }
        return strtotime( $dateToCheck ) <= strtotime( $endingDatePermitted );
    }
    private function checkDateNotExpired ( $seat ) {
        $currentDate = date( 'Y-m-d' );
        $currentHour = date( 'H' );
        $seatDate = date( 'Y-m-d' , strtotime( $seat[ 'date' ] ) );
        $seatHour = date( 'H' , strtotime( $seat[ 'time' ] ) );
        if ( $seatDate < $currentDate ) {
            return false;
        } else if ($seatDate == $currentDate ) {
            if ( $seatHour < $currentHour ) {
                return false;
            }
        }
        return true;
    }
    private function checkIfErrorSeats ( $seats ) {
        $errors = [];
        foreach ( $seats as $weirdKey => $seat ) {
            // $errors[] = $seat;continue;
            // file_put_contents( __DIR__ . '/dat' , json_encode([
            //     'dateseat' => $seat[ 'date' ],
            // ]));
            if ( $this -> checkDatePermitted( $seat[ 'date' ] ) === false ) {
                // 1.date not permitted
                file_put_contents( env( 'LOG_PATH' ) . '/date_not_permitted.error.log' , json_encode( $seats ) . PHP_EOL , FILE_APPEND );
                return 'date_not_permitted';
            }
            if ( $this -> checkDateNotExpired( $seat ) === false ) {
                // 2.date is expired
                file_put_contents( env( 'LOG_PATH' ) . '/date_expired.error.log' , json_encode( $seats ) . PHP_EOL , FILE_APPEND );
                return 'date_expired';
            }
            $seatRecord = Seat::where([
                'time' => $seat[ 'time' ],
                'id' => $seat[ 'id' ],
                'arena_id' => $seat[ 'arenaId' ],
                'court_id' => $seat[ 'courtId' ],
            ]) -> first();
            if ( null == $seatRecord ) {
                // 3.no such seat
                return 555554444;
            }

            $isBuying = BoughtSeat::where([
                'court_id' => $seat[ 'courtId' ],
                'time' => $seat[ 'time' ],
                'arena_id' => $seat[ 'arenaId' ],
                'seat_id' => $seat[ 'id' ],
                'date' => $seat[ 'date' ],
                'user_id' => Auth::user() -> id,
            ]) -> orderBy( 'id' , 'DESC' ) -> first();
            if ( $isBuying != null ) {
                // 4.user reserved , but 20 minutes didnt pass
                if ( strtotime( $isBuying -> created_at ) - strtotime( '-20 minutes' ) > 0 ) {
                    return 789789765465;
                }
            }
            $isBought = BoughtSeat::where([
                'court_id' => $seat[ 'courtId' ],
                'time' => $seat[ 'time' ],
                'arena_id' => $seat[ 'arenaId' ],
                'seat_id' => $seat[ 'id' ],
                'date' => $seat[ 'date' ],
                'paid' => 1
            ]) -> count();
            if ( $isBought || $seatRecord -> price <= 100 ) {
                // 5.bought by someone
                // 3.1.Seat price is wrong
                $errors[] = $seat;
            } else {
                $isReserved = ReservedSeat::where([
                    'court_id' => $seat[ 'courtId' ],
                    'time' => $seat[ 'time' ],
                    'arena_id' => $seat[ 'arenaId' ],
                    'seat_id' => $seat[ 'id' ],
                    'date' => $seat[ 'date' ],
                ]) -> first();
                if ( $seat[ 'price' ] < 50 ) {
                    // 3.1.Seat price is wrong
                    $errors[] = $seat;
                } else if ( null !== $isReserved ) {
                // 4.user reserved , but 20 minutes PASSED?
                    if ( $isReserved -> user_id == Auth::user() -> id ) {
                        $errors[] = [ 'old_reserve' => $isReserved ];
                    }
                    // 6.other user reserved
                    if ( $isReserved -> reserved_until - time() > 0 && $isReserved -> user_id != Auth::user() -> id ) {
                        $errors[] = $seat;
                    }
                }
            }
        }
        return $errors;
    }
    protected function log ( $input ) {
        if ( isset( $input[ 'cartTotal' ] ) && count( $input[ 'cartTotal' ][ 'seats' ] ) ) {
            $log = new AppPayment;
            $log -> message = json_encode( $input );
            $log -> user_id = Auth::user() -> id;
            $log -> save();
        }
    }
    

    public function showPayment ( Request $req , $confirmation_token ) {
        return view( 'payment.index' , [ 'confirmation_token' => $confirmation_token , 'isCard' => false ] );
    }

    public function showPaymentCard ( Request $req , $confirmation_token ) {
        return view( 'payment.index' , [ 'confirmation_token' => $confirmation_token , 'isCard' => true ] );
    }
    public function showPaymentTest() {
        $response = PaymentHelper::createPayment( 2 , '+7 (111) 111-11-11' , "test" , 'vmxitaryan@gmail.com'  );
        // file_put_contents( __DIR__ . '/asd' , $response[ 'payment_url' ] );
        // Order::find( $order -> id ) -> update([
        //     'price' => $totalPrice,
        //     'key' => $response[ 'payment_id' ]
        // ]);
        $ur = $response[ 'payment_url' ];
        return \Redirect::away( $ur );
        exit;
        return response() -> json( $response );
    }
    public function showPaymentTestNew () {
        $confirmation_token = PaymentHelper::createPaymentNew(1)['confirmation_token'];
        return view( 'payment.test' , [ 'confirmation_token' => $confirmation_token , 'isCard' => false ] );
    }

    public function payCartNew ( Request $req ) {
        return $this -> payCart( $req , true );
    }
    public function payCart ( Request $req , $isNew = false ) {

        if ( env( 'BITRIX_DOWN' ) == true ) {
            return response() -> json( [ 'error' => 'Ведутся технические работы, приложение временно не принимает оплату. Пожалуйста, попробуйте позже.' ]);
        }
        $this -> log( $req -> all() );
        $seats = $req -> seats;
        $subscriptions = $req -> subscriptions;
        $totalPrice = 0;
        $orderUniqueId = uniqid();
        if ( count( $seats ) ) {
            $errors = $this -> checkIfErrorSeats( $seats );
            if ( $errors === 555554444 ) {
                return response() -> json( [ 'error' => 'Неправильные данные, пожалуйста обратитесь в техническую поддержку' ] );
            }
            if ( $errors === 'date_not_permitted' ) {
                return response() -> json( [ 'error' => $this -> getError( 'date_not_permitted' ) ] );
            }
            if ( $errors === 'date_expired' ) {
                return response() -> json( [ 'error' => $this -> getError( 'date_expired' ) ] );
            }
            if ( $errors === 789789765465 ) {
                return response() -> json( [ 'error' => 'Убедитесь, что Вы только что не оплачивали данный заказ.
Если Вы уже оплатили заказ, то в течение 20 минут забронированные корты и купленные абонементы отобразятся в приложении у Вас в профиле.' ] );
            }
            if ( count( $errors ) ) {
                foreach ( $errors as $key => $error ) {
                    if ( isset( $error[ 'old_reserve' ] ) ) {
                        BitrixCancelReserveHelper::cancelReserve( $error[ 'old_reserve' ] , Auth::user() );
                        unset( $errors[ $key ] );
                    }
                }
                if ( count( $errors ) ) {
                    return response() -> json( [ 'errors' => $errors ] );
                }
                // if ( isset( $errors[ 0 ][ 'old_reserve' ] ) ) {
                //     BitrixCancelReserveHelper::cancelReserve( $errors[ 0 ][ 'old_reserve' ] )
                // } else {
                // }
            }
        }
        $order = Order::create([
            'unique' => $orderUniqueId,
            'user_id' => Auth::user() -> id,
        ]);
        // dd($seats);
        if ( count( $seats ) ) {
            $totalPrice = $this -> handleSeats( $seats , $order -> id , $isNew );
            if ( $totalPrice == 'reserve_try_again' ) {
                file_put_contents(env('LOG_PATH') . '/reserve_try_again' , 'data' . PHP_EOL , FILE_APPEND );
                return response() -> json( [ 'error' => $this -> getError( 'reserve_try_again' ) ] );
            }
            if ( $totalPrice == 190909090 ) {
                return response() -> json( [ 'error' => 'К сожалению в заданном промежутке времени уже есть забронированные занятия' ] );
            }
            if ( $totalPrice == 290909090 ) {
                return response() -> json( [ 'error' => 'Неправильные данные' ] );
            }
        }
        if ( count( $subscriptions ) ) {
            $totalPrice += $this -> handleSubscriptions( $subscriptions , $order -> id );
        }
        $response = PaymentHelper::createPayment( $totalPrice , Auth::user() -> phone , $this -> payment_description , Auth::user() -> email  );
        // file_put_contents( __DIR__ . '/asd' , $response[ 'payment_url' ] );
        Order::find( $order -> id ) -> update([
            'price' => $totalPrice,
            'key' => $response[ 'payment_id' ]
        ]);
        return response() -> json( $response );
        // return response() -> json( [ 'status' => 'ok' ] );
        // exit(json_encode($seats));
    }
    

    protected function getError ( $code , $data = null ) {
        if ( $code == 'reserved_already' ) {
            // return 'reserved_already';
            return 'К сожалению в заданном промежутке времени уже есть забронированные занятия';
        } else if ( $code == 'reserve_try_again' ) {
            return 'Произошла ошибка, пожалуйста попробуйте позже.';
        } else if ( $code === 'date_not_permitted' ) {
            return 'Бронирования кортов в следующем месяце будут доступны начиная с 10:00 26-го числа текущего месяца.';
        }  else if ( $code === 'date_expired' ) {
            return 'Невозможно бронировать корт на прошедшую дату.';
        } else if ( $code == 'wait_20_minutes' ) {
            // return '20 min';
            return "Убедитесь, что Вы только что не оплачивали данный заказ.\nЕсли Вы уже оплатили заказ, то в течение 20 минут забронированные корты и купленные абонементы отобразятся в приложении у Вас в профиле.";
        } else if ( $code == 'bought' ) {
            $message = '';
            foreach ( $data as $seat ) {
                $message .= "Корт в " . $seat[ 'time' ] . " часа " . $seat[ 'date' ] . " больше недоступен\n\n";
            }
            $message .= "\nПожалуйста выберите другие часы и попробуйте снова";
            return $message;
        } else if ( $code == 'card_invalid' ) {
            return 'Недостаточно средств на карте';
        }
    }



    public function payNewEra ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return response() -> json( [ 'error' => 'Ведутся технические работы, приложение временно не принимает оплату. Пожалуйста, попробуйте позже.' ]);
        }
        $this -> log( $req -> all() );
        $seats = $req -> cartTotal[ 'seats' ] ?? [];
        $subscriptions = $req -> cartTotal[ 'subscriptions' ] ?? [];
        $totalPrice = 0;
        $orderUniqueId = uniqid();
        $order = Order::create([
            'unique' => $orderUniqueId,
            'user_id' => Auth::user() -> id,
            'card_id' => $req -> cardId
        ]);
        if ( count( $seats ) ) {
            $checkReserved = ReserveHelper::checkReservedBatch( $seats , Auth::user() -> id );
            if ( $checkReserved[ 'success' ] === false ) {
                return response() -> json( [ 'error' => $checkReserved[ 'error_messages' ][ 0 ] ] );
            }
            $storeReservesResult = $this -> handleSeats( $seats , $order -> id , true );
            if ( $storeReservesResult[ 'success' ] === false ) {
                return response() -> json( [ 'error' => $checkReserved[ 'error_messages' ][ 0 ] ] );
            } else {
                $totalPrice = $storeReservesResult[ 'totalPrice' ];
            }
        }

        // DO NOT MOVE
        if ( count( $subscriptions ) ) {
            $totalPrice += SubscriptionHelper::storeSubscriptions( $subscriptions , $order -> id , Auth::user() );
        }
        // DO NOT MOVE

        if ( $req -> cardId != 0 ) {
            Order::find( $order -> id ) -> update([
                'price' => $totalPrice,
            ]);
            $response = PaymentHelper::payWithCard( $totalPrice , Auth::user() -> phone , $req -> cardId , $order -> id , Auth::user() -> email );
            if ( $response ) {
                // $this -> orderSuccess( $order -> id );
                return response() -> json( [ 'done' => 1 ] );
            } else {
                return response() -> json( [ 'error' => $this -> getError( 'card_invalid' ) ] );
            }
        } else {
            $response = PaymentHelper::createPayment( $totalPrice , Auth::user() -> phone , 'Оплата корта(-ов)' , Auth::user() -> email );
            Order::find( $order -> id ) -> update([
                'price' => $totalPrice,
                'key' => $response[ 'payment_id' ]
            ]);
        }
        return response() -> json( $response );
    }
}
