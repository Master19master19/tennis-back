<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use App\User;
use Auth;
use App\Http\Requests\Api\RegisterOrderRequest;
use App\Http\Requests\Api\FeedBackRequest;
use App\Category;
use App\Order;
use App\Seat;
use App\Card;
use App\Subscription;
use App\BoughtSeat;
use App\BoughtSubscription;
use App\Trainer;

class OtherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rules() {
        return view( 'mobile.rules' );
    }
    public function getUser () {
        $user = Auth::user();
        return response() -> json( [ $user ] );
    }
    public function services () {
        $services = Service::whereActive( 1 ) -> with( 'type' ) -> with( 'category' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $services );
    }
    public function needCardDemo ( Request $req ) {
        $res = true;
        $cardsCount = Card::whereUserId( Auth::user() -> id ) -> whereNotNull( 'token' ) -> count();
        if ( $cardsCount > 0 ) {
            return response() -> json(false);
        }
        // $ordersCount = Order::whereUserId( Auth::user() -> id ) -> wherePaid( '1' ) -> count();
        // if ( $ordersCount > 3 ) $res = false;
        if ( Auth::user() -> shown_demo > 4 ) {
            $res = false;
        } else {
            User::find( Auth::user() -> id ) -> increment( 'shown_demo' , 1 );
        }
        // $res = true;
        return response() -> json($res);
    }
    public function registerOrder ( RegisterOrderRequest $req ) {
        $balance = Auth::user() -> balance;
        $service = Service::whereCategoryId( $req -> category_id ) -> whereTypeId( $req -> type_id ) -> firstOrFail();
        $totalPrice = $req -> quantity * ( $service -> thousand_price / 1000 );
        if ( $balance > $totalPrice ) {
            $order = Order::create([
                'user_id' => Auth::user() -> id,
                'service_id' => $service -> id,
                'quantity' => $req -> quantity,
                'price' => $service -> price,
                'total_price' => $totalPrice,
                'paid' => '1',
                'state' => 'started',
                'link' => $req -> link,
            ]);
            User::find( Auth::user() -> id ) -> decrement( 'balance' , $totalPrice );
            return response() -> json( [ 'order' => $order ] );
        } else {
            return response() -> json( [ 'error' => __( 'Balance error' ) ] , 422 );
        }
    }
    public function orders () {
        $order = Order::whereUserId( Auth::user() -> id ) -> with( 'service' ) -> with( 'service.type' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $order );
    }


    public function feedback ( FeedBackRequest $req ) {
        $data = $req -> all();
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        \App\Feedback::insert( $data );
        \App\Helpers\Mail::sendFeedBackToAdmin( 'Новая заявка' , $data );
        return response() -> json( [ 'status' => 'ok' ] );
    }


    
    public function seatsHistory () {
        $boughtSeats = BoughtSeat::where([
            'paid' => 1 ,
            'user_id' => Auth::user() -> id
        ]) -> where( 'date' , '>=' , date( 'Y-m-d' , strtotime( '-5 months') ) )-> with( 'court' ) -> orderBy( 'date' , 'DESC' ) -> get();
        // $seats = Seat::whereIn( 'court_id' , $boughtSeats ) -> get();
        return response() -> json( $boughtSeats );
    }
    public function subscriptionsHistory() {
        $boughtSubscriptions = BoughtSubscription::where([
            'paid' => 1 ,
            'user_id' => Auth::user() -> id
        ]) -> pluck( 'subscription_id' );
        $subscriptions = Subscription::whereIn( 'id' , $boughtSubscriptions ) -> get();
        return response() -> json( $subscriptions );
    }
    public function getCatServices ( Request $req , $categoryId ) {
        $services = Service::whereActive( 1 ) -> whereCategoryId( $categoryId ) -> with( 'type' ) -> with( 'category' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $services );
    }
    public function categories () {
        $cats = Category::orderBy( 'id' , 'ASC' ) -> get() -> toArray();
        return response() -> json( $cats );
    }

    public function getService ( Request $req , $categoryId , $typeId ) {
        $service = Service::whereActive( 1 ) -> where( [ 'category_id' => $categoryId , 'type_id' => $typeId ] ) -> firstOrFail() -> toArray();
        return response() -> json( $service );
    }
    public function typesAndCats () {
        $categories = [];
        $types = [];
        $services = Service::whereActive( 1 ) -> with( 'type' ) -> with( 'category' ) -> orderBy( 'id' , 'ASC' ) -> get() -> toArray();
        foreach ( $services as $service ) {
            if ( isset( $categories[ $service[ 'category' ][ 'id' ] ] ) ) {
                $categories[ $service[ 'category' ][ 'id' ] ][ 'types' ][] = $service[ 'type' ];
            } else {
                $categories[ $service[ 'category' ][ 'id' ] ] = [
                    'id' => $service[ 'category' ][ 'id' ],
                    'title' => $service[ 'category' ][ 'title' ],
                    'types' => [ $service[ 'type' ] ]
                ];
            }
        }
        return response() -> json( array_values( $categories ) );
    }

    public function getSubscriptions () {
        $subscriptions = Subscription::whereActive(1)->get();
        return response() -> json( $subscriptions );
    }
    public function trainers() {
        // APP_URL
        $trainers = Trainer::all() -> toArray();
        foreach ( $trainers as $key => $trainer ) {
            // $trainers[ $key ][ 'image_url' ] = env( 'APP_URL' ) . $trainers[ $key ][ 'image_url' ];
            $trainers[ $key ][ 'experience' ] = json_decode( $trainers[ $key ][ 'experience' ] , 1 );
            $trainers[ $key ][ 'achievements' ] = json_decode( $trainers[ $key ][ 'achievements' ] , 1 );
        }
        return response() -> json( $trainers );
    }
    public function setExpoPushToken ( Request $req ) {
        $pushToken = $req -> pushToken;
        $user = User::whereId( Auth::user() -> id ) -> firstOrFail();
        $user -> expo_token = $pushToken;
        $user -> save();
        return response() -> json([ 'status' => 'ok' ]);
        // file_put_contents( __DIR__ . '/expotoken' , json_encode($req -> all()) );
    }
}
