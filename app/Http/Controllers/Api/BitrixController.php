<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\BitrixSeatHelper;
use App\Helpers\BitrixUserHelper;
use App\Helpers\BitrixSubscriptionHelper;
use App\Helpers\BitrixRentHelper;
use App\Helpers\LogHelper;


class BitrixController extends Controller {
    protected function handleResponse ( $data , $input ) {
        if ( $data[ 'result' ] == false ) {
            return $this -> throwErr( $data , $input );
        } else {
            return $this -> throwSuccess( $data , $input );
        }
    } 
    protected function throwErr ( $data , $input ) {
        LogHelper::bitrix_in( $input , 1  , $data[ 'error' ] );
        // LogHelper::send( $data[ 'error' ] );
        return response() -> json( $data );
    }
    protected function throwSuccess ( $data , $input ) {
        LogHelper::bitrix_in( $input );
        return response() -> json( $data );
    }
    public function inhale ( Request $req ) {
        $input = $req -> all();
        LogHelper::bitrix_in_init( $input );
        usleep( 200000 );
        
        if ( env( 'BITRIX_DOWN' ) == true ) {
            exit('virus');
        }
        // file_put_contents( __DIR__ . '/inhale.json' , date( 'Y-m-d H:i:s') . ' ' . json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        // $ip = $req -> ip();
        // file_put_contents( __DIR__ . '/inhaleip.json' , $ip . PHP_EOL , FILE_APPEND );
        $class = $req -> input( 'class' );
        if ( $class === 'chargingRent' ) {
            $res = BitrixSeatHelper::handleRent( $input );
            return $this -> handleResponse( $res , $input );
        } else if ( $class === 'partner' ) {
            $res = BitrixUserHelper::handleUser( $input );
            return $this -> handleResponse( $res , $input );
        } else if ( $class == 'membership' ) {
            $res = BitrixSubscriptionHelper::handleSubscription( $input );
            return $this -> handleResponse( $res , $input );
        } else if ( $class == 'rent' ) {
            $res = BitrixRentHelper::handleRent( $input );
            return $this -> handleResponse( $res , $input );
        }
        // else if ( $class == 'paymentMembership' ) {
            // $res = BitrixSubscriptionHelper::handleSubscriptionPayment( $input );
            // return $this -> handleResponse( $res , $input );
        // }
    }
    public function createContragent ( Request $req ) {
        file_put_contents( __DIR__ . '/contragent.json' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        return response() -> json(['status'=>'ok'],200);
    }
    public function updateContragent ( Request $req ) {
        file_put_contents( __DIR__ . '/updateContragent.json' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        return response() -> json(['status'=>'ok'],200);
    }
    public function rent ( Request $req ) {
        file_put_contents( __DIR__ . '/rent.json' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
    }
    public function subscription ( Request $req ) {
        file_put_contents( __DIR__ . '/subscription.json' , json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
    }
    public function courts () {
        $courts = \App\Court::select([ 'id' , 'title' , 'arena_id' ]) -> whereArenaId( 6 ) -> get() -> toArray();
        // dd($courts);
        foreach ( $courts as $key => $court ) {
            $arena = \App\Arena::whereId( $court[ 'arena_id' ] ) -> first();
            // $arena = \App\Arena::whereId( $court[ 'arena_id' ] ) -> whereActive( 1 ) -> first();
            if ( $arena === null ) {
                unset( $courts[ $key ] );
            } else {
                $courts[ $key ][ 'arena_type' ] = $arena[ 'type_code' ];
                unset( $courts[ $key ][ 'arena_id' ] );
            }
        }
        return response() -> json( $courts );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
