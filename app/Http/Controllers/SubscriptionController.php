<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use App\Helpers\BitrixHelper;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $subscriptions = Subscription::get();
        return view( 'subscription.index' , compact( 'subscriptions' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'subscription.create' );
        //
    }

    public function store(Request $req)
    {
        $data = $req -> only( 'price' , 'weekCount' , 'title' , 'duration' );
        $res = Subscription::create( $data );
        return redirect() -> route( 'subscriptions.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
        return view( 'subscription.show' , compact( 'subscription' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
        return view( 'subscription.edit' , compact( 'subscription' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Subscription $subscription)
    {
        $data = $req -> only( 'price' , 'weekCount' , 'title' , 'duration' );
        $res = $subscription -> update( $data );
        BitrixHelper::updateSubscription( $subscription );
        return redirect() -> route( 'subscriptions.index' );
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Subscription::findOrFail( $id ) -> delete();
        return redirect() -> route( 'subscriptions.index' );
        //
    }
}
