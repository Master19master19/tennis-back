<?php

namespace App\Http\Controllers;

use App\Arena;
use App\Seat;
use App\Court;
use Illuminate\Http\Request;

class ArenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $arenas = Arena::all();
        return view( 'arena.index' , compact( 'arenas' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'arena.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $data = $req -> only( 'title' , 'cell_color' );
        $res = Arena::create( $data );
        return redirect() -> route( 'arenas.index' );
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Arena  $arena
     * @return \Illuminate\Http\Response
     */
    public function show(Arena $arena)
    {
        //
        return view( 'arena.show' , compact( 'arena' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Arena  $arena
     * @return \Illuminate\Http\Response
     */
    public function edit(Arena $arena)
    {
        //
        return view( 'arena.edit' , compact( 'arena' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \App\Arena  $arena
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Arena $arena)
    {
        $data = $req -> only( 'title' , 'cell_color' );
        $data[ 'active' ] = 0;
        if ( $req -> has( 'active' ) ) {
            $data[ 'active' ] = 1;
        }
        $res = $arena -> update( $data );
        return redirect() -> route( 'arenas.index' );
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Arena  $arena
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Arena::findOrFail( $id ) -> delete();
        Court::where( 'arena_id' , $id ) -> delete();
        Seat::where( 'arena_id' , $id ) -> delete();
        return redirect() -> route( 'arenas.index' );
        //
    }
}
