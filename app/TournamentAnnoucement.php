<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentAnnoucement extends Model
{
	protected $guarded = [];
    protected $months = [
    		'' , 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
    	];
    protected $appends = [ 'image_url' , 'formatted_start_date' ];
	public function category () {
		return $this -> hasOne( 'App\TournamentCategory', 'id' , 'category_id' );
	}
    public function getImageUrlAttribute () {
        return env( 'APP_URL' ) . '/' . $this -> img_url;
    }
    public function getFormattedStartDateAttribute () {
    	$dateTime = strtotime( $this -> date_start );
    	$month = $this -> months[ date( 'n' , $dateTime ) ];
        return date( 'd ' . $month . ' Y' , $dateTime );
    }
}
