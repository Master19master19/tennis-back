<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    //
    protected $connection = 'second_connection';
    protected $guarded = [];
}
