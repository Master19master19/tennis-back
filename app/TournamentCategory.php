<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentCategory extends Model
{
    //
    protected $guarded = [];
    public function getGetLevelAttribute () {
        return $this -> level == 'mature' ? "Взрослые" : "Дети";
    }
}
