<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model {
    protected $appends = [ 'image_url' ];
    public function getImageUrlAttribute () {
        return env( 'APP_URL' ) . '/' . $this -> img_url;
    }
    public function getExperienceeAttribute () {
        return json_decode( $this -> experience );
    }
    public function getAchievementeAttribute () {
        return json_decode( $this -> achievements );
    }
}