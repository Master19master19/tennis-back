<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveLog extends Model
{
    //
    protected $connection = 'second_connection';
    protected $guarded = [];
}
