<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BoughtSubscription;
use App\BoughtSeat;
use App\Order;
use App\User;
use App\Helpers\Mail;
use App\Mail as MailDB;


class CheckEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unsent = MailDB::whereDone( 0 ) -> take( 30 ) -> get();
        foreach ( $unsent as $key => $value ) {
            $orderId = $value -> order_id;
            MailDB::whereId( $value -> id ) -> update([ 'done' => 1 ]);
            $this -> sendOrderEmail( $orderId );
        }
    }
    protected function sendOrderEmail( $orderId ) {
        $order = Order::findOrFail( $orderId );
        $user = User::findOrFail( $order -> user_id ) -> toArray();
        $finalData = [
            'boughtSubscriptions' => [],
            'boughtSeats' => [],
            'user' => $user,
            'order' => $order
        ];
        $boughtSubscriptions = BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> with( 'subscription' ) -> get() -> toArray();
        if ( null !== $boughtSubscriptions ) {
            foreach ( $boughtSubscriptions as $key => $value ) {
                $finalData[ 'boughtSubscriptions' ][] = $value;
            }
        }
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> with( [ 'arena' , 'court' ] ) -> get() -> toArray();
        if ( null !== $boughtSeats ) {
            foreach ( $boughtSeats as $key => $value ) {
                $finalData[ 'boughtSeats' ][] = $value;
            }
        }
        Mail::sendToAdmin( $finalData );
        sleep( 3 );
    }
}
