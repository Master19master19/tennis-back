<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arena extends Model
{
    //
    protected $guarded = [];
	public function courts () {
		return $this -> hasMany( 'App\Court', 'arena_id' , 'id' );
	}
	public function seats () {
		return $this -> hasMany( 'App\Seat', 'arena_id' , 'id' );
	}
}
