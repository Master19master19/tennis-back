<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Out extends Model
{
    //
    
	protected $guarded = [];
	public function getMessageAdminAttribute()  {
		$decoded = json_decode( $this -> message , 1 );
		$start = '';
		if ( isset( $decoded[ 'cancelReserve' ] ) ) {
			$start = '<b><h2 class="text-center">Cancel Reserve</h2></b><br>';
		}
		if ( isset( $decoded[ 'newReserve' ] ) ) {
			$start = '<b><h2 class="text-center">New Reserve</h2></b><br>';
		}
		if ( $decoded[ 'class' ] == 'payment' ) {
			$start = '<b><h2 class="text-center">Payment</h2></b><br>';
		}
		$pretty = json_encode( $decoded , JSON_PRETTY_PRINT );
		return $start . '' . $pretty;
	}
	public function getClasssAttribute()  {
		$decoded = json_decode( $this -> message , 1 );
		return $decoded[ 'class' ];
	}
}
