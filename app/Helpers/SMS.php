<?php

namespace App\Helpers;
use App\Helpers\LogHelper;


class SMS {
	protected static function aeroSms ( $sms , $to ) {
		$aeroLogin = env( 'SMS_AERO_LOGIN' );
		$aeroKey = env( 'SMS_AERO_KEY' );
		$to = str_replace( '+' , '' , $to );
    	$to = str_replace( '(' , '' , $to );
		$to = str_replace( ')' , '' , $to );
		$to = str_replace( ' ' , '' , $to );
		$to = str_replace( ' ' , '' , $to );
		$to = str_replace( ' ' , '' , $to );
		$url = "https://{$aeroLogin}:{$aeroKey}@gate.smsaero.ru/v2/sms/send?number={$to}&text={$sms}&sign=SMS Aero&channel=DIRECT";
		$output = file_get_contents( $url );
		$output = json_decode( $output , 1 );
		file_put_contents( env( 'LOG_PATH' ) . '/sms_aero' , json_encode(['output'=>$output,'url' => $url,'to'=>$to]).PHP_EOL,FILE_APPEND);
		if ( is_array( $output ) && isset( $output[ 'success' ] ) && $output[ 'success' ] == 'true' ) {
			return true;
		}
		return false;
	}
	protected static function smsc ( $sms , $to ) {
		$login = env( 'SMSC_LOGIN' );
		$password = env( 'SMSC_PASSWORD' );
		// $to = '+37477797590';
		$url = "https://smsc.ru/sys/send.php?login={$login}&psw={$password}&phones={$to}&mes={$sms}";
		// $url = "https://smsc.ru/sys/send.php?login={$login}&psw={$password}&phones={$to}&mes={$sms}&cost=1";
		$ch = curl_init();
        curl_setopt( $ch , CURLOPT_URL , $url );
        curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
        $output = curl_exec( $ch );
        curl_close( $ch );
		file_put_contents( env( 'LOG_PATH' ) . '/smsc' , json_encode(['output'=>$output,'url' => $url,'to'=>$to]).PHP_EOL,FILE_APPEND);
        if ( strpos( $output , 'OK' ) !== false ) {
        	return true;
        } else {
        	$error = "Ошибка smsc, на телефон <b> {$to} </b> , \n " . $output;
        	LogHelper::send( $error );
        	LogHelper::telegram( $error );
        	return false;
        }
	}
	
	public static function sendSMS ( $title , $desc , $to ) {
		// $to = '89778699228';
		try {
			$sms = $title . " " . $desc;
			$smscSent = self::smsc ( $sms , $to );
			if ( ! $smscSent ) {
				return self::aeroSms( $sms , $to );
			}
	        return true;
		} catch ( \Exception $e ) {
        	LogHelper::telegram( $e -> getMessage() );
        	LogHelper::send( $e -> getMessage() );
		}
	}

	public static function sendSMSNew ( $title , $desc , $to ) {
		$sms = $title . " " . $desc;
		try {
			$aeroSMSSent = self::aeroSms( $sms , $to );
			if ( ! $aeroSMSSent ) {
				return self::smsc ( $sms , $to );
			}
	        return true;
		} catch ( \Exception $e ) {
        	LogHelper::telegram( $e -> getMessage() );
        	LogHelper::send( $e -> getMessage() );
		}
	}
}