<?php

namespace App\Helpers;
use App\Subscription;
use App\BoughSubscription;
use App\User;


// Пример файла с тарификацией абонемента:
// {
// "objects": [
// {
// "id": "ff58ea05-1336-4af7-8e9a-8663d4baf21a",
// "type": "membership",
// "name": "Карта  годовая - Серебряная",
// "price": "35000.00"
// },
// {
// "id": "945a612c-6165-4ddd-9ca6-251113916452",
// "type": "membership",
// "name": "Карта  годовая - детская",
// "price": "15000.00"
// }
// ]
// }
 
// В случае оплаты абонемента – необходимо передавать эту информацию из МП в 1С.
 
// Пример файла с оплатой абонемента:
// {
// "objects": [
// {
// "id": "6df72a01-6715-444a-a62a-1805fd6058c5",
// "type": "paymentMembership",
// "phone": "+79109101010",
// "items": [
// {
// "membershipId": "ff58ea05-1336-4af7-8e9a-8663d4baf21a",
// "count": 1,
// "sum": "35000.00"
// },
// {
// "membershipId": "945a612c-6165-4ddd-9ca6-251113916452",
// "count": 2,
// "sum": "30000.00"
// }
// ]
// }
// ]
// }
 
// "membershipId" – id абонемента.  "count" – количество. "sum" – сумма.

class BitrixSubscriptionHelper {
	// public static function handleSubscriptionPayment ( $data ) {
	// 	$items = $data[ 'items' ];
	// 	$nowDate = date( 'Y-m-d H:i:s' );
	// 	$user = User::wherePhone( $data[ 'phone' ] ) -> first();
	// 	foreach ( $items as $value ) {
	// 		$subscription = Subscription::whereExternalId( $value[ 'membershipId' ] ) -> first();
	// 		if ( null === $subscription ) {
	//         	return [
	// 	            'result' => false,
	// 	            'errorCode' => 404,
	// 	            'error' => "Абонемент " . $value[ 'membershipId' ] . " не найден"
	// 	        ];
	// 		}
	// 		$info = [
	// 			'paid' => 1,
	// 			'subscription_id' => $subscription -> id,
	// 			'created_at' => $nowDate,
	// 			'order_id' => 0,
	// 			'price' => $value[ 'sum' ]
	// 		];
	// 		if ( null !== $user ) {
	// 			$info[ 'user_id' ] = $user -> id;
	// 		}
	// 		$record = BoughSubscription::insert( $info );
	// 	}
 //        return [
 //            'result' => true
 //        ];
	// }
	// {"class":"membership","id":"d0dc6636-c5ba-11e9-9ab5-382c4a64b65a","price":10000,"isActive":true,"countVisits":2,"countVisitsType":"\u041d\u0435\u0434\u0435\u043b\u044f","duration":60,"name":"\u0422\u0435\u043d\u043d\u0438\u0441-\u041c\u0435\u0441\u044f\u0446 10 000 \u0440\u0443\u0431. - 2 \u0440\u0430\u0437\u0430 \u0432 \u043d\u0435\u0434\u0435\u043b\u044e\/1 \u0447\u0430\u0441"}

	public static function handleSubscription ( $data ) {
		$externalId = $data[ 'id' ];
		$nowDate = date( 'Y-m-d H:i:s' );
		if ( $data[ 'isActive' ] == false ) {
			$subscription = Subscription::whereExternalId( $externalId ) -> first();
			if ( null == $subscription ) {
				return [
					'result' => true,
                    'errorCode' => 500,
                    'error' => "Абонемент " . $data[ 'id' ] . " не найден"
				];
			} else {
				$subscription -> active = 0;
				$subscription -> save();
		        return [
		            'result' => true
		        ];
			}
		} else {
			$info = [
				'title' => $data[ 'name' ],
				'price' => $data[ 'price' ],
				'weekCount' => $data[ 'countVisits' ],
				'duration' => ( (int)$data[ 'duration' ] / 60 ),
				'updated_at' => $nowDate,
				'active' => ( $data[ 'isActive' ] == 'true' ? 1 : 0 )
			];
		}
		$subscription = Subscription::whereExternalId( $externalId ) -> first();
		if ( $subscription === null ) {
			$info[ 'external_id' ] = $externalId;
			$subscription = Subscription::create( $info );
		} else {
        	$subscription -> update( $info );
		}
        return [
            'result' => true
        ];
// 		{"class":"membership","isActive":false,"id":"ccee15ee-21c2-11e6-868d-382c4a64b65a","name":"\u041a\u0430\u0440\u0442\u0430  \u0433\u043e\u0434\u043e\u0432\u0430\u044f \u0434\u0435\u0442\u0441\u043a\u0430\u044f"}
// {"class":"membership","id":"88bc1b8d-8524-11e5-9dab-382c4a64b65a","price":35000,"isActive":true,"name":"\u041a\u0430\u0440\u0442\u0430  \u0433\u043e\u0434\u043e\u0432\u0430\u044f - \u0421\u0435\u0440\u0435\u0431\u0440\u044f\u043d\u0430\u044f"}
// {"class":"membership","id":"d0dc6636-c5ba-11e9-9ab5-382c4a64b65a","price":10000,"isActive":true,"countVisits":2,"countVisitsType":"\u041d\u0435\u0434\u0435\u043b\u044f","duration":60,"name":"\u0422\u0435\u043d\u043d\u0438\u0441-\u041c\u0435\u0441\u044f\u0446 10 000 \u0440\u0443\u0431. - 2 \u0440\u0430\u0437\u0430 \u0432 \u043d\u0435\u0434\u0435\u043b\u044e\/1 \u0447\u0430\u0441"}

	}
}