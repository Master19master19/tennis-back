<?php

namespace App\Helpers\Reserve;
use App\Subscription;
use App\BoughtSubscription;
use App\Helpers\Reserve\LogHelper;

class SubscriptionHelper {
    public static  function storeSubscriptions ( $subscriptions , $orderId , $userInfo ) {
        $totalPrice = 0;
        $allSubscriptions = [];
        foreach ( $subscriptions as $weirdKey => $subscription ) {
            if ( isset( $subscription[ 0 ] ) ) {
                $subscription_id = $subscription[ 0 ][ 'id' ]; // single
            } else {
                $subscription_id = $subscription[ 'id' ]; // backward compatibility
            }
            $subscriptionRecord = Subscription::whereId( $subscription_id ) -> firstOrFail(); // single
            $price = $subscriptionRecord -> price;
            $current_data = [
                'order_id' => $orderId,
                'price' => $price,
                'subscription_id' => $subscription_id,
                'user_id' => $userInfo -> id,
                'created_at' => date( 'Y-m-d H:i:s' ),
                'updated_at' => date( 'Y-m-d H:i:s' ),
                // 'paid' => 1 // toDelete
            ];
            $current_data[ 'profile_name' ] = $userInfo -> name;
            $current_data[ 'profile_birthday' ] = "01-01-2010";
            if ( isset( $subscription[ 0 ] ) && isset( $subscription[ 0 ][ 'profile' ] ) && isset( $subscription[ 0 ][ 'profile' ][ 'name' ] ) ) {
                $current_data[ 'profile_name' ] = $subscription[ 0 ][ 'profile' ][ 'name' ];
            } 
            if ( isset( $subscription[ 0 ] ) && isset( $subscription[ 0 ][ 'profile' ] ) && isset( $subscription[ 0 ][ 'profile' ][ 'birthday' ] ) ) {
                $current_data[ 'profile_birthday' ] = substr( $subscription[ 0 ][ 'profile' ][ 'birthday' ] , 0 , 10 );
            } 
            if ( isset( $subscription[ 0 ] ) && isset( $subscription[ 0 ][ 'type' ] ) ) {
                $current_data[ 'type' ] = $subscription[ 0 ][ 'type' ];
            } 
            $allSubscriptions[] = $current_data;
            $totalPrice += ( float ) $price;
        }
        BoughtSubscription::insert( $allSubscriptions );
        return $totalPrice;
    }
}