<?php

namespace App\Helpers;

class MysqlHelper {
    public static function create ( $model , $data ) {
        try {
            $res = $model::create( $data );
        } catch ( \Exception $e ) {
            echo $e -> getMessage();
            return false;
        }
        return $res;
    }
    public static function update ( $modelRecord , $data ) {
        try {
            $res = $modelRecord -> update( $data );
        } catch ( \Exception $e ) {
            echo $e -> getMessage();
            return false;
        }
        return $res;
    }
    public static function first ( $model , $data ) {
        try {
            $res = $model::where( $data ) -> first();
            // $res = $modelRecord -> update( $data );
        } catch ( \Exception $e ) {
            echo $e -> getMessage();
            exit;
            return false;
        }
        return $res;
    }
}