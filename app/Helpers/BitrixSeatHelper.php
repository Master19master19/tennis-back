<?php

namespace App\Helpers;
use App\Arena;
use App\Seat;
use App\Court;

// {"class":"chargingRent","courtId":"19","days":1,"startTime":"0001-01-01T09:00:00","finishTime":"0001-01-01T09:59:59","price":0}

class BitrixSeatHelper {
    private static $error = [];
    private static $weekDays = [ 'price' , 'monday' , 'tuesday' , 'wednesday' , 'thursday' , 'friday' , 'saturday' , 'sunday' ];
	public static function handleRent ( $data ) {
        self::updateSeatPricesNew( $data );
        return self::updateSeatPrices( $data );
	}
    private static function getWeekType ( $days ) {
        if ( is_array( $days ) ) {
           $day = $days[ 0 ]; 
        } else {
            $day = $days;
        }
        if ( in_array( $day , [6,7,9] ) ) {
            return 'weekend';
        } else if ( $day == 0 ) {
            return 'all';
        } else {
            return 'week';
        }
    }
// {"class":"chargingRent","courtId":"255","days":"4","startTime":"0001-01-01T17:00:00","finishTime":"0001-01-01T17:59:59","price":"1500"}
    private static function updateSeatPrices ( $data ) {
        $weekType = self::getWeekType( $data[ 'days' ] );
        $start = str_replace( '0001-01-01T' , '' , $data[ 'startTime' ] );
        $start = str_replace( ':00:00' , '' , $start );
        $end = str_replace( '0001-01-01T' , '' , $data[ 'finishTime' ] );
        $end = str_replace( ':59:59' , '' , $end );
        for ( $x = (int) $start; $x <= ( int ) $end; $x++ ) {
            if ( $x < 10 ) {
                $time = "0" . $x . ":00";
            } else {
                $time = $x . ":00";
            }
            $searchData = [
                'time' => $time,
                'court_id' => $data[ 'courtId' ],
            ];
            $court = Court::whereId( $data[ 'courtId' ] ) -> first();
            // dd($court);
            if ( null == $court ) {
                self::$error = [
                    'result' => false,
                    'errorCode' => 500,
                    'error' => "Корт " . $data[ 'courtId' ] . " не найден"
                ];
            } else {
                $seat = Seat::where( $searchData ) -> first();
                if ( $seat == null ) {
                    if ( $weekType == 'week' ) {
                        $searchData[ 'price' ] = $data[ 'price' ];
                    } else if ( $weekType == 'all' ) {
                        $searchData[ 'price_weekend' ] = $data[ 'price' ];
                        $searchData[ 'price' ] = $data[ 'price' ];
                    } else {
                        $searchData[ 'price_weekend' ] = $data[ 'price' ];
                    }
                    $res = Seat::create( $searchData );
                } else {
                    $updateData = [];
                    if ( $weekType == 'week' ) {
                        $updateData = ['price' => $data[ 'price' ]];
                    } else if ( $weekType == 'all' ) {
                        $updateData = ['price_weekend' => $data[ 'price' ],'price' => $data[ 'price' ] ];
                    } else {
                        $updateData = ['price_weekend' => $data[ 'price' ]];
                    }
                    $seat -> update( $updateData );
                }
            }
            if ( count( self::$error ) ) {
                return self::$error;
            } else {
                return [ 'result' => true ];
            }
        }
    }



    private static function updateSeatPricesNew ( $data ) {
        $start = str_replace( '0001-01-01T' , '' , $data[ 'startTime' ] );
        $start = str_replace( ':00:00' , '' , $start );
        $end = str_replace( '0001-01-01T' , '' , $data[ 'finishTime' ] );
        $end = str_replace( ':59:59' , '' , $end );
        for ( $x = (int) $start; $x <= ( int ) $end; $x++ ) {
            if ( $x < 10 ) {
                $time = "0" . $x . ":00";
            } else {
                $time = $x . ":00";
            }
            $searchData = [
                'time' => $time,
                'court_id' => $data[ 'courtId' ],
            ];
            $court = Court::whereId( $data[ 'courtId' ] ) -> first();
            // dd($court);
            if ( null == $court ) {
                self::$error = [
                    'result' => false,
                    'errorCode' => 500,
                    'error' => "Корт " . $data[ 'courtId' ] . " не найден"
                ];
            } else {
                self::setDays( $searchData , $data[ 'days' ] , $data[ 'price' ] );
            }
        }
        if ( count( self::$error ) ) {
            return self::$error;
        } else {
            return [ 'result' => true ];
        }
    }

    private static function setDays ( $searchData , $days , $price ) {
        $seat = Seat::where( $searchData ) -> first();
        if ( $seat == null ) {
            self::$error = [
                'result' => false,
                'errorCode' => 500,
                'error' => "Такого часа " . $searchData[ 'time' ]  . ' для ' .  $searchData[ 'court_id' ] . " не существует"
            ];
            return;
        }
        if ( $days == 0 ) {
            $loopArr = [ 1 , 2 , 3 , 4 , 5 , 6 , 7 ];
        } else if ( $days == 8 ) {
            $loopArr = [ 1 , 2 , 3 , 4 , 5 ];
        } else if ( $days == 9 ) {
            $loopArr = [ 6 , 7 ];
        } else {
            $seat -> update([
                self::$weekDays[ $days ] => $price
            ]);
            return true;
        }
        foreach ( $loopArr as $day ) {
            $seat -> update([
                self::$weekDays[ $day ] => $price
            ]);
        }
        return true;
    }
}