<?php

namespace App\Helpers;

class BackupHelper {
	public static function backup ( $public = false ) {
		$res = self::mysql( $public );
		if ( $public ) {
			// self::files();
			header( "Content-type: application/x-gzip" );
			header( 'Content-Disposition: attachment; filename=' . $res[ 1 ] );
			readfile( $res[ 0 ] );
			exit;
		}
		if ( true !== $res ) {
			exit( 'There was an error backing up db - ' . $res );
		}
		$res = self::files();
		exit( 'OK' );
	}
	protected static function mysql ( $public ) {
		$title = env( 'DB_DATABASE' ) . '_' . date( 'd' ) . '.sql.gz';
		$path = '/var/www/tennis/backups/' . $title;
		$cmd = "mysqldump --user=" . env( 'DB_USERNAME' ) . " --password=" . env( 'DB_PASSWORD' ) . " " . env( 'DB_DATABASE' ) . " | gzip > " . $path;
		$res = exec( $cmd );
		if ( $public ) {
			return [ $path , $title ];
		}
		return $res == '' ? true : $res;
	}
	protected static function files () {
		$path = '/var/www/tennis/backups/files_' . date( 'd' ) . '.gz';
		$path = '/var/www/tennis/backups/files.gz';
		$cmd = "tar --exclude='/var/www/tennis/vendor' --exclude='/var/www/tennis/logs' --exclude='/var/www/tennis/.git' --exclude='/var/www/tennis/public' --exclude='/var/www/tennis/node_modules' --exclude='/var/www/tennis/storage' --absolute-names -cvf " . $path . " /var/www/tennis/ ";
		$res = exec( $cmd , $out , $oky );
		return true;
	}
}