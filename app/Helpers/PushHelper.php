<?php

namespace App\Helpers;
use App\User;

class PushHelper {

    public static function push( $userId , $data , $type = 'default' ) {
        $user = User::whereId( $userId ) -> first();
        if ( null == $user ) return false;
        $expoToken = $user -> expo_token;
        if ( null == $expoToken ) return false;
        if ( strlen( $expoToken ) < 5 ) return false;
        $payload = array(
            'to' => $expoToken,
            // 'sound' => 'default',
            'body' => $data[ 'body' ],
        );

        $curl = curl_init();

        curl_setopt_array( $curl,
            [
                CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode( $payload ),
                CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Accept-Encoding: gzip, deflate",
                "Content-Type: application/json",
                "cache-control: no-cache",
                "host: exp.host"
                ),
            ]
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return false;
        } else {
            try {
                $decoded = json_decode( $response );
                if ( $decoded -> data -> status === 'ok' ) {
                    return true;
                }
                return false;
            } catch ( \Exception $e ) {
                report( $e );
                return false;
            }
        }
    }

    public static function pushBak ( $userId , $data , $type = 'default' ) {
        $user = User::whereId( $userId ) -> first();
        if ( null == $user ) return false;
        $expoToken = $user -> expo_token;
        if ( null == $expoToken ) return false;
        if ( strlen( $expoToken ) < 5 ) return false;
        $expo = \ExponentPhpSDK\Expo::normalSetup();
        $channelName = $type . '_' . date( 'md' ) . uniqid();
        try {
            $expo -> subscribe( $channelName , $expoToken );
        } catch ( \Exception $e ) {
            report( $e );
            return false;
        }
        $notification = [
            'title' => $data[ 'title' ],
            'body' => $data[ 'body' ],
            // 'badge' => 1,
            'data'=> json_encode ( [ 'data' => $data , 'type' => $type ] )
        ];
        try {
            $res = $expo -> notify( $channelName , $notification );
        } catch ( \Exception $e ) {
            report( $e );
            return false;
        }
        return $res;
    }




    
    // public static function send ( $type , $data = [] ) {
    //     $expoTokens = [];
    //     $users = User::whereNotNull( 'expo_token' ) -> get();
    //     foreach ( $users as $key => $user ) {
    //         $expoTokens[] = $user -> expo_token;
    //     }
    //     $expo = \ExponentPhpSDK\Expo::normalSetup();
    //     $channelName = $type . '_' . date( 'md' ) . uniqid();
    //     foreach ( $expoTokens as $recipient ) {
    //         // var_dump($channelName , $recipient);
    //         $expo -> subscribe( $channelName , $recipient );
    //     }
    //     $notification = [
    //         'title' => 'Новость',
    //         'body' => $data[ 'title' ],
    //         'data'=> json_encode ( [ 'data' => $data , 'type' => $type ] )
    //     ];
    //     $res = $expo -> notify( $channelName , $notification );
    //     if ( isset( $res[ 'status_code' ] ) && $res[ 'status_code' ] == 400 ) {
    //         var_dump($response['body']);
    //     }
    //     // dd($channelName,$notification);
    // }
    // public static function sendAnnouncement ( $type , $data = [] ) {
    //     $expoTokens = [];
    //     $users = User::whereNotNull( 'expo_token' ) -> get();
    //     foreach ( $users as $key => $user ) {
    //         $expoTokens[] = $user -> expo_token;
    //     }
    //     $expo = \ExponentPhpSDK\Expo::normalSetup();
    //     $channelName = $type;
    //     foreach ( $expoTokens as $recipient ) {
    //         $expo -> subscribe( $channelName , $recipient );
    //     }
    //     $notification = [
    //         'title' => 'Анонс турнира',
    //         'body' => $data[ 'title' ],
    //         'data'=> json_encode ( [ 'data' => $data , 'type' => $type ] )
    //     ];
    //     try {
    //         $res = $expo -> notify( $channelName , $notification );
    //     } catch ( Exception $e ) {
    //         report( $e );
    //     }
    // }
}