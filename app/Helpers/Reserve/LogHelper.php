<?php

namespace App\Helpers\Reserve;
use App\ReserveLog;

class LogHelper {
	public static function log ( $errorMessage , $place = '' ) {
		try {
			$log = new ReserveLog;
			$log -> error = $errorMessage;
			$log -> place = $place;
			$log -> save();
		} catch ( \Exception $e ) {
			file_put_contents( __DIR__ . '/fatal_error.log' , $e -> getMessage() . PHP_EOL , FILE_APPEND );
		}
	}
}