<?php

namespace App\Helpers\Reserve;
use App\Reserve;
use App\Helpers\Reserve\VariousHelper;
use App\Helpers\Reserve\RollBackHelper;
use App\Helpers\Reserve\LogHelper;

class ReserveHelper {


    // "price": 2500,
    // "price_weekend": 2500,
    // "id": 253,
    // "time": "19:00",
    // "court_id": 10,
    // "courtTitle": "Корт 6",
    // "courtId": 10,
    // "arenaId": 3,
    // "arenaTitle": "Хард, второй корпус",
    // "date": "2021-12-07"


	protected $hash = null;
	protected $data = null;
	protected $userId = null;

	public function __construct( $data , $userId = null ) {
		$this -> userId = $userId ?? '0';
	}


	public static function checkReservedBatch ( $batchData , $userId ) {
		try {
			$response = [
				'success' => true,
				'error_messages' => [],
			];
			foreach ( $batchData as $weirdKey => $data ) {
				// checking dates
				$datesOkayOrErrorMessage = ReserveDateChecker::checkDates( $data );
				if ( $datesOkayOrErrorMessage !== true ) {
                	// 1.date not permitted
       				// 2.date is expired
					$response[ 'success' ] = false;
					$response[ 'error_messages' ][] = $datesOkayOrErrorMessage;
				}
				// checking dates
				// $hash = VariousHelper::hash( $data );
				// $reserved = Reserve::where( 'hash' , $hash ) -> first();
				// if ( $reserved !== null && $reserved -> user_id == $userId ) {
				// 	// 5.already bought by user(bought by someone)
				// 	if ( $reserved -> paid == 1 ) {
				// 		$response[ 'success' ] = false;
				// 		$response[ 'error_messages' ][] = VariousHelper::getBoughtByUserMessage( $data );
				// 	} else if ( strtotime( $reserved -> created_at ) - strtotime( '-20 minutes' ) > 0  ) {
				// 		// 4.user reserved , but 20 minutes didnt pass
				// 		$response[ 'success' ] = false;
				// 		$response[ 'error_messages' ][] = VariousHelper::getReservedMessage20Minutes( $data );
				// 	}
				// } else if ( $reserved !== null && $reserved -> paid == 1 ) {
				// 	// 5.other user bought(bought by someone)
				// 	$response[ 'success' ] = false;
				// 	$response[ 'error_messages' ][] = VariousHelper::getBoughtMessage( $data );
				// } else if ( $reserved !== null ) {
				// 	// 6.other user reserved
				// 	$response[ 'success' ] = false;
				// 	$response[ 'error_messages' ][] = VariousHelper::getReservedMessage( $data );
				// }
			}
			LogHelper::log( json_encode( $response ) , 'ReserveHelper checkReservedBatch response' );
			return $response;
		} catch ( \Exception $e ) {
			$message = $e -> getMessage();
			LogHelper::log( $message , 'ReserveHelper checkReservedBatch catch' );
			$response[ 'success' ] = false;
			$response[ 'error_messages' ][] = VariousHelper::getFatalErrorMessage( $message );
			return $response;
		}
	}


	public function reserveSingle ( $seat ) {
		$seat[ 'court_id' ] = ( $seat[ 'court_id' ] && strlen( $seat[ 'court_id' ] ) ) ? $seat[ 'court_id' ] : $seat[ 'courtId' ];
		try {
			$reserve = new Reserve;
			$reserve -> hash = VariousHelper::hash( $seat );
			$reserve -> date = $seat[ 'date' ];
			$reserve -> time = $seat[ 'time' ];
			$reserve -> user_id = $this -> userId;
			$reserve -> order_id = $seat[ 'order_id' ];
			$reserve -> court_id = $seat[ 'court_id' ];
			$reserve -> price = $seat[ 'price' ];
			$saved = $reserve -> save();
			if ( $saved ) {
				return $saved -> id;
			} else {
				LogHelper::log( $message , 'ReserveHelper createDBRow saveReserve' );
				return false;
			}
		} catch ( \Exception $e ) {
			$message = $e -> getMessage();
			LogHelper::log( $message , 'ReserveHelper createDBRow catch' );
			return false;
		}
	}

	protected function checkSeatParameters ( $seat ) {
        $seatRecord = Seat::where([
            'time' => $seat[ 'time' ],
            'id' => $seat[ 'id' ],
            'arena_id' => $seat[ 'arenaId' ],
            'court_id' => $seat[ 'courtId' ],
        ]) -> first();
        if ( null === $seatRecord || $seatRecord -> price < 100 ) {
        	return false;
        }
        return true;
	}

	protected function getSeatPriceOfWeekDay ( $seat , $isNewVersion = true ) {
        $seatRecord = Seat::where([
            'time' => $seat[ 'time' ],
            'id' => $seat[ 'id' ],
            'arena_id' => $seat[ 'arenaId' ],
            'court_id' => $seat[ 'courtId' ],
        ]) -> first();
        if ( null === $seatRecord ) {
        	return 3000;
        }
        if ( ! $isNewVersion ) {
            $price = $seatRecord -> price;
            if ( date( 'N' , strtotime( $seat[ 'date' ] ) ) >= 6 ) {
                $price = $seatRecord -> price_weekend;
            }
        } else {
            $dateTitle = strtolower( date( 'l' , strtotime( $seat[ 'date' ] ) ) );
            $price = $seatRecord[ $dateTitle ];
        }
        return price;
	}

	public function checkReservedSingle ( $hash ) {
		$reservedCount = Reserve::where( 'hash' , $hash ) -> count();
		return $reservedCount > 0;
	}

	public function reserveIfNotReservedSingle ( $seat ) {
		if ( ! $this -> checkReservedSingle( $seat[ 'hash' ] ) && $this -> checkSeatParameters( $seat ) ) {
			$rowId = $this -> reserveSingle( $seat );
			return $rowId;
		} else {
			return false;
		}
	}
	public function handleSeats ( $seats , $orderId , $isNewVersion = false ) {
		$response = [
			'success' => true,
			'error_messages' => [],
		];
        $totalPrice = 0;
        $couldNotBuy = false;
        $reservedSeatsIds = [];
        foreach ( $seats as $weirdKey => $seat ) {
        	$seat[ 'hash' ] = VariousHelper::hash( $seat );
        	$price = $this -> getSeatPriceOfWeekDay( $seat , $isNewVersion );
        	$seat[ 'price' ] = $price;
        	$seat[ 'order_id' ] = $orderId;
        	$reservedId = $this -> reserveIfNotReserved( $seat );
        	if ( $reservedId === false ) {
        		$rollbackHelper = new RollBackHelper( $reservedSeatsIds );
        		$rollbackHelper -> rollback();
        		$response[ 'status' ] = false;
				$response[ 'error_messages' ][] = VariousHelper::getReservedMessage( $seat );
				return $response;
        		break;
        	}
        	$reservedSeatsIds[] = $reservedId;
            $totalPrice += ( float ) $price;
        }
        if ( $totalPrice < 100 ) {
    		$response[ 'status' ] = false;
			$response[ 'error_messages' ][] = VariousHelper::getCommonError( $data );
			return $response;
        }
        $response[ 'totalPrice' ] = $totalPrice;
        return $response;
    }
}



class ReserveDateChecker {

	public static function checkDates ( $seat ) {
		$datePermissionResult = self::checkDatePermitted( $seat[ 'date' ] );
		$dateExpirationResult = self::checkDateNotExpired( $seat );
		if ( $datePermissionResult === false ) {
			return VariousHelper::getDateNotPermittedMessage(); 
		}
		if ( $dateExpirationResult === false ) {
			return VariousHelper::getDateExpiredMessage(); 
		}
		return true;
	}
    protected static function checkDatePermitted ( $dateToCheck ) {
        $endNextMonth = date( 'Y-m-t' , strtotime( "+1 month" ) );
        $endCurrMonth = date( 'Y-m-t' , strtotime( "+1 minute" ) );
        $endingDatePermitted = $endNextMonth;
        $todayDate = date( 'd' );
        $todayHour = date( 'H' );
        if ( $todayDate < env( 'RESERVE_OPENING_MONTH_DAY' ) ) {
            $endingDatePermitted = $endCurrMonth;
        } else if ( $todayDate == env( 'RESERVE_OPENING_MONTH_DAY' ) && $todayHour < 10 ) {
            $endingDatePermitted = $endCurrMonth;
        }
        return strtotime( $dateToCheck ) <= strtotime( $endingDatePermitted );
    }
    protected static function checkDateNotExpired ( $seat ) {
        $currentDate = date( 'Y-m-d' );
        $currentHour = date( 'H' );
        $seatDate = date( 'Y-m-d' , strtotime( $seat[ 'date' ] ) );
        $seatHour = date( 'H' , strtotime( $seat[ 'time' ] ) );
        if ( $seatDate < $currentDate ) {
            return false;
        } else if ($seatDate == $currentDate ) {
            if ( $seatHour < $currentHour ) {
                return false;
            }
        }
        return true;
    }
}