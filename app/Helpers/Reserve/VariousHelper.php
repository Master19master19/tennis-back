<?php

namespace App\Helpers\Reserve;
use App\Reserve;
use App\Court;

class VariousHelper {


    // "price": 2500,
    // "price_weekend": 2500,
    // "id": 253,
    // "time": "19:00",
    // "court_id": 10,
    // "courtTitle": "Корт 6",
    // "courtId": 10,
    // "arenaId": 3,
    // "arenaTitle": "Хард, второй корпус",
    // "date": "2021-12-07"

	public static function hash ( $data ) {
		$data[ 'court_id' ] = ( $data[ 'court_id' ] && strlen( $data[ 'court_id' ] ) ) ? $data[ 'court_id' ] : $data[ 'courtId' ];
		$stringToHash = $data[ 'court_id' ] . '-' . $data[ 'date' ] . '-' . $data[ 'time' ];
		$hash = md5( $stringToHash );
		return $hash;
	}


	public static function getCommonError() {
		return 'Неправильные данные, пожалуйста обратитесь в техническую поддержку';
	}

	public static function getReservedMessage ( $data ) {
		try {
			$court = Court::whereId( $data[ 'court_id' ] ) -> first();
			$message = $court[ 'title' ] . ' в ' . $data[ 'time' ] . " " . $data[ 'date' ] . " недоступен на данный момент";
			return $message;
		} catch ( \Exception $e ) {
			return "Корт в " . $data[ 'time' ] . " часа " . $data[ 'date' ] . " недоступен на данный момент";
		}
	}
	
	public static function getBoughtMessage ( $data ) {
		try {
			$court = Court::whereId( $data[ 'court_id' ] ) -> first();
			$message = $court[ 'title' ] . ' в ' . $data[ 'time' ] . " " . $data[ 'date' ] . " больше недоступен";
			return $message;
		} catch ( \Exception $e ) {
			return "Корт в " . $data[ 'time' ] . " часа " . $data[ 'date' ] . " больше недоступен";
		}
	}
	public static function getBoughtByUserMessage ( $data ) {
		try {
			$court = Court::whereId( $data[ 'court_id' ] ) -> first();
			$message = "Вы уже купили " . $court[ 'title' ] . ' ' . $data[ 'time' ] . " " . $data[ 'date' ];
			return $message;
		} catch ( \Exception $e ) {
			$message = "Вы уже купили корт " . $data[ 'time' ] . " часа " . $data[ 'date' ];
		}
	}
	public static function getReservedMessage20Minutes ( $data ) {
		return 'Убедитесь, что Вы только что не оплачивали данный заказ.\nЕсли Вы уже оплатили заказ, то в течение 20 минут забронированные корты и купленные абонементы отобразятся в приложении у Вас в профиле.';
	}

	public static function getFatalErrorMessage ( $errorMessage ) {
		// use $errorMessage?
		return "Пожалуйста попробуйте забронировать позже";
	}




	// DATES
	public static function getDateNotPermittedMessage() {
		return "Бронирования кортов в следующем месяце будут доступны начиная с 10:00 26-го числа текущего месяца.";
	}

	public static function getDateExpiredMessage() {
		return 'Невозможно бронировать корт на прошедшую дату.';
	}
	// DATES

}