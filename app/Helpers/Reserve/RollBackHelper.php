<?php

namespace App\Helpers\Reserve;
use App\Reserve;
use App\Helpers\Reserve\VariousHelper;
use App\Helpers\Reserve\RollBackHelper;
use App\Helpers\Reserve\LogHelper;

class RollBackHelper {
	protected $reservedSeatsIds = null;

	public function __construct( $reservedSeatsIds ) {
		$this -> reservedSeatsIds = $reservedSeatsIds;
	}

	protected function validate() {
		if ( $this -> reservedSeatsIds && is_array( $this -> reservedSeatsIds ) && count( $this -> reservedSeatsIds ) ) {
			return true;
		}
		return false;
	}
	public function rollback () {
		try {
			if ( $this -> validate() ) {
				Reserve::whereIn( 'id' , $this -> reservedSeatsIds ) -> delete();
			}
		} catch ( \Exception $e ) {
			LogHelper::log( $e -> getMessage() , 'RollBackHelper rollback exception' );
		}
	}
}