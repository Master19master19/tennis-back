<?php

namespace App\Helpers\Reserve;
use App\Reserve;
use App\BoughtSeat;
use App\BoughtSubscription;
use App\Helpers\Reserve\VariousHelper;
use App\Helpers\Reserve\RollBackHelper;
use App\Helpers\Reserve\LogHelper;
use App\Helpers\BitrixReserveHelper;
use App\Helpers\BitrixSendPaymentHelper;


class CronHelper {
	
	public static function sendToBitrix() {
		BitrixHelper::start();
	}

	public static function removeExpiredReserves () {
		$res = Reserve::where( 'paid' , 0 ) -> whereDate( 'created_at' , '<' , date( 'Y-m-d H:i:s' , strtotime( env( 'RESERVE_TIME_PASSED' ) ) ) ) -> delete();
	}
	
	public static function copyBoughtSeats () {
		$currentRowId = null;
		$res = Reserve::where( 'paid' , 1 ) -> where( 'copied_to_main_db' , '!=' , 1 ) -> get();
		try {
			foreach ( $res as $key => $dbRowToCopy ) {
				$currentRowId = $dbRowToCopy[ 'id' ];
				self::copyToMainDbSingle( $dbRowToCopy );
				Reserve::where( 'id' , $currentRowId ) -> update([ 'copied_to_main_db' => 1 ]);
			}
		} catch ( \Exception $e ) {
			LogHelper::log( $e -> getMessage() , 'App\Helpers\Reserve\CronHelper copyBoughtSeats error' );
			Reserve::where( 'id' , $currentRowId ) -> update([ 'copied_to_main_db' => -1 ]);
		}
	}

	protected static function copyToMainDbSingle ( $row ) {
		$data = [
            'order_id' => $row[ 'order_id' ],
            'court_id' => $row[ 'court_id' ],
            'time' => $row[ 'time' ],
            'price' => $row[ 'price' ],
            'user_id' => $row[ 'user_id' ],
            'date' => $row[ 'date' ],
            'created_at' => date( 'Y-m-d H:i:s' ),
            'updated_at' => date( 'Y-m-d H:i:s' ),
            'paid' => 1,
            'isNewVersion' => 1,
        ];
        BoughtSeat::create( $data );
	}

}




class BitrixHelper {
	public static function start () {
		$orderIds = BoughtSeat::where([
			'isNewVersion' => 1,
		]) -> where( 'user_id' , '!=' , 0 ) -> whereIn( 'sentToPartner' , [ -1 , -2 , -3 , 0 ] ) -> select( 'order_id' ) -> distinct() -> get() -> toArray();
		if ( self::validate( $orderIds ) ) {
			foreach ( $orderIds as $orderId ) {
				self::sendToBitrixByOrderId( $orderId );
			}
		}
	}

	protected static function sendToBitrixByOrderId ( $orderId ) {
		$boughtSeats = BoughtSeat::where([
			'isNewVersion' => 1,
			'order_id' => $orderId,
		]) -> get() -> toArray();
		if ( self::validate( $boughtSeats ) ) {
			try {
				$bitrixReservedResult = BitrixReserveHelper::sendNewVersion( $boughtSeats );
				if ( $bitrixReservedResult ) {
					try {
            			BoughtSubscription::where( [ 'order_id' => $orderId ] ) -> update([ 'paid' => 1 ]);
			            BitrixSendPaymentHelper::send( $orderId );
					} catch ( \Exception $e ) {
						BoughtSeat::where([
							'order_id' => $orderId,
						]) -> update([ 'sentToPartner' => -3 ]);
						return false;
					}
				} else {
					BoughtSeat::where([
						'order_id' => $orderId,
					]) -> update([ 'sentToPartner' => -1 ]);
					return false;
				}
			} catch ( \Exception $e ) {
				BoughtSeat::where([
					'order_id' => $orderId,
				]) -> update([ 'sentToPartner' => -2 ]);
				return false;
			}
		}
	}

	protected static function validate ( $dataResult ) {
		if ( $dataResult && is_array( $dataResult ) && count( $dataResult ) ) {
			return true;	
		}
		return false;
	}
}