<?php
namespace App\Helpers;
use App\Arena;
use App\Court;
use App\Seat;
use App\BoughtSeat;

class CourtHelper {
    public static function getCourts ( $dateString , $forAdmin = false ) {
        // return [];
        $date = strtotime( $dateString );
        $date = date( 'Y-m-d' , $date );
        $arenas = Arena::whereActive( 1 ) -> with( 'courts' ) -> with([
                'seats' => function( $query ) {
                    $query -> orderBy( 'time' );
                }
            ]) -> orderBy( 'sort' , 'ASC' ) -> get() -> toArray();
        // dd($arenas);
        $response = [];
        foreach ( $arenas as $arena ) {
            $arenaRow = [];
            if ( $forAdmin === false ) {
                $unAvailableSeats = BoughtSeat::where( [ 'date' =>  $date , 'paid' => 1 , 'arena_id' => $arena[ 'id' ] ] ) -> get( [ 'seat_id' , 'court_id' ] ) -> mapToGroups( function ( $item , $key ) {
                    return [ $item[ 'court_id' ] => $item[ 'seat_id' ] ];
                }) -> toArray();
            } else {
                $unAvailableSeats = BoughtSeat::where( [ 'date' =>  $date , 'paid' => 1 , 'arena_id' => $arena[ 'id' ] ] ) -> get( [ 'time' , 'court_id' ] ) -> mapToGroups( function ( $item , $key ) {
                    return [ $item[ 'court_id' ] => $item[ 'time' ] ];
                }) -> toArray();
            }
            $arenaRow = [
                'title' => $arena[ 'title' ],
                'id' => $arena[ 'id' ],
                'cell_color' => $arena[ 'cell_color' ],
                'courts' => $arena[ 'courts' ],
                'unAvailableSeats' => $unAvailableSeats
            ];
            foreach ( $arena[ 'seats' ] as $seat ) {
                $arr = [
                    'price' => $seat[ 'price' ],
                    'id' => $seat[ 'id' ],
                    'time' => $seat[ 'time' ],
                    'price_weekend' => $seat[ 'price_weekend' ],
                    'court_id' => $seat[ 'court_id' ]
                    // 'keey' => rand( 0 , 1000 ) + $seat[ 'id' ] + rand( 0 , 1000 )
                ];
                $arenaRow[ 'seats' ][] = $arr;
            }
            $response[] = $arenaRow;
        }
        return $response;
    }
    private static function getWeekDay ( $dateString ) {
        $date = strtolower( date( 'l' , strtotime( $dateString ) ) );
        return $date;
    }
    public static function getCourtsNew ( $dateString ) {
        $date = strtotime( $dateString );
        $date = date( 'Y-m-d' , $date );
        $weekDayTitle = self::getWeekDay( $dateString );
        $arenas = Arena::whereActive( 1 )
        -> with( 'courts' )
        -> with([
                'seats' => function( $query ) {
                    $query -> orderBy( 'time' );
                }
            ]) -> orderBy( 'sort' , 'ASC' ) -> get() -> toArray();
        $response = [];
        foreach ( $arenas as $arenaKey => $arena ) {
            foreach ( $arena[ 'courts' ] as $courtKey => $courtValue ) {
                if ( $courtValue[ 'active' ] == 0 ) {
                    unset( $arenas[ $arenaKey ][ 'courts' ][ $courtKey ] );
                }
            }
        }
        foreach ( $arenas as $arena ) {
            $arenaRow = [];
            $unAvailableSeats = BoughtSeat::where( [ 'date' =>  $date , 'paid' => 1 , 'arena_id' => $arena[ 'id' ] ] ) -> get( [ 'seat_id' , 'court_id' ] ) -> mapToGroups( function ( $item , $key ) {
                return [ $item[ 'court_id' ] => $item[ 'seat_id' ] ];
            }) -> toArray();
            $arenaRow = [
                'title' => $arena[ 'title' ],
                'id' => $arena[ 'id' ],
                'cell_color' => $arena[ 'cell_color' ],
                'courts' => $arena[ 'courts' ],
                'unAvailableSeats' => $unAvailableSeats
            ];
            foreach ( $arena[ 'seats' ] as $seat ) {
                $arr = [
                    'price' => $seat[ $weekDayTitle ],
                    'price_weekend' => $seat[ $weekDayTitle ],
                    'id' => $seat[ 'id' ],
                    'time' => $seat[ 'time' ],
                    'court_id' => $seat[ 'court_id' ]
                    // 'keey' => rand( 0 , 1000 ) + $seat[ 'id' ] + rand( 0 , 1000 )
                ];
                $arenaRow[ 'seats' ][] = $arr;
            }
            foreach ( $arena[ 'courts' ] as $courtKey => $courtValue ) {
                $cellColor = $courtValue[ 'cell_color' ] && strlen( $courtValue[ 'cell_color' ] ) ? $courtValue[ 'cell_color' ] : $arena[ 'cell_color' ];
                $arenaRow[ 'courts' ][ $courtKey ][ 'cell_color' ] = $cellColor;
            }
            $response[] = $arenaRow;
        }
        return $response;
    }
}