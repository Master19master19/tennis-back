<?php

namespace App\Helpers;
use App\User;

class NotificationHelper {
    public static function send ( $type , $data = [] ) {
        $expoTokens = [];
        $users = User::whereNotNull( 'expo_token' ) -> get();
        foreach ( $users as $key => $user ) {
            $expoTokens[] = $user -> expo_token;
        }
        $expo = \ExponentPhpSDK\Expo::normalSetup();
        $channelName = $type . '_' . date( 'md' ) . uniqid();
        foreach ( $expoTokens as $recipient ) {
            // var_dump($channelName , $recipient);
            $expo -> subscribe( $channelName , $recipient );
        }
        $notification = [
            'title' => 'Новость',
            'body' => $data[ 'title' ],
            'data'=> json_encode ( [ 'data' => $data , 'type' => $type ] )
        ];
        $res = $expo -> notify( $channelName , $notification );
        if ( isset( $res[ 'status_code' ] ) && $res[ 'status_code' ] == 400 ) {
            var_dump($response['body']);
        }
        // dd($channelName,$notification);
    }
    public static function sendAnnouncement ( $type , $data = [] ) {
        $expoTokens = [];
        $users = User::whereNotNull( 'expo_token' ) -> get();
        foreach ( $users as $key => $user ) {
            $expoTokens[] = $user -> expo_token;
        }
        $expo = \ExponentPhpSDK\Expo::normalSetup();
        $channelName = $type;
        foreach ( $expoTokens as $recipient ) {
            $expo -> subscribe( $channelName , $recipient );
        }
        $notification = [
            'title' => 'Анонс турнира',
            'body' => $data[ 'title' ],
            'data'=> json_encode ( [ 'data' => $data , 'type' => $type ] )
        ];
        $res = $expo -> notify( $channelName , $notification );
    }
}