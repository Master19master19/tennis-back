<?php

namespace App\Helpers;

use App\BoughtSeat;
use App\Helpers\BitrixHelper;
use App\Helpers\BitrixSendPaymentHelper;

class FixHelper {
    private static $templateCancel = [
        'class' => 'rent',
        'cancelReserve' => [],
        'phone' => ''
    ];
    public static function cancelReserve ( $reserve , $user ) {
        $startHour = (int) $reserve -> time;
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reserve -> date . "T" . $startHour . ":00:00";
        $finishTime = $reserve -> date . "T" . $startHour . ":59:59";
        self::$templateCancel[ 'cancelReserve' ] = [
            [
                'courtId' => strval( $reserve -> court_id ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ]
        ];
        self::$templateCancel[ 'phone' ] = BitrixHelper::getPhone( $user -> phone );
        BitrixHelper::send( self::$templateCancel , 'cancel_reserve' );
    }
    private static $templateReserve = [
        'class' => 'rent',
        'newReserve' => [],
        'phone' => ''
    ];
    public static function Reserve ( $reserve , $user ) {
        $startHour = (int) $reserve -> time;
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reserve -> date . "T" . $startHour . ":00:00";
        $finishTime = $reserve -> date . "T" . $startHour . ":59:59";
        self::$templateReserve[ 'newReserve' ] = [
            [
                'courtId' => strval( $reserve -> court_id ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ]
        ];
        self::$templateReserve[ 'phone' ] = BitrixHelper::getPhone( $user -> phone );
        BitrixHelper::send( self::$templateReserve , 'cancel_reserve' );
    }
}