<?php

namespace App\Helpers;

use App\BoughtSeat;
use App\ReservedSeat;
use App\Order;
use App\User;
use App\Helpers\BitrixHelper;
use App\Helpers\PaymentHelper;

class BitrixCancelReserveHelper {
    private static $template = [
        'class' => 'rent',
        'cancelReserve' => [],
        'guid' => '',
        'phone' => ''
    ];
    public static function cancelReserveByOrderId ( $orderId ) {
        $order = Order::whereId( $orderId ) -> firstOrFail();
        $user = User::whereId( $order -> user_id ) -> firstOrFail();
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $orderId ] ) -> get();
        if ( null == $boughtSeats || ! count( $boughtSeats ) ) return true;
        foreach ( $boughtSeats as $seat ) {
            $isReserved = ReservedSeat::where([
                'court_id' => $seat -> court_id,
                'time' => $seat -> time,
                'arena_id' => $seat -> arena_id,
                'seat_id' => $seat -> seat_id,
                'date' => $seat -> date,
                'user_id' => $user -> id
            ]) -> first();
            if ( null != $isReserved ) {
                self::cancelReserve( $isReserved , $user );
            }
        }
    }
    public static function cancelReserve ( $reserve , $user ) {
        $startHour = (int) $reserve -> time;
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reserve -> date . "T" . $startHour . ":00:00";
        $finishTime = $reserve -> date . "T" . $startHour . ":59:59";
        self::$template[ 'cancelReserve' ] = [
            [
                'courtId' => strval( $reserve -> court_id ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ]
        ];
        self::$template[ 'phone' ] = BitrixHelper::getPhone( $user -> phone );
        $guid = Helper::generateGuid( self::$template );
        self::$template[ 'guid' ] = $guid;
        ReservedSeat::whereId( $reserve -> id ) -> delete();
        $res = BitrixHelper::send( self::$template , 'cancel_reserve' );
    }
    public static function cancelFixReserve ( $reserve , $user ) {
        $startHour = (int) $reserve -> time;
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reserve -> date . "T" . $startHour . ":00:00";
        $finishTime = $reserve -> date . "T" . $startHour . ":59:59";
        self::$template[ 'cancelReserve' ] = [
            [
                'courtId' => strval( $reserve -> court_id ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ]
        ];
        self::$template[ 'phone' ] = BitrixHelper::getPhone( $user -> phone );
        // dd(json_encode( self::$template));
        $guid = Helper::generateGuid( self::$template );
        self::$template[ 'guid' ] = $guid;
        BitrixHelper::send( self::$template , 'cancel_reserve' );
    }
    private static function reversePhone ( $number ) {
        $xpl = str_split ( $number );
        if ( ! isset( $xpl[ 2 ] ) ) return '000000000000';
        $newNum = '+7' . " (" . $xpl[ 2 ] . $xpl[ 3 ] . $xpl[ 4 ] . ") " . $xpl[ 5 ] . $xpl[ 6 ] . $xpl[ 7 ] . "-" . $xpl[ 8 ] . $xpl[ 9 ] . "-" . $xpl[ 10 ] . $xpl[ 11 ];
        return $newNum;
    }
    public static function canCancelByDataProvided ( $data ) {
// {"class":"rent","cancelReserve":[{"courtId":"13","startTime":"2021-02-15T08:00:00","finishTime":"2021-02-15T08:59:59"}],"phone":"+79269524100","changeDate":"2021-01-26T11:45:02"}
        $phone = $data[ 'phone' ];
        $user = User::wherePhone( self::reversePhone( $phone ) ) -> first();
        if ( null === $user ) {
            // file_put_contents(__DIR__ . '/triedToCancelNoUser' , json_encode( $data ) . PHP_EOL , FILE_APPEND );
            return false;
        }
        // file_put_contents(__DIR__ . '/triedToCancel' , json_encode( $data ) . PHP_EOL , FILE_APPEND );
        $reservations = $data[ 'cancelReserve' ];
        $errors = [];
        if ( is_array( $reservations ) && isset( $reservations[ 0 ] ) && isset( $reservations[ 1 ] ) ) { // this is gonna be ended soon
            file_put_contents( env( 'LOG_PATH' ) . '/cancel.array' , json_encode( $data ) . PHP_EOL , FILE_APPEND );
        } else {
            // file_put_contents( env( 'LOG_PATH' ) . '/aaaaa.array' , json_encode( $reservations ) . PHP_EOL , FILE_APPEND );
            $reservation = $reservations[ 0 ];
            $startHourInit = explode( 'T' , $reservation[ 'startTime' ] )[ 1 ];
            $startHour = substr( $startHourInit , 0 , -3 );
            $startDate = explode( 'T' , $reservation[ 'startTime' ] )[ 0 ];
            $courtId = $reservation[ 'courtId' ];
            $search = [
                'court_id' => $courtId,
                'date' => $startDate,
                'time' => $startHour,
                'paid' => 1
            ];
            $boughtSeat = BoughtSeat::where( $search ) -> first();
            if ( null !== $boughtSeat ) {
                return null;
            }
            unset( $search[ 'paid' ] );
            $boughtSeat = ReservedSeat::where( $search ) -> where( 'reserved_until' , '>' , time() ) -> first();
            if ( null != $boughtSeat ) {
                return false;
            }
        }
        // file_put_contents( env( 'LOG_PATH' ) . '/triedToCancel' , json_encode([ 'left' => $reservations , 'last_search' => $search ]) . PHP_EOL , FILE_APPEND );
        return $data;
        // if ( count( $reservations ) > 0 ) {
        //     $data[ 'cancelReserve' ] = array_values( $reservations );
        //     return $data;
        // } else {
        //     // file_put_contents(__DIR__ . '/triedToCancelSucucess' , json_encode( $data ) . PHP_EOL , FILE_APPEND );
        //     return false;
        // }
    }
    public static function check() {
        $reservedSeats = ReservedSeat::orderBy( 'id' , 'DESC' ) -> whereChecked( 0 ) -> get();
        foreach ( $reservedSeats as $reserve ) {
            if ( $reserve -> reserved_until - time() < 0 ) {
                // usleep( 500000 );
                // dd($reserve);
                $search = [
                    'paid' => 1,
                    'seat_id' => $reserve -> seat_id,
                    'court_id' => $reserve -> court_id,
                    'arena_id' => $reserve -> arena_id,
                    'user_id' => $reserve -> user_id,
                    'date' => $reserve -> date,
                    'time' => $reserve -> time,
                ];
                $boughtSeat = BoughtSeat::where( $search ) -> first();
                if ( null === $boughtSeat ) {
                    $user = User::whereId( $reserve -> user_id ) -> first();
                    if ( null == $user ) continue;




                    unset( $search[ 'paid' ] );
                    $boughtSeat = BoughtSeat::where( $search ) -> first();
                    if ( null !== $boughtSeat ) {
                        $order = Order::whereId( $boughtSeat -> order_id ) -> first();
                        if ( null !== $order ) {
                            $key = $order -> key;
                            $status = PaymentHelper::getPaymentInfoStatus( $key );
                            $order -> status = $status;
                            $order -> save();
                            if ( $status == 'pending' ) {
                                // continue;
                            }
                            // dd ( $status );
                        }
                    }
                    // dd($boughtSeat);
                    // dd($reserve);
                    self::cancelReserve ( $reserve , $user );
                    // dd(self::$template,$reserve);
                } else {
                    $reserve -> checked = 1;
                    $reserve -> save();
                }
            } else {
                // file_put_contents( __DIR__ . '/aaaacancel' , json_encode( self::$template ) );
                // dd('no timeout yet',$reserve,$reserve -> reserved_until - time());
            }
            // dd($reserve);
        }
        // dd($reservedSeats);
    }
}