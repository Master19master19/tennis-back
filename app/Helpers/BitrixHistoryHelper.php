<?php

namespace App\Helpers;
use \GuzzleHttp\Exception\RequestException;
use App\BitrixHistory;
use App\Helpers\BitrixHelper;
use App\Helpers\BitrixCancelReserveHelper;

class BitrixHistoryHelper {
	public static function createHistory( $data , $message ) {
		$fill = [
			'data' => json_encode( $data ),
			'error' => $message
		];
		BitrixHistory::create( $fill );
	}
	protected static function log ( $data , $title = 'history.cancel.reserve' ) {
		file_put_contents( env( 'LOG_PATH' ) . '/' . $title , json_encode( $data ) . PHP_EOL . PHP_EOL , FILE_APPEND );
	}
	public static function clearReserveHistory ( $data , $record ) {
		$canCancel = BitrixCancelReserveHelper::canCancelByDataProvided( $data );
		if ( null === $canCancel ) {
			BitrixHistory::whereId( $record -> id ) -> update([ 'done' => 1 , 'error' => 'Been bought' ]);
		} else if ( false === $canCancel ) {
			BitrixHistory::whereId( $record -> id ) -> update([ 'tries' => $record -> tries + 1 , 'error' => 'Reserved yet' ]);
		} else {
			self::log( $canCancel );
			self::handleHistory( $canCancel , $record );
		}
	}
	public static function handleHistory ( $data , $record ) {
		$resp = BitrixHelper::send( $data , 'checking' );
		if ( $resp === true ) {
			BitrixHistory::whereId( $record -> id ) -> update([ 'done' => 1 ]);
		} else {
			BitrixHistory::whereId( $record -> id ) -> update([ 'tries' => $record -> tries + 1 , 'error' => $resp ]);
		}
	}
	// public static function clearHistory() {

	// }
	public static function clearHistory() {
		if ( env( 'BITRIX_DOWN' ) == true ) {
			return;
		}
		$res = BitrixHistory::where( 'done' , 0 ) -> whereRaw( "DATE_ADD(created_at,INTERVAL 2 MINUTE ) < NOW()" ) -> where( 'tries' , '<' , 80 ) -> where( 'data' , 'LIKE' , '%payment%' ) -> take( 10 ) -> get();
		foreach ( $res as $paymentRecord ) {
			$data = $paymentRecord -> data;
			$dat = json_decode( $data , 1 );
			self::handleHistory( $dat , $paymentRecord );
            usleep( 500000 );
            // sleep( 1 );
		}
		$resp = BitrixHistory::where( 'done' , 0 ) -> whereRaw( "DATE_ADD( created_at , INTERVAL 10 MINUTE ) < NOW()" ) -> where( 'tries' , '<' , 80 ) -> where( 'data' , 'NOT LIKE' , '%payment%' ) -> take( 60 ) -> get();
		foreach ( $resp as $record ) {
			$data = $record -> data;
			$dat = json_decode( $data , 1 );
			if ( isset( $dat[ 'cancelReserve' ] ) ) {
				self::clearReserveHistory( $dat , $record );
			} else {
				self::handleHistory( $dat , $record );
			}
            usleep( 5000 );
            // sleep( 1 );
		}
	}

}