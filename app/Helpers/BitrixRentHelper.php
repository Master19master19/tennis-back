<?php

namespace App\Helpers;
use App\BoughtSeat;
use App\ReservedSeat;
use App\Court;
use App\Seat;

// {"class":"rent","cancelReserve":{"courtId":"25","startTime":"2020-06-03T17:30:00","finishTime":"2020-06-03T21:29:59"},"newReserve":{"courtId":"25","startTime":"2020-06-03T17:00:00","finishTime":"2020-06-03T20:59:59"}}


class BitrixRentHelper {
    private static $error = [];
    private static function getSeatIds ( $info , $courtId ) {
        $startExplode = explode( 'T' , $info[ 'startTime' ] );
        $endExplode = explode( 'T' , $info[ 'finishTime' ] );
        $date = $startExplode[ 0 ];
        $response = [
            'date' => $date,
            'seats' => []
        ];
        $startTime = str_replace( ':00:00' , '' , $startExplode[ 1 ] );
        $endTime = str_replace( ':59:59' , '' , $endExplode[ 1 ] );
        $endTime = str_replace( ':58:59' , '' , $endExplode[ 1 ] );
        // if ( $startTime == $endTime ) {
        //     $endTime--;
        // }
        for ( $x = (int) $startTime; $x <= ( int ) $endTime ; $x++ ) {
            if ( $x < 10 ) {
                $time = "0" . $x . ":00";
            } else {
                $time = $x . ":00";
            }
            $searchData = [
                'time' => $time,
                'court_id' => $courtId,
            ];
            $seat = Seat::where($searchData) -> first();
            if ( $seat == null ) {
                self::$error = [
                    'result' => true,
                    'errorCode' => 500,
                    'error' => "Время {$time} для корта в арене {$arenaId} не найдена"
                ];
                continue;
            } else {
                $response[ 'seats' ][] = [
                    'id' => $seat -> id ,
                    'price' => $seat -> price,
                    'time' => $seat -> time,
                ];
            }
        }
        return $response;
    }
    
// {"class":"rent","newReserve":{"courtId":"9","startTime":"2020-06-04T11:00:00","finishTime":"2020-06-04T12:59:59"}}
    private static function handleNewReserve ( $newReserve ) {
        // if ( is_array( $newReserve ) && count ( $newReserve ) ) {
        //     foreach ( $newReserve as $value ) {
        //         $this -> 
        //     }
        // }
        $courtId = $newReserve[ 'courtId' ];
        $court = Court::whereId( $courtId ) -> first();
        if ( null == $court ) {
            self::$error = [
                'result' => true,
                'errorCode' => 500,
                'error' => "Корт {$courtId} не найден"
            ];
            return;
        }
        $arenaId = $court -> arena_id;
        $resp = self::getSeatIds ( $newReserve , $courtId );
        $date = $resp[ 'date' ];
        $seats = $resp[ 'seats' ];
        if ( count( $seats ) ) {
            foreach ( $seats as $seat ) {
                $search = [
                    'arena_id' => $arenaId,
                    'court_id' => $courtId,
                    // 'user_id' => 0,
                    'seat_id' => $seat[ 'id' ],
                    // 'price' => $seat[ 'price' ],
                    'date' => $date,
                    // 'created_at' => date( 'Y-m-d H:i:s' ),
                    'paid' => 1,
                    'time' => $seat[ 'time' ],
                ];
                $check = BoughtSeat::where( $search ) -> first();
                if ( null !== $check ) {
                    if ( $check -> user_id != 0 ) {
                        self::$error = [
                            'result' => true,
                            'errorCode' => 500,
                            'error' => "Время " . $seat[ 'time' ] . " уже оплачено"
                        ];
                        return;
                    }
                } else {
                    unset( $search[ 'paid' ] );
                    $check = BoughtSeat::where( $search ) -> first();
                    if ( null !== $check ) {
                        $chicken = ReservedSeat::where( $search ) -> first();
                        // dd($chicken);
                        if ( null !== $chicken && $chicken -> reserved_until - time() > 0 ) {
                            self::$error = [
                                'result' => false,
                                'errorCode' => 400,
                                'error' => "Время " . $seat[ 'time' ] . " забронировано"
                            ];
                            return;
                        }
                    }
                }
                $fill = [
                    'arena_id' => $arenaId,
                    'court_id' => $courtId,
                    'user_id' => 0,
                    'seat_id' => $seat[ 'id' ],
                    'price' => $seat[ 'price' ],
                    'date' => $date,
                    'created_at' => date( 'Y-m-d H:i:s' ),
                    'paid' => 1,
                    'time' => $seat[ 'time' ],
                ];
                // $check = 
                BoughtSeat::create( $fill );
            }
        }
    }
    private static function handleCancelReserve ( $cancelReserve ) {
        $courtId = $cancelReserve[ 'courtId' ];
        $court = Court::whereId( $courtId ) -> first();
        if ( null == $court ) {
            self::$error = [
                'result' => true,
                'errorCode' => 500,
                'error' => "Корт {$courtId} не найден"
            ];
            return;
        }
        $arenaId = $court -> arena_id;
        $resp = self::getSeatIds ( $cancelReserve , $courtId );
        $date = $resp[ 'date' ];
        $seats = $resp[ 'seats' ];
        if ( count( $seats ) ) {
            foreach ( $seats as $seat ) {
                $search = [
                    'arena_id' => $arenaId,
                    'court_id' => $courtId,
                    // 'user_id' => 0,
                    'seat_id' => $seat[ 'id' ],
                    // 'price' => $seat[ 'price' ],
                    'date' => $date,
                    // 'created_at' => date( 'Y-m-d H:i:s' ),
                    // 'paid' => 1,
                    'time' => $seat[ 'time' ],
                ];
                $boughtSeat = BoughtSeat::where( $search ) -> delete();
                // $boughtSeat = BoughtSeat::where( $search ) -> first();
                // if ( null !== $boughtSeat && $boughtSeat -> user_id != 0 ) {
                //     self::$error = [
                //         'result' => true,
                //         'errorCode' => 500,
                //         'error' => "Время " . $seat[ 'time' ] . " уже оплачено"
                //     ];
                //     return;
                // } else {
                //     unset ( $search[ 'paid' ] );
                //     $chicken = ReservedSeat::where( $search ) -> first();
                //     if ( null !== $chicken && $chicken -> reserved_until - time() > 0 ) {
                //         self::$error = [
                //             'result' => false,
                //             'errorCode' => 400,
                //             'error' => "Время " . $seat[ 'time' ] . " забронировано"
                //         ];
                //         return;
                //     }
                //     $boughtSeat = BoughtSeat::where( $search ) -> delete();
                // }
                // $search = [
                //     'arena_id' => $arenaId,
                //     'court_id' => $courtId,
                //     // 'user_id' => 0,
                //     'seat_id' => $seat[ 'id' ],
                //     // 'price' => $seat[ 'price' ],
                //     'date' => $date,
                //     // 'created_at' => date( 'Y-m-d H:i:s' ),
                //     'paid' => 1,
                //     'time' => $seat[ 'time' ],
                // ];
                // $boughtSeat = BoughtSeat::where( $search ) -> delete();
                // dd($boughtSeat);
            }
        }
    }
	public static function handleRent ( $data ) {
        // dd($data);
        if ( isset( $data[ 'cancelReserve' ] ) && is_array( $data[ 'cancelReserve' ] ) && count( $data[ 'cancelReserve' ] ) ) {
            self::handleCancelReserve( $data[ 'cancelReserve' ] );
        }
        if ( isset( $data[ 'newReserve' ] ) && is_array( $data[ 'newReserve' ] ) && count( $data[ 'newReserve' ] ) ) {
            // dd('pl');
            self::handleNewReserve( $data[ 'newReserve' ] );
        }
        if ( count( self::$error ) ) {
            return self::$error;
        } else {
            return [ 'result' => true ];
        }
	}
}