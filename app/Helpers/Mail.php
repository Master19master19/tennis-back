<?php

namespace App\Helpers;
use Mailgun\Mailgun;

class Mail {
	protected static function getAdminMail () {
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$xpl = explode( ',' , $to );
		$response = [];
		if ( count( $xpl ) > 1 ) {
			$response[ 0 ] = array_shift ( $xpl );
			$response[ 1 ] = $xpl;
		} else if ( count( $xpl ) == 1 ) {
			$response[ 0 ] = array_shift ( $xpl );
			$response[ 1 ] = [];

		} else {
			$response = [ $to , [] ];
		}
		return $response;
	}
	public static function sendMail ( $title , $desc , $to ) {
		$data = [
			'title' => $title ,
			'desc' => $desc
		];
		$res = \Mail::send( 'mail.send' , $data , function( $message ) use ( $to ) {
			$message -> to( $to );
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
	public static function sendToAdmin ( $data ) {


		// log email data
		// if ( isset( $data[ 'order' ] ) && isset( $data[ 'order' ][ 'key' ] ) ) {
		// 	file_put_contents( env( 'LOG_PATH' ) . '/emails/' . $data[ 'order' ][ 'key' ] . '.json' , json_encode( $data ) );
		// }
		// if ( isset( $data[ 'order' ] ) && isset( $data[ 'order' ][ 'id' ] ) ) {
			// $mailForFuture = new \App\Mail;
			// $mailForFuture -> order_id = $data[ 'order' ][ 'id' ];
			// $mailForFuture -> save();
		// }
		// log email data

		
		$title = "Новая оплата";
		$data = [
			'title' => $title ,
			'data' => $data
		];
		// $message = view( 'mail.admin' ,  $data );
		// $message = $message -> render();
		// exit($message);
		try {
			$res = \Mail::send( 'mail.admin' , $data , function( $message ) {
				$emails = self::getAdminMail();
				$message -> to( $emails[ 0 ] );
				$message -> cc( $emails[ 1 ] );
				$message -> subject( 'Tennis.ru' );
			});
			return $res;
		} catch ( \Exception $e ) {
			if ( isset( $data[ 'data' ] ) && isset( $data[ 'data' ][ 'order' ] ) && isset( $data[ 'data' ][ 'order' ][ 'id' ] ) ) {
				$mailForFuture = new \App\Mail;
				$mailForFuture -> order_id = $data[ 'data' ][ 'order' ][ 'id' ];
				$mailForFuture -> save();
			} else {
				file_put_contents( env( 'LOG_PATH' ) . '/mailErrorNoOrderData' , json_encode($data) . PHP_EOL , FILE_APPEND );
			}
			file_put_contents( env( 'LOG_PATH' ) . '/mailError' , date( 'Y-m-d H:i:s' ) . $e -> getMessage() . PHP_EOL , FILE_APPEND );
			return false;
		}
		return false;
	}
	public static function sendToAdminTest ( $data ) {
		$title = "Новая оплата Test";
		$data = [
			'title' => $title ,
			'data' => $data
		];
		// $message = view( 'mail.admin' ,  $data );
		// $message = $message -> render();
		// exit($message);
		try {
			$res = \Mail::send( 'mail.admin' , $data , function( $message ) {
				$message -> to('vmxitaryan@gmail.com' );
				$message -> subject( 'Tennis.ru' );
			});
			return $res;
		} catch ( \Exception $e ) {
			file_put_contents( env( 'LOG_PATH' ) . '/mailError' , date( 'Y-m-d H:i:s' ) . $e -> getMessage() . PHP_EOL , FILE_APPEND );
			return false;
		}
		return false;
	}
	public static function sendFeedBackToAdmin ( $title , $info ) {
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];

		$data = [
			'title' => $title ,
			'name' => $info[ 'name' ],
			'email' => $info[ 'email' ],
			'phone' => $info[ 'phone' ],
			'messagee' => $info[ 'message' ],
		];
		try {
			$res = \Mail::send( 'mail.feedback' , $data , function( $message ) {
				$emails = self::getAdminMail();
				$message -> to( $emails[ 0 ] );
				$message -> cc( $emails[ 1 ] );
				$message -> subject( 'Tennis.ru' );
			});
			return $res;
		} catch ( \Exception $e ) {
			file_put_contents( env( 'LOG_PATH' ) . '/feedbackErr.log' , json_encode( $data ) . PHP_EOL , FILE_APPEND );
		}
	}
	public static function sendpersonalChildTrainingOrderToAdmin ( $info ) {
		$title = 'Новая заявка для школы тенниса';
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$data = [
			'title' => $title ,
			'child_name' => $info[ 'child_name' ],
			'phone_number' => $info[ 'phone_number' ],
			'parent_name' => $info[ 'parent_name' ],
			'child_birthday' => $info[ 'child_birthday' ],
			'additional' => $info[ 'additional' ],
			'level' => $info[ 'level' ],
		];
		$res = \Mail::send( 'mail.personalTrainingChild' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
	public static function sendpersonalTrainingOrder ( $info ) {
		$title = 'Новая заявка для школы тенниса';
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$data = [
			'title' => $title ,
			'name' => $info[ 'name' ],
			'phone' => $info[ 'phone' ],
			'email' => $info[ 'email' ],
			'additional' => $info[ 'additional' ],
		];
		$res = \Mail::send( 'mail.personalTraining' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			// var_dump($emails);
			// exit;
			$message -> subject( 'Tennis.ru' );
		});
		return $res;
	}
}