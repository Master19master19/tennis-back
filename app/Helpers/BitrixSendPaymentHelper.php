<?php

namespace App\Helpers;
use App\BoughtSeat;
use App\Subscription;
use App\User;
use App\Court;
use App\BoughtSubscription;
use App\Helpers\BitrixHelper;
use App\Helpers\BitrixReserveHelper;


class BitrixSendPaymentHelper {
    private static $template = [
        'class' => 'payment',
        'phone' => '',
        'items' => []
    ];
    private static $error = [];
    public static function send ( $orderId , $guid = null ) {
        if ( null == $guid ) {
            $guid = uniqid();
        }
        self::$template = [
            'class' => 'payment',
            'phone' => '',
            'items' => []
        ];
        $boughtSeats = BoughtSeat::where([ 'order_id' => $orderId ]) -> get();
        if ( null !== $boughtSeats && count( $boughtSeats ) ) {
            self::handleSeats( $boughtSeats );
        }
        $boughtSubs = BoughtSubscription::where([ 'order_id' => $orderId ]) -> get();
        if ( null !== $boughtSubs && count( $boughtSubs ) ) {
            self::handleSubs( $boughtSubs );
        }
        // file_put_contents(__DIR__ . '/paymentSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        self::$template[ 'phone' ] = BitrixHelper::getPhone( self::$template[ 'phone' ] );
        // dd($orderId,$boughtSeats,self::$template);
        self::$template[ 'guid' ] = $guid;
        $payment = BitrixHelper::send( self::$template , 'payment' );
        if ( $payment == false ) {
            $reserveBedorePayment = BitrixReserveHelper::sendBeforePaymentByOrderId( $orderId );
            $payment = BitrixHelper::send( self::$template , 'payment' );
        }
        return $payment;
    }
    private static function countExpirationTime ( $sub ) {
        $todayDate = date( 'd' );
        if ( $todayDate >= 25 ) { // next month
            $countingMonthString = "+1 month";
        } else { // this month
            $countingMonthString = "+1 day";
        }
        $expiration_time = date( 'Y-m-t\T23:59:59' , strtotime( $countingMonthString ) );
        $activation_time = date( 'Y-m-01\T00:00:00' , strtotime( $countingMonthString ) );
        return [
            'expiration_time' => $expiration_time,
            'activation_time' => $activation_time,
        ];
    }
    private static function handleSubs ( $subs ) {
        foreach ( $subs as $key => $sub ) {
            $subscription = Subscription::whereId( $sub -> subscription_id ) -> firstOrFail();
            $expiration_time = self::countExpirationTime( $sub )[ 'expiration_time' ];
            $activation_time = self::countExpirationTime( $sub )[ 'activation_time' ];
            $user = User::whereId( $sub -> user_id ) -> firstOrFail();
            $membershipType = isset( $sub -> type ) ? $sub -> type : 'kid';
            if ( $membershipType === 'adult' ) {
                $profileName = $user -> name;
            } else {
                $profileName = $sub -> profile_name;
            }
            $currentData = [
                'itemType' => 'membership',
                'membershipId' => $subscription -> external_id,
                'count' => 1,
                'membershipType' => $membershipType,
                'profileName' => $profileName,
                'expirationTime' => $expiration_time,
                'activationTime' => $activation_time,
                'sum' => $sub -> price,
            ];
            if ( $membershipType === 'kid' ) {
                $currentData[ 'birthday' ] = date( 'Y-m-d\T00:00:00' , strtotime( $sub -> profile_birthday ) );
            } else { // adult
                $currentData[ 'phone' ] =  BitrixHelper::getPhone( $user -> phone );
            }
            self::$template[ 'items' ][] = $currentData;
            self::$template[ 'phone' ] = $user -> phone;
        }
        file_put_contents( env( 'LOG_PATH' ) . '/bought.subscriptions' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        // dd(self::$template);
    }
    private static function handleSeats ( $seats ) {
        foreach ( $seats as $key => $seat ) {
            $court = Court::whereId( $seat -> court_id ) -> firstOrFail();
            self::$template[ 'items' ][] = [
                'itemType' => 'rent',
                // 'courtName' => $court -> title,
                'courtId' => $court -> id,
                'sum' => $seat -> price,
                'startTime' =>  $seat -> date . "T" . $seat -> time . ":00",
                'finishTime' =>  $seat -> date . "T" . str_replace( ':00' , ':59' , $seat -> time ) . ":59",
            ];
            $user = User::whereId( $seat -> user_id ) -> firstOrFail();
            self::$template[ 'phone' ] = $user -> phone;
        }
    } 









    //TEMP

    public static function tempSendPaymentByBoughtSeatId ( $boughtSeatId ) {
            $guid = uniqid();
        self::$template = [
            'class' => 'payment',
            'phone' => '',
            'items' => []
        ];
        $boughtSeats = BoughtSeat::where([ 'id' => $boughtSeatId ]) -> get();
        if ( null !== $boughtSeats && count( $boughtSeats ) ) {
            self::handleSeats( $boughtSeats );
        }
        // file_put_contents(__DIR__ . '/paymentSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        self::$template[ 'phone' ] = BitrixHelper::getPhone( self::$template[ 'phone' ] );
        // dd($orderId,$boughtSeats,self::$template);
        self::$template[ 'guid' ] = $guid;
        $payment = BitrixHelper::send( self::$template , 'payment' );
        return $payment;
    }
    //TEMP


}