<?php

namespace App\Helpers;

class Helper {
    public static function generateGuid ( $template ) {
        $timeFactor = date( 'mdH' ) . ceil( date( 'i' ) * 100 / 1000 ); // dividing 1 hour into 6 parts , each = 10 mins
        $guid = hash( 'sha256' , json_encode( $template ) ) . 'ft' . $timeFactor;
        // $guid = hash( 'md5' , time() ) . uniqid();
        return $guid;
    }
    public static function splitToBeCanceledReserve( $data , $message , $isHistory = false ) {
		if ( $isHistory === false ) { // if fixing Feb 12 history
			$data[ 'cancelReserve' ] = $data[ 'newReserve' ];
			unset( $data[ 'newReserve' ] );
		}
		$tempData = $data;
		foreach ( $data[ 'cancelReserve' ] as $key => $value ) {
			$tempData[ 'cancelReserve' ] = [ $value ];
			$tempData[ 'changeDate' ] = date( 'Y-m-d\TH:i:s' );
			$tempData[ 'guid' ] = uniqid() . '_cancel_' . $key;
			BitrixHistoryHelper::createHistory( $tempData , $message );
		}
    }
}