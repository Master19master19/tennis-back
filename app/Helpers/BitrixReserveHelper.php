<?php

namespace App\Helpers;
use App\Helpers\BitrixHelper;
use App\Helpers\Helper;
use Auth;
use App\BoughtSeat;
use App\ReservedSeat;
use App\Seat;
use App\Order;
use App\User;



class BitrixReserveHelper {
    private static $template = [
        'class' => 'rent',
        'newReserve' => [],
        'guid' => '',
        'phone' => ''
    ];
    public static function getPhone ( $number ) {
        $response = str_replace( '-' , '' , $number );
        $response = str_replace( ')' , '' , $response );
        $response = str_replace( '(' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        return $response;
    }
    private static $error = [];
    public static function send ( $reservedSeats ) {
        foreach ( $reservedSeats as $key => $res ) {
            $startHour = (int) $res[ 'time' ];
            if ( $startHour < 10 ) $startHour = '0' . $startHour;
            $startTime = $res[ 'date' ] . "T" . $startHour . ":00:00";
            $finishTime = $res[ 'date' ] . "T" . $startHour . ":59:59";
            self::$template[ 'newReserve' ][] = [
                'courtId' => strval( $res[ 'court_id' ] ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ];
        }
        self::$template[ 'phone' ] = self::getPhone( Auth::user() -> phone );
        $guid = Helper::generateGuid( self::$template );
        self::$template[ 'guid' ] = $guid;
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        return $res;
    }
    public static function sendNewVersion ( $reservedSeats ) {
        $userId = $reservedSeats[ 0 ][ 'user_id' ];
        $user = User::find( $userId );
        foreach ( $reservedSeats as $key => $res ) {
            $startHour = (int) $res[ 'time' ];
            if ( $startHour < 10 ) $startHour = '0' . $startHour;
            $startTime = $res[ 'date' ] . "T" . $startHour . ":00:00";
            $finishTime = $res[ 'date' ] . "T" . $startHour . ":59:59";
            self::$template[ 'newReserve' ][] = [
                'courtId' => strval( $res[ 'court_id' ] ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ];
        }
        self::$template[ 'phone' ] = self::getPhone( $user -> phone );
        $guid = Helper::generateGuid( self::$template );
        self::$template[ 'guid' ] = $guid;
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        return $res;
    }
    public static function sendBeforePaymentByOrderId ( $orderId ) {
        $order = Order::whereId( $orderId ) -> first();
        if ( $order == null ) {
            return false;
        }
        // if ( strtotime( $order -> created_at ) - strtotime( '-55 minutes' ) > 0 ) return true; // has to be 60 mins passed
        $reservedSeats = BoughtSeat::where( [ 'order_id' => $order -> id , 'paid' => 1 ] ) -> get() -> toArray();
        if ( null == $reservedSeats ) return false;
        $user = User::whereId( $order -> user_id ) -> first();
        if ( null == $user ) return false;
        self::$template[ 'newReserve' ] = [];
        foreach ( $reservedSeats as $key => $res ) {
            $startHour = (int) $res[ 'time' ];
            if ( $startHour < 10 ) $startHour = '0' . $startHour;
            $startTime = $res[ 'date' ] . "T" . $startHour . ":00:00";
            $finishTime = $res[ 'date' ] . "T" . $startHour . ":59:59";
            self::$template[ 'newReserve' ][] = [
                'courtId' => strval( $res[ 'court_id' ] ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ];
        }
        self::$template[ 'phone' ] = self::getPhone( $user -> phone );
        $guid = Helper::generateGuid( self::$template );
        self::$template[ 'guid' ] = $guid . '_before_payment';
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        return $res;
    }
    public static function BAKsendSingleBAK ( $reservedSeat , $phone ) {
        $startHour = (int) $reservedSeat[ 'time' ];
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reservedSeat[ 'date' ] . "T" . $startHour . ":00:00";
        $finishTime = $reservedSeat[ 'date' ] . "T" . $startHour . ":59:59";
        self::$template[ 'newReserve' ][] = [
            'courtId' => strval( $reservedSeat[ 'court_id' ] ),
            'startTime' => $startTime,
            'finishTime' => $finishTime,
        ];
        self::$template[ 'phone' ] = self::getPhone( $phone );
        dd(self::$template);
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        return $res;
    }











    // TEMP

    public static function tempSendWithBoughtSeatId ( $boughtSeatId ) {
        $reservedSeats = BoughtSeat::where( [ 'id' => $boughtSeatId ] ) -> get() -> toArray();
        if ( null == $reservedSeats ) return false;
        $user = User::whereId( $reservedSeats[0]['user_id'] ) -> first();
        if ( null == $user ) {
            $userRaw = [
                'phone' => '+70000000000'
            ];
            $user = json_decode( json_encode( $userRaw ) );
        };
        self::$template[ 'newReserve' ] = [];
        foreach ( $reservedSeats as $key => $res ) {
            $startHour = (int) $res[ 'time' ];
            if ( $startHour < 10 ) $startHour = '0' . $startHour;
            $startTime = $res[ 'date' ] . "T" . $startHour . ":00:00";
            $finishTime = $res[ 'date' ] . "T" . $startHour . ":59:59";
            self::$template[ 'newReserve' ][] = [
                'courtId' => strval( $res[ 'court_id' ] ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ];
        }
        self::$template[ 'phone' ] = self::getPhone( $user -> phone );
        $guid = Helper::generateGuid( self::$template );
        self::$template[ 'guid' ] = $guid . '_before_payment';
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        return $res;
    }

    // TEMP
}