<?php

namespace App\Helpers;
use App\User;
use App\Ban;

// {"class":"partner","email":"vmxitaryan@gmail.comyuy","phone":"+73636363636","name":"Sixxxx","surname":"Testing"}

class BitrixUserHelper {
    private static function getPhone ( $number ) {
        $xpl = str_split ( $number );
        if ( ! isset( $xpl[ 2 ] ) ) return '000000000000';
        if ( ! isset( $xpl[ 11 ] ) ) $xpl[ 11 ] = 5;
        $newNum = '+7' . " (" . $xpl[ 2 ] . $xpl[ 3 ] . $xpl[ 4 ] . ") " . $xpl[ 5 ] . $xpl[ 6 ] . $xpl[ 7 ] . "-" . $xpl[ 8 ] . $xpl[ 9 ] . "-" . $xpl[ 10 ] . $xpl[ 11 ];
        return $newNum;
    }
    
    protected static function handleBan ( $data ) {
        $check = Ban::wherePhone( $data[ 'phonee' ] ) -> first();
        if ( $data[ 'banned' ] == 'true' ) {
            if ( ! $check ) {
                $ban = new Ban;
                $ban -> phone = $data[ 'phonee' ];
                $ban -> save();
            }
        } else {
            if ( $check ) {
                Ban::wherePhone( $data[ 'phonee' ] ) -> delete();
            }
        }
    }
	public static function handleUser ( $data ) {
        $data[ 'phonee' ] = $data[ 'phone' ];
        if ( strlen( $data[ 'phone' ] ) < 7 ) {
            return [
                'result' => false,
                'errorCode' => 500,
                'error' => "Неправильный номер телефона" . $data[ 'phone' ]
            ];
        }
        $data[ 'phone' ] = self::getPhone( $data[ 'phone' ] );
        $phone = $data[ 'phone' ];
        if ( isset( $data[ 'oldPhone' ] ) ) {
            $data[ 'oldPhone' ] = self::getPhone( $data[ 'oldPhone' ] );
            $user = User::wherePhone( $data[ 'oldPhone' ] ) -> first();
            if ( null == $user ) {
                self::handleBan( $data );
                return [ 'result' => true ];
            } else {
                if ( null != $data[ 'email' ] ) {
                    $user -> email = $data[ 'email' ];
                }
                $user -> phone = $data[ 'phone' ];
                $user -> name = $data[ 'name' ] . ' ' . $data[ 'surname' ];
                $user -> save();
                self::handleBan( $data );
                return [ 'result' => true ];
            }
        } else {
            $user = User::wherePhone( $data[ 'phone' ] ) -> first();
            if ( null == $user ) {
                return [ 'result' => true ];
            } else {
                self::handleBan( $data );
                if ( User::whereEmail( $data[ 'email' ] ) -> count() ) {
                    return [
                        'result' => true,
                        'errorCode' => 500,
                        'error' => "Пользователь с таким email уже существует"
                    ];
                }
                if ( null != $data[ 'email' ] ) {
                    $user -> email = $data[ 'email' ];
                }
                $user -> phone = $data[ 'phone' ];
                $user -> name = $data[ 'name' ] . ' ' . $data[ 'surname' ];
                $user -> save();
                self::handleBan( $data );
                return [ 'result' => true ];
            }
        }
	}
}