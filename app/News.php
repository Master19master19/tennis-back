<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $guarded = [];
    public function getCreatedAtAttribute ( $value ) {
        $dateTime = strtotime( $value );
        $months = [
            '' , 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
        ];
        $month = $months[ date( 'n' , $dateTime ) ];
        return date( 'd ' . $month . ' Y' , $dateTime );
    }
    public function getImageUrlAttribute ( $value ) {
        if ( strpos( $value , 'http' ) === false ) {
            return env( 'APP_URL' ) . $value;
        }
        return $value;
    }

}
