<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    //
    protected $guarded = [];
	public function arena () {
		return $this -> hasOne( 'App\Arena', 'id' , 'arena_id' );
	}
}
